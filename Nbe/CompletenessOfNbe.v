(******************************************************************************
 * Pawel Wieczorek
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtSb_EquivalenceJudgements.


(******************************************************************************
 * Evaluators are defined for modeled terms/types
 *)

Theorem Terminating_on_valid:
  (forall Gamma,
    Gamma |= ->
    exists denv, EvalCxt Gamma denv /\ [denv] === [denv] #in# Gamma
  ) /\
  (forall Gamma Delta,
    |= {Gamma} === {Delta} -> True) /\
  (forall Gamma A B,
    Gamma |= A === B -> forall denv,  EvalCxt Gamma denv -> [denv] === [denv] #in# Gamma ->
    exists DA, EvalTm A denv DA
  ) /\
  (forall Gamma t s A,
    Gamma |= t === s : A -> forall denv, EvalCxt Gamma denv ->
    [denv] === [denv] #in# Gamma -> exists dt, EvalTm t denv dt
  )
.
Proof.
apply Val_ind.
(*.*)
eexists; repeat split; eauto.
(*.*)
intros.
destruct H as [denv Hdenv]; destruct Hdenv as [ Hdenv Henv ].
edestruct H0 as [d Hd]; eauto.
eexists; repeat split; eauto.
clear_inversion v0.
destruct H1 with denv denv as [DA HTp]; auto.
destruct HTp as [db Htp]; auto.

program_simpl.
assert (exists PA, InterpType DA PA).
apply InterpType_dom_PerType;  eauto with *.

destruct H4 as [PA HPA].
constructor 2 with PA; auto.
red; exists DA. repeat split; auto.
(**)
assert (d = DA ); eauto. subst.
apply Reflect_Characterization; eauto;
  eapply Per_domL; eauto. 

(**)
auto.
(*.*)
intros.
destruct e with denv denv as [a Ha]; auto.
destruct Ha as [a' Ha].
exists a. intuition.

(*.*)

intros.
destruct e with denv denv as [dtm0 HH]; auto.
destruct HH as [dtm1 HH].
destruct HH as [PA HH].
exists dtm0; intuition.
Qed.

Definition Terminating_on_ValCxt := proj1 Terminating_on_valid.
Definition Terminating_on_ValCxtEq  := proj1 (proj2 Terminating_on_valid).
Definition Terminating_on_ValTp  := proj1 (proj2 (proj2 Terminating_on_valid)).
Definition Terminating_on_ValTm  := proj2 (proj2 (proj2 Terminating_on_valid)).

Lemma Terminating_on_ValCxt1 : forall Gamma, Gamma |= -> exists denv, EvalCxt Gamma denv.
Proof.
intros.
destruct Terminating_on_ValCxt with Gamma; auto.
exists x; intuition.
Qed.


(******************************************************************************
 * Evaluators are defined for well-formed/typed terms
 *)

Lemma Terminating_on_WfCxt: forall Gamma,
  Gamma |-- ->
  exists denv, EvalCxt Gamma denv
.
Proof.
intros.
destruct (proj1 Terminating_on_valid) with Gamma.
apply (proj1 Soundness_model); auto.
exists x; intuition.
Qed.


Lemma ValEnv_from_WfCxt: forall Gamma denv,
  Gamma |-- ->
  EvalCxt Gamma denv ->
  [denv] === [denv] #in# Gamma
.
Proof.
intros.
destruct Terminating_on_ValCxt with Gamma.
apply (proj1 Soundness_model); auto.
intuition.
replace x with denv in *; auto.
eapply EvalCxt_deter; eauto.
Qed.
Hint Resolve ValEnv_from_WfCxt.


Lemma Terminating_on_WfTp: forall Gamma A denv,
  Gamma |-- A ->
  EvalCxt Gamma denv ->
  exists d, EvalTm A denv d
.
Proof.
intros.
assert (Gamma |= A) as HV.
apply (proj1 (proj2 Soundness_model)) ; auto.
inversion HV; subst.

destruct Terminating_on_ValCxt with Gamma as [denv' Hdenv']; eauto.
program_simpl.
replace denv with denv' in *.
destruct Terminating_on_ValTp with Gamma A A denv'; eauto.
eapply EvalCxt_deter; eauto.
Qed.


Lemma Terminating_on_WtTm: forall Gamma t A denv,
  Gamma |-- t : A ->
  EvalCxt Gamma denv ->
  exists d, EvalTm t denv d
.
Proof.
intros.
assert (Gamma |= t : A) as HV.
apply (proj1 (proj2 (proj2 Soundness_model))) ; auto.
inversion HV; subst.
inversion H1; subst.

destruct Terminating_on_ValCxt with Gamma as [denv' Hdenv']; eauto.
program_simpl.
replace denv with denv' in *.
destruct H2 with denv' denv'; auto.
program_simpl.
exists x; auto.
eapply EvalCxt_deter; eauto.
Qed.


(******************************************************************************
 * NBE completeness (semantical version)
 *)

Theorem NBE_DeterminedSem: forall Gamma t t' A v v', 
  Gamma |= t === t' : A ->
  Nbe A Gamma t v ->
  Nbe A Gamma t' v' ->
  v = v'
.
Proof.
intros.
clear_inversion H0.
clear_inversion H1.
assert (denv0 = denv).  eapply EvalCxt_deter; eauto. subst.
assert (DA0 = DA). eapply EvalTm_deter; eauto. subst.
clear_inversion H.
clear_inversion H1.
destruct Terminating_on_ValCxt with Gamma. auto.
destruct H1.
assert (x = denv).  eapply EvalCxt_deter; eauto. subst.
assert (Ddown DA d === Ddown DA d0 #in# PerNf).

destruct H9 with denv denv as [dtm Hdtm]; auto.
destruct Hdtm as [dtm' Hdtm].
destruct Hdtm as [PA Hdtm].
destruct Hdtm as [HPA Hdtm].
destruct Hdtm as [Hdtm Hdtm'].
destruct Hdtm' as [Hdtm' Htt].
destruct HPA as [Da HPA].
destruct HPA as [HDa HPA].
assert (Da = DA).  eapply EvalTm_deter; eauto. subst.
assert (d = dtm). eapply EvalTm_deter; eauto. subst.
assert (d0 = dtm'). eapply EvalTm_deter; eauto. subst.

clear_dups.
destruct H0 with denv denv as [Da HPA']; auto.
destruct HPA' as [Db HPA'].
destruct HPA' as [HDa HPA'].
destruct HPA' as [HDb Hab].
assert (Da = Db). eapply EvalTm_deter; eauto. subst.
assert (Db = DA). eapply EvalTm_deter; eauto. subst.

apply Reify_Characterization with PA; auto.
clear_inversion H10.
specialize H11 with (length Gamma).
destruct H11 as [nf Hnf].
destruct Hnf.

assert (v = nf). eapply RbNf_deter; eauto.
assert (v' = nf). eapply RbNf_deter; eauto.
subst.

reflexivity.
Qed.

Theorem NBE_TerminatingSem: forall Gamma t A,
  Gamma |= t : A ->
  exists v, Nbe A Gamma t v
.
Proof.
intros.

assert (Gamma |= A) as VGA.  clear_inversion H; auto.
assert (Gamma |= ) as VG.  clear_inversion VGA; auto.

destruct Terminating_on_ValCxt with Gamma as [denv Hdenv]; auto.
destruct Hdenv as [Hdenv Henv].
edestruct Terminating_on_ValTm as [d Hd]; eauto.

clear_inversion VGA.

destruct H1 with denv denv as [Da HPA']; auto.
destruct HPA' as [Db HPA'].
destruct HPA' as [HDa HPA'].
destruct HPA' as [HDb Hab].
assert (Da = Db). eapply EvalTm_deter; eauto. subst.
clear_dups. rename HDa into HDb.

clear_inversion H.

destruct H2 with denv denv as [dtm Hdtm]; auto.
destruct Hdtm as [dtm' Hdtm].
destruct Hdtm as [PA Hdtm].
destruct Hdtm as [HPA Hdtm].
destruct Hdtm as [Hdtm Hdtm'].
destruct Hdtm' as [Hdtm' Htt].
destruct HPA as [Da HPA].
destruct HPA as [HDa HPA].
assert (Db = Da).  eapply EvalTm_deter; eauto. subst.
assert (d = dtm). eapply EvalTm_deter; eauto. subst.
assert (dtm' = dtm). eapply EvalTm_deter; eauto. subst.

clear_dups.
assert (Ddown Da dtm === Ddown Da dtm #in# PerNf).
apply Reify_Characterization with PA; auto.

clear_inversion H.
specialize H3 with (length Gamma).
destruct H3 as [nf Hnf].
destruct Hnf.

exists nf.
econstructor 1; eauto.
Qed.



Lemma Helper_REFL: forall Gamma t t' A,
  Gamma |= t === t' : A ->
  Gamma |= t : A
.
Proof.
intros.
assert (Gamma |= t' === t : A).
apply Helper_SYM; auto.

eapply Helper_TRANS; eauto.
Qed.

Theorem NBE_CompletenessSem: forall Gamma t t' A, 
  Gamma |= t === t' : A ->
  exists v,   Nbe A Gamma t v /\   Nbe A Gamma t' v
.
Proof.
intros.

assert (Gamma |= A). clear_inversion H; auto. 
assert (Gamma |= ). clear_inversion H0; auto.
assert (Gamma |= t : A).
eapply Helper_REFL; eauto.
assert (Gamma |= t' : A).
eapply Helper_REFL; eauto.

assert (exists v1, Nbe A Gamma t v1).
apply NBE_TerminatingSem; auto.

assert (exists v2, Nbe A Gamma t' v2).
apply NBE_TerminatingSem; auto.

destruct H4 as [v1 Hv1].
destruct H5 as [v2 Hv2].

assert (v1 = v2).
apply NBE_DeterminedSem with Gamma t t' A; eauto.
subst. exists v2. repeat split; auto.
Qed.

(******************************************************************************
 * NBE completeness (syntactical version)
 *)

Theorem NBE_Determined: forall Gamma t t' A v v', 
  Gamma |-- t === t' : A ->
  Nbe A Gamma t v ->
  Nbe A Gamma t' v' ->
  v = v'
.
Proof.
intros.
eapply NBE_DeterminedSem.
apply Soundness_model; eauto.
eauto.
eauto.
Qed.

Theorem NBE_Terminating: forall Gamma t A,
  Gamma |-- t : A ->
  exists v, Nbe A Gamma t v
.
Proof.
intros.
eapply NBE_TerminatingSem; eauto.
apply Soundness_model; eauto.
Qed.

Theorem NBE_Completeness: forall Gamma t t' A, 
  Gamma |-- t === t' : A ->
  exists v,   Nbe A Gamma t v /\   Nbe A Gamma t' v
.
Proof.
intros.
apply NBE_CompletenessSem.
apply Soundness_model; eauto.
Qed.

