(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Proof of soundness of NbE
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.PER.
Require Import Nbe.Model.Interp.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.IdentityEnvironment.
Require Import Nbe.LogicalRelations.FundamentalTheorem.
Require Import Nbe.CompletenessOfNbe.


(******************************************************************************
 * Main
 *)

Corollary RelType_Refl: forall Gamma T DT denv,
  Gamma |-- T ->
  EvalCxt Gamma denv ->
  EvalTm T denv DT ->
  forall HDT : DT === DT #in# PerType,
  Gamma ||- T #in# HDT
.
Proof.
intros.
apply RelType_ClosedForConversion with (T := TmSb T Sid).
eapply Rel_Fundamental_Tp; eauto.
apply RelSid; eauto.
eauto.
Qed.

Corollary RelTerm_Refl: forall Gamma t T dt DT denv,
  Gamma |-- t : T ->
  EvalCxt Gamma denv ->
  EvalTm T denv DT ->
  EvalTm t denv dt ->
  forall HDT : DT === DT #in# PerType,
  Gamma ||- t : T #aeq# dt #in# HDT
.
Proof.
intros.
apply RelTerm_ClosedForConversion with (t := TmSb t Sid) (T := TmSb T Sid).
eapply RelType_Refl; eauto.
eauto.
eapply Rel_Fundamental_Tm; eauto.
apply RelSid; eauto.
apply EQ_CONV with (A := T).
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Fact Helper_HDT: forall Gamma T denv DT,
  Gamma |-- T ->
  EvalCxt Gamma denv ->
  EvalTm T denv DT ->
  DT === DT #in# PerType
.
Proof.
intros.
assert (Gamma |= T).
apply Valid_for_WfTp.
auto.

clear_inversion H2.
edestruct H4.
apply ValEnv_from_WfCxt; eauto.
program_simpl.
renamer.
compute_sth.
Qed.
Hint Resolve Helper_HDT.

(******************************************************************************
 *)
Theorem Nbe_Soundness: forall Gamma t T n,
  Gamma |-- t : T -> Nbe T Gamma t n ->
  Gamma |-- t === n : T
.
Proof.
intros.
clear_inversion H0.

renamer.
assert (DT_denv === DT_denv #in# PerType) as HDT by eauto.

destruct RelTerm_Reify
  with (Gamma := Gamma) (T := T) (DT := DT_denv) (DT' := DT_denv) (t := t) (dt := Dt_denv)
    (HDT := HDT)
; eauto.
eapply RelType_Refl; eauto.
eapply RelTerm_Refl; eauto.
program_simpl.
assert (x = n).
eapply RbNf_deter; eauto.
subst.
auto.
Qed.

(******************************************************************************
 *)
Corollary Nbe_Soundness': forall Gamma t T,
  Gamma |-- t : T ->
  exists n, Nbe T Gamma t n /\ Gamma |-- t === n : T
.
Proof.
intros.
destruct NBE_Terminating with (Gamma := Gamma) (t := t) (A := T); auto.
exists x; split; auto.
apply Nbe_Soundness; eauto.
Qed.


(******************************************************************************
 *)
Corollary Nbe_NotConvertible: forall Gamma t t' T n n',
  Gamma |-- t : T ->
  Gamma |-- t' : T ->
  ~ Gamma |-- t === t' : T ->
  Nbe T Gamma t n ->
  Nbe T Gamma t' n' ->
  n' <> n
.
Proof.
intros.

intro Heq; subst.
contradict H1.
eapply EQ_TRANS.
apply Nbe_Soundness; eauto.
apply EQ_SYM.
apply Nbe_Soundness; eauto.
Qed.


(******************************************************************************
 *)
Corollary Nbe_DecLogic: forall Gamma t t' T,
  Gamma |-- t : T ->
  Gamma |-- t' : T ->
  Gamma |-- t === t' : T \/ ~ Gamma |-- t === t' : T
.
Proof.
intros.

destruct Nbe_Soundness' with Gamma t T as [n]; auto.
destruct Nbe_Soundness' with Gamma t' T as [n']; auto.
program_simpl.
destruct (Tm_eqdec n n').

(* yes *)
subst.
left.
eapply EQ_TRANS.
eauto.
eauto.

(* no *)

right.
intro Heq.
contradict n0.
eapply NBE_Determined; eauto.
Qed.


