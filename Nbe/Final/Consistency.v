(******************************************************************************
 * Pawel Wieczorek
 *)

(******************************************************************************
 * Consistency of logic
 *)

Require Import Nbe.Utils.
Require Import Nbe.CompletenessOfNbe.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.CompletenessOfNbe.
Require Import Nbe.SoundnessOfNbe.

Require Import Nbe.Final.Nbe.

(******************************************************************************
 *)

Inductive DHasDne : D -> Prop :=
| DHasDne_upx: forall DT dne, DHasDne (DupX DT dne)
| DHasDne_upn: forall dne, DHasDne (DupN dne)
| DHasDne_clo: forall tm denv, DEnvHasDne denv -> DHasDne (Dclo tm denv)
with DEnvHasDne : DEnv -> Prop :=
| DEnvHasDne_head: forall d denv, DHasDne d -> DEnvHasDne (Dext denv d)
| DEnvHasDne_tail: forall d denv, DEnvHasDne denv -> DEnvHasDne (Dext denv d)
.
Hint Constructors DHasDne.
Hint Constructors DEnvHasDne.

(******************************************************************************
 *)
Lemma DNeComesOnlyFromEnvironment: 
  (forall t denv d, EvalTm t denv d -> ~ DEnvHasDne denv -> ~ DHasDne d) /\
  (forall sb denv nenv, EvalSb sb denv nenv -> ~ DEnvHasDne denv -> ~ DEnvHasDne nenv) /\
  (forall d1 d2 d3, App d1 d2 d3 ->  ~ DHasDne d1 ->  ~ DHasDne d2 ->  ~ DHasDne d3).
Proof.

eapply Evals_ind; intros; try intro HH; try solve 
  [ clear_inversion HH; eauto
  | eauto
  | eapply H0; eauto
  | eapply H1; eauto
  | eapply H;  eauto
  ].

{
specialize (H0 H1).
specialize (H H1).
clear_inversion HH; auto.
}

{
apply H; auto.
intro HHH.
clear_inversion HHH; auto.
}
Qed.


(******************************************************************************
 *)
Lemma ComputationInEmptyEnvironmentCannotBringDne: forall t d,
  EvalTm t Did d -> ~ DHasDne d.
Proof.
intros.
destruct DNeComesOnlyFromEnvironment.
eapply H0; eauto.
intro HH.
clear_inversion HH.
Qed.

(******************************************************************************
 *)
Theorem Consistency: 
  ~ exists M, nil |-- M : TmEmpty
.
Proof.
intro H.
destruct H as [M HM].

assert (nil |= M : TmEmpty) as HSemantic.
eapply Valid_for_WtTm; assumption.

clear_inversion HSemantic.
edestruct H0 as [dtm0 [dtm1 [DPA [HX1 [HX2 [HX3 HX4]]]]]].
econstructor 1.

clear_inversion HX1.
clear_inversion H1.
clear_inversion H2.

assert (~ DHasDne dtm0) as HX5.
eapply ComputationInEmptyEnvironmentCannotBringDne.
eauto.

clear_inversion H3.
clear_inversion HX4.
contradict HX5.
unfold Dup.
destruct DisFun_dec.
eapply DHasDne_upx.
destruct DisUnit_dec.
eapply DHasDne_upx.
eapply DHasDne_upn.
Qed.
