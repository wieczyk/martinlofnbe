(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)


Set Implicit Arguments.

(******************************************************************************
 * Extraction Parameters
 *)

Extraction Language Haskell.
Set Extraction Optimize.
Set Extraction AutoInline. 


Extraction Inline proj1_sig.
Extraction Inline False_rect.
Extraction Inline False_rec.
Extraction Inline eq_rect.
Extraction Inline eq_rec.
Extraction Inline eq_rec_r.

Extract Inductive nat => "Prelude.Int" [ "0" "Prelude.succ" ]
  "(\ fO fS n -> if (Prelude.==) n 0 then fO () else fS ((Prelude.-) n 1))".

Extract Constant minus =>
"\a b -> if (Prelude.<=) a b then 0 else (Prelude.-) a b ".

Extract Inductive list => "[]" [ "([])" "(:)" ]
.

Extract Constant length =>
  "Prelude.length"
.

Extraction Inline length.

Extract Inductive sumbool => "Prelude.Bool" [ "Prelude.True" "Prelude.False" ]
.

