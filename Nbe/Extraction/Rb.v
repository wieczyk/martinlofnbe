(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.
Require Import List.
Require Import Arith.
Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Bool.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Extraction.Eval.

(******************************************************************************
 * 
 *)

Inductive RbNf_Dom : nat -> DNf -> Prop :=
| reifyNf_absD: forall f DA m F,
  RbNf_Dom m (DdownN DA) ->
  App_Dom F (Dup DA (Dvar m)) ->
  App_Dom f (Dup DA (Dvar m)) ->
  (forall db DB, App F (Dup DA (Dvar m)) DB -> App f (Dup DA (Dvar m)) db ->
    RbNf_Dom (S m) (Ddown DB db)) ->
  RbNf_Dom m (DdownX (Dfun DA F) f) 

| reifyNf_neD: forall m e,
  RbNe_Dom m e  ->
  RbNf_Dom m (DdownN (DupN e))

| reifyNf_0: forall m,
  RbNf_Dom m (Ddown Dnat D0)

| reifyNf_S: forall m d,
  RbNf_Dom m (Ddown Dnat d) ->
  RbNf_Dom m (Ddown Dnat (DS d))

| reifyNf_1D: forall m n,
  RbNf_Dom m (DdownX Dunit n)

| reifyNf_nat: forall m,
  RbNf_Dom m (DdownN Dnat)

| reifyNf_unit: forall m,
  RbNf_Dom m (DdownN Dunit)

| reifyNf_fun: forall m DA DF ,
  RbNf_Dom m (DdownN   DA) ->
  App_Dom DF (Dup DA (Dvar m)) ->
  (forall DB, App DF (Dup DA (Dvar m)) DB -> RbNf_Dom (S m) (DdownN DB)) ->
  RbNf_Dom m (DdownN $ Dfun DA DF)

| reifyNf_empty: forall m,
  RbNf_Dom m (DdownN Dempty)

| reifyNf_univ: forall m,
  RbNf_Dom m (DdownN Duniv)

with RbNe_Dom: nat -> DNe -> Prop :=
| reifyNe_varD: forall m j,
  RbNe_Dom m (Dvar j)

| reifyNe_appD: forall m e d,
  RbNe_Dom m e ->
  RbNf_Dom m d ->
  (forall ne nd,  RbNe m e ne -> RbNf m d nd ->  RbNe_Dom m (Dapp e d))

| reifyNe_absurdD: forall m u DA,
  RbNf_Dom m DA ->
  RbNe_Dom m u ->
  RbNe_Dom m (DneAbsurd DA u)
.

Hint Constructors RbNf_Dom.
Hint Constructors RbNe_Dom.

(******************************************************************************
 * RbNf and RbNe domain predicate correctness
 *)

Theorem Rbs_Dom_Correctness: 
  (forall m t v, 
    RbNf m t v -> RbNf_Dom m t
  ) /\
  (forall m t v, 
    RbNe m t v -> RbNe_Dom m t
  ) 
.
Proof.

apply Rbs_ind; intros; auto.
(**)
apply reifyNf_absD; eauto.
intros; eauto.
replace db0 with db.
replace DB0 with DB.
auto.
eapply App_deter; eauto.
eapply App_deter; eauto.
(**)
unfold Ddown; simpl; intuition.
(**)
apply reifyNf_fun; intros; eauto. 
replace DB0 with DB; auto.
eapply App_deter; eauto.
(**)
eapply reifyNe_appD; eauto.
Qed.

Definition RbNf_Dom_corr := proj1 (Rbs_Dom_Correctness).
Definition RbNe_Dom_corr := proj2 (Rbs_Dom_Correctness).


(******************************************************************************
 * Inversions for RbNf_Dom
 *)

Definition RbNf_Dom_s_inv:
  forall m d dd,
  RbNf_Dom m d ->
  d = (DdownN (DS dd)) ->
  RbNf_Dom m (DdownN dd)
.
intros m d dd Hdom.
auto_inversion.
intros.
injection H0.
intro H1.
rewrite <- H1.
assumption.
Defined.

Definition RbNf_Dom_abs_inv0:
  forall m d f DA DF,
  RbNf_Dom m d ->
  d = (DdownX (Dfun DA DF) f) ->
  RbNf_Dom m (DdownN DA)
.
intros m d f DA DF H.
auto_inversion.
intros.
injection H4.
intro Hr0.
intro Hr1.
intro Hr2; rewrite <- Hr2.
assumption.
Defined.

Definition RbNf_Dom_abs_inv1:
  forall m d f DA DF,
  RbNf_Dom m d ->
  d = (DdownX (Dfun DA DF) f) ->
  App_Dom DF (Dup DA (Dvar m))
.
intros m d f DA DF H.
auto_inversion.
intros.
injection H4.
intro Hr0.
intro Hr1; rewrite <- Hr1.
intro Hr2; rewrite <- Hr2.
assumption.
Defined.

Definition RbNf_Dom_abs_inv2:
  forall m d f DA DF,
  RbNf_Dom m d ->
  d = (DdownX (Dfun DA DF) f) ->
  App_Dom f (Dup DA (Dvar m))
.
intros m d f DA DF H.
auto_inversion.
intros.
injection H4.
intro Hr0; rewrite <- Hr0.
intro Hr1.
intro Hr2; rewrite <- Hr2.
assumption.
Defined.

Definition RbNf_Dom_abs_inv3:
  forall m d f DA DF,
  RbNf_Dom m d ->
  d = (DdownX (Dfun DA DF) f) ->
  (forall db DB, App DF (Dup DA (Dvar m)) DB -> App f (Dup DA (Dvar m)) db ->
    RbNf_Dom (S m) (Ddown DB db))
.
intros m d f DA DF H.
auto_inversion.
do 9 intro.
injection H4.
intro Hr0; rewrite <- Hr0.
intro Hr1; rewrite <- Hr1.
intro Hr2; rewrite <- Hr2.
assumption.
Defined.

Definition RbNf_Dom_ne_inv:
  forall m e d,
  RbNf_Dom m d ->
  d = DdownN (DupN e) ->
  RbNe_Dom m e
.
intros m e d H.
auto_inversion.
intros.
injection H1.
intro Hr0; rewrite <- Hr0.
assumption.
Defined.

Definition RbNf_Dom_fun_inv1:
  forall m d DA DF,
  RbNf_Dom m d ->
  d = (DdownN $ Dfun DA DF) ->
  RbNf_Dom m (DdownN DA)
.
intros m d DA DF H.
auto_inversion.
intros.
injection H3.
intro Hr0. 
intro Hr1. rewrite <- Hr1.
assumption.
Defined.

Definition RbNf_Dom_fun_inv2:
  forall m d DA DF,
  RbNf_Dom m d ->
  d = (DdownN $ Dfun DA DF) ->
  App_Dom DF (Dup DA (Dvar m))
.
intros m d DA DF H.
auto_inversion.
intros.
injection H3.
intro Hr0. rewrite <- Hr0.
intro Hr1. rewrite <- Hr1.
assumption.
Defined.

Definition RbNf_Dom_fun_inv3:
  forall m d DA DF,
  RbNf_Dom m d ->
  d = (DdownN $ Dfun DA DF) ->
  (forall DB, App DF (Dup DA (Dvar m)) DB -> RbNf_Dom (S m) (DdownN DB))
.
intros m d DA DF H.
auto_inversion.
do 7 intro.
injection H3.
intro Hr0; rewrite <- Hr0.
intro Hr1. rewrite <- Hr1.
assumption.
Defined.

(******************************************************************************
 * Inversions for RbNe_Dom
 *)

Definition RbNe_Dom_app_inv1:
  forall m e' e d,
  RbNe_Dom m e' ->
  e' = Dapp e d ->
  RbNe_Dom m e
.
intros m e' e d H.
auto_inversion.
intros.
injection H4.
intro Hr0.
intro Hr1; rewrite <- Hr1.
assumption.
Defined.

Definition RbNe_Dom_app_inv2:
  forall m e' e d,
  RbNe_Dom m e' ->
  e' = Dapp e d ->
  RbNf_Dom m d
.
intros m e' e d H.
auto_inversion.
intros.
injection H4.
intro Hr0; rewrite <- Hr0.
intro Hr1.
assumption.
Defined.

Definition RbNe_Dom_absurd_inv1:  forall m e DA u,
  RbNe_Dom m e ->
  e =  (DneAbsurd DA u) ->
  RbNf_Dom m DA
.
intros m e DA u He.
auto_inversion.
intros.
injection H1.
intro Hr0.
intro Hr1. rewrite <- Hr1.
assumption.
Defined.


Definition RbNe_Dom_absurd_inv2:  forall m e DA u,
  RbNe_Dom m e ->
  e =  (DneAbsurd DA u) ->
  RbNe_Dom m u
.
intros m e DA u H.
auto_inversion.
intros.
injection H2.
intro Hr0. rewrite <- Hr0.
intro Hr1. 
assumption.
Defined.

(******************************************************************************
 * Functions
 *)

Program Fixpoint rbNe (m:nat) (e:DNe) (H:RbNe_Dom m e) {struct H} :
  { n | RbNe m e n } :=
  match e as E
    return (e = E -> { n | RbNe m E n })
    with
    | Dvar j => fun H' =>
      tmVar (m - S j)

    | Dapp e d => fun H' => 
      let (ne, Hne) := rbNe (RbNe_Dom_app_inv1 H H') in
      let (de, Hde) := rbNf (RbNe_Dom_app_inv2 H H') in
      TmApp ne de

    | DneAbsurd DA u => fun H' =>
      let (A, HA) := rbNf (RbNe_Dom_absurd_inv1 H H') in
      let (n, Hn) := rbNe (RbNe_Dom_absurd_inv2 H H') in
      TmApp (TmAbsurd A) n

  end (eq_refl e)

with rbNf (m:nat) (e:DNf) (H:RbNf_Dom m e) {struct H} :
  { n | RbNf m e n } :=
  match e as E
    return (e = E -> { n | RbNf m E n })
    with

    | DdownX (Dfun DA DB) f => fun H' =>
      let (A,  HA)  := rbNf (RbNf_Dom_abs_inv0 H H') in
      let (DB, HDB) := app  (RbNf_Dom_abs_inv1 H H') in
      let (db, Hdb) := app  (RbNf_Dom_abs_inv2 H H') in
      let (w,  Hw)  := rbNf (RbNf_Dom_abs_inv3 H H' HDB Hdb) in
      TmAbs A w

    | DdownX Dunit _ => fun H' =>
      Tm1

    | DdownN (DupN e) => fun H' =>
      let (v, Hv) := rbNe (RbNf_Dom_ne_inv H H') in
      v

    | DdownN (D0) => fun H' =>
      Tm0

    | DdownN (DS d) => fun H' =>
      let (n, Hn) := rbNf (RbNf_Dom_s_inv H H') in
      TmS n

    | DdownN Dnat => fun H' =>
      TmNat

    | DdownN Dunit => fun H' =>
      TmUnit

    | DdownN Dempty => fun H' =>
      TmEmpty

    | DdownN Duniv => fun H' =>
      TmUniv

    | DdownN (Dfun DA DF) => fun H' =>
      let (A, HA)   := rbNf (RbNf_Dom_fun_inv1 H H') in
      let (DB, HDB) := app  (RbNf_Dom_fun_inv2 H H') in
      let (F, HF)   := rbNf (RbNf_Dom_fun_inv3 H H' HDB) in
        TmFun A F

    | _ => _
     
  end (eq_refl e)
.

(*Obligations of rbNe.*)

(*Obligations of rbNf.*)
(*
Obligation 18 of rbNf.
intros.
replace (DdownN (Dnv n)) with (Ddown Dnat (Dnv n)).
auto.
reflexivity.
Defined.
*)
(*
Obligations of rbNe.

Obligations of rbNf.
*)

(******************************************************************************
 * Extraction parameters
 *)

Extraction Inline rbNe_obligation_1.
Extraction Inline rbNe_obligation_2.
Extraction Inline rbNe_obligation_3.

Extraction Inline rbNf_obligation_1.
Extraction Inline rbNf_obligation_2.
Extraction Inline rbNf_obligation_3.
Extraction Inline rbNf_obligation_4.
Extraction Inline rbNf_obligation_5.
Extraction Inline rbNf_obligation_6.
Extraction Inline rbNf_obligation_7.
Extraction Inline rbNf_obligation_8.
Extraction Inline rbNf_obligation_9.
Extraction Inline rbNf_obligation_10.
Extraction Inline rbNf_obligation_11.
Extraction Inline rbNf_obligation_12.
Extraction Inline rbNf_obligation_13.
Extraction Inline rbNf_obligation_14.
Extraction Inline rbNf_obligation_15.
Extraction Inline rbNf_obligation_16.
Extraction Inline rbNf_obligation_17.
Extraction Inline rbNf_obligation_18.
Extraction Inline rbNf_obligation_19.
Extraction Inline rbNf_obligation_20.
Extraction Inline rbNf_obligation_21.
Extraction Inline rbNf_obligation_22.
Extraction Inline rbNf_obligation_23.
Extraction Inline rbNf_obligation_24.
