(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments (types)
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.


(******************************************************************************
 *)
Lemma Helper_SB_F: forall Gamma Delta A S,
  Gamma |= [S] : Delta ->
  Delta |= A ->
  Gamma |= TmSb A S
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
compute_sth; apply_valenvs; program_simpl; eauto.
apply_valenvs; program_simpl.
do 2 eexists; split; eauto.
Qed.
Hint Resolve Helper_SB_F.

(******************************************************************************
 *)
Lemma Helper_SB: forall Gamma Delta M A S,
  Gamma |= [S] : Delta ->
  Delta |= M : A ->
  Gamma |= TmSb M S : TmSb A S
.
Proof.

constructor 1; auto; intros; extract_vals; reds; auto.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs.
program_simpl; renamer.
apply_valenvs.
program_simpl; renamer.
do 2 eexists. repeat split; eauto.

compute_sth.
apply_valenvs.
program_simpl; renamer.
apply_valenvs.
program_simpl; renamer.
do 3 eexists. repeat split; eauto.
red.
do 1 eexists. repeat split; eauto.
red in H12.
program_simpl; renamer. compute_sth.
Qed.
Hint Resolve Helper_SB.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_CONG_SB: forall Gamma Delta A1 A2 S1 S2, 
  Gamma |=  [S1]=== [S2]:Delta ->
  Delta |= A1 === A2 ->
  Gamma |= TmSb A1 S1 === TmSb A2 S2
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.
exists DA1_DS1_env0; exists DA2_DS2_env1.
repeat split; eauto.
Qed.
Hint Resolve Soundness_of_EQ_TP_CONG_SB.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_SBSEQ: forall Gamma A S1 S2 GammaS1 GammaS2,
  Gamma |=  [S1]:GammaS1 ->
  GammaS1 |=  [S2]:GammaS2 ->
  GammaS2 |= A ->
  Gamma |= TmSb (TmSb A S2) S1 === TmSb A (Sseq S2 S1)
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.

exists DA_DS2_DS1_env0. exists DA_DS2_DS1_env1.
repeat split; eauto.
Qed.
Hint Resolve Soundness_of_EQ_TP_SBSEQ.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_SBSID: forall Gamma A,
  Gamma |= A ->
  Gamma |= TmSb A Sid === A
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_EQ_TP_SBSID.

