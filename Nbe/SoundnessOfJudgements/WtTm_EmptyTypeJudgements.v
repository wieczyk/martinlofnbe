(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for empty type (terms)
 *)
Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

Require Import Nbe.SoundnessOfJudgements.WfTp_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_FunTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_GeneralJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONG_ABSURD: forall Gamma A1 A2,
   Gamma |= A1 === A2 ->
   Gamma |=  TmAbsurd A1 === TmAbsurd A2 : TmArr TmEmpty A1
.
Proof.
intros.

assert (Gamma |= TmArr TmEmpty A1) as HX1.
apply Helper_FUN_ARR; auto.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
eauto.

assert (Gamma |= TmEmpty) as HX2.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.

constructor 1.
eapply Helper_FUN_ARR; eauto.
intros.

clear_inversion HX1.
specialize (H2 _ _ H0).
program_simpl.

clear_inversion H.
specialize (H8 _ _ H0).
program_simpl.

renamer.

clear_inversion H4.
clear_inversion H5.
clear_inversion H13.
clear_inversion H11.

edestruct InterpType_dom_PerType with (d := DA1_env0) as [PDA1 HPDA]; eauto.
edestruct InterpType_dom_PerType with (d :=  Dfun Dempty (Dclo (TmSb A1 Sup) env0)) as [PDFUN HPDFUN]; eauto.

exists (Dabsurd DA1_env0).
exists (Dabsurd DA2_env1).
exists PDFUN.

repeat split.
eexists. repeat split; eauto.
eapply evalFun.
eauto.
eapply evalAbsurd.
eauto.
eapply evalAbsurd.
eauto.

(* . *)

clear_inversion_of_interptype HPDFUN.
clear_inversion H3.
constructor 1.
intros.

clear_inversion po_ro.

assert (exists B, PF a0 B) as HX3.
eapply ro_resp_ex.
eauto.

destruct HX3.
renamer.
rename x into PDB.
 
clear_inversion H2.

clear_inversion H.

set (dne0 := DneAbsurd (DdownN DA1_env0) e).
set (dne1 := DneAbsurd (DdownN DA2_env1) e').

set (y0 := Dup DA1_env0 dne0).
set (y1 := Dup DA2_env1 dne1).

exists y0.
exists y1.
exists PDB.

subst y0.
subst y1.
subst dne0.
subst dne1.
repeat split.
eauto.
unfold Dup at 1; simpl.
eauto.
unfold Dup at 1; simpl.
eauto.

eapply Reflect_Characterization.
eauto.

clear_inversion_of_pertype H6.
clear_inversion H11.
clear_inversion H8.
eapply H4.
constructor 1.
eapply Per_domL.
eauto.
eauto.
eauto.
eauto.

constructor 1.
intros.
destruct H2.
destruct H with m.
destruct H2.

assert (DdownN DA1_env0 === DdownN DA2_env1 #in# PerNf) as HPnf.
eapply ReifyTp_Characterization.
eauto.
eauto.

destruct HPnf.
destruct H11 with m.
destruct H12.

eexists.
split.
eauto.
eauto.

Qed.
Hint Immediate Soundness_of_EQ_CONG_ABSURD.

(******************************************************************************
 *)
Lemma Soundness_of_EMPTY_E: forall Gamma A,
  Gamma |= A ->
  Gamma |= TmAbsurd A : TmArr TmEmpty A
.
Proof.
intros.
eauto.
Qed.
Hint Immediate Soundness_of_EMPTY_E.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBABSURD: forall Gamma S Delta A,
  Gamma |=  [S]:Delta ->
  Delta |= A ->
  Gamma |=  TmSb (TmAbsurd A) S === TmAbsurd (TmSb A S) : TmArr TmEmpty (TmSb A S)
.
Proof.

intros.


assert (Gamma |= TmArr TmEmpty (TmSb A S)).
apply Helper_FUN_ARR; auto.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
eauto.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.

clear_inversion H19.
clear_inversion H17.

exists (Dabsurd DA_DS_env0).
exists (Dabsurd DA_DS_env1).

assert (exists PF, InterpType ( Dfun Dempty (Dclo (TmSb (TmSb A S) Sup) env0) ) PF) as HPF.
eauto.


destruct HPF as [PF HPF].

exists PF.
repeat (split; auto).
exists ( Dfun Dempty (Dclo (TmSb (TmSb A S) Sup) env0)); split; auto.

eapply evalFun; auto.
eapply evalSb; eauto.
eapply evalAbsurd; eauto.

clear_inversion_of_interptype HPF.
apply RelProd_intro.
intros.
clear_inversion H11.

clear_inversion po_ro.
destruct ro_resp_ex with a0 as [PDB HPDB].
eauto.

destruct H16 with a0 as [DB HDB].
eauto.

assert (InterpType DB PDB).
eauto.

clear_inversion H0.
clear_inversion H.

unfold Dup; simpl.

exists ((Dup DA_DS_env0 (DneAbsurd (DdownN DA_DS_env0) e))).
exists ((Dup DA_DS_env1 (DneAbsurd (DdownN DA_DS_env1) e'))).
exists PDB.

repeat (split; auto).

clear_inversion HDB.
clear_inversion H20.
clear_inversion H18.
clear_inversion H22.
compute_sth.

apply Reflect_Characterization; auto.
apply PerNe_intro; intros.
clear_inversion H0.
destruct H with m as [na [Hna1 Hna2] ].

assert (DdownN DA_DS_env0 === DdownN DA_DS_env1 #in# PerNf) as HNFA.
eapply ReifyTp_Characterization.
auto.
eauto.

destruct HNFA.
destruct H0 with m as [nA [HA1 HA2]].
exists (TmApp (TmAbsurd nA) na).
split; auto.
Qed.
Hint Immediate Soundness_of_EQ_SBABSURD.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_SEQ: forall Gamma S1 S2 GammaS1 GammaS2 M A,
  Gamma |=  [S1]:GammaS1 ->
  GammaS1 |=  [S2]:GammaS2 ->
  GammaS2 |= M : A ->
  Gamma |=  TmSb (TmSb M S2) S1 === TmSb M (Sseq S2 S1) : TmSb (TmSb A S2) S1
.
Proof.
intros.
assert (GammaS1 |= TmSb A S2) by (inversion H1; eauto).
assert (Gamma |= TmSb (TmSb A S2) S1) by eauto.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.

do 3 eexists; repeat split; eauto.
red; eexists; repeat split; eauto.
clear_inversion H12; clear_inversion H13.
clear_inversion H20; clear_inversion H21. 
program_simpl; compute_sth; reds; renamer.
clear_inversion H33; clear_inversion H39.
program_simpl; compute_sth; reds; renamer.
Qed.
Hint Immediate Soundness_of_EQ_SB_SEQ.
