(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Auxiliary tactics used in Soundness of Judgements
 *)

Set Implicit Arguments.
Require Import Program.Tactics.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.

(******************************************************************************
 * Tactics
 *)

 
Ltac extract_val :=
  match goal with
    | [ H : ?Gamma,,?A |=  |- _ ] => 
      clear_inversion H
    | [ H : ?Gamma |= ?A === ?B  |- _ ] =>
      clear_inversion H
    | [ H : ?Gamma |= ?t === ?s : ?A  |- _ ] =>
      clear_inversion H
    | [ H : ?Gamma |= [?t] === [?s] : ?Delta |- _ ] =>
      clear_inversion H
    | [ H : [?p] === [?p'] #in# ?Gamma,,?A |- _ ] =>
      clear_inversion H
    | [ H : [Dext ?p ?d] === [Dext ?p' ?d'] #in# ?Gamma |- _ ] =>
      clear_inversion H
    | [ H : |= {?Gamma} === {?Delta}  |- _ ] =>
      clear_inversion H
  end
.

Ltac extract_vals := 
  repeat progress extract_val
.

Ltac reds :=
  repeat
    match goal with
      | [ H : InterpTypePer ?A ?B ?C |- _ ] => red in H; program_simpl
    end
.

Ltac isDomainTp H :=
  match eval compute in H with
    | Dnat => true
    | Dunit => true
    | Dfun ?A ?B => false
    | _ => false
  end
.


Ltac isConcreteTm H :=
  match eval compute in H with
    | TmUnit => true
    | TmS => true
    | Tm0 => true
    | TmApp ?A ?B => true
    | TmAbs ?A => true
    | TmSb ?A => true
    | TmVar => true
    | TmNat => true
    | Tm1 => true
    | TmUnit => true
    | TmFun ?A ?B => true
    | _ => false
  end
.

Ltac isConcreteSb H :=
  match eval compute in H with
    | Sid => true
    | Sext ?S ?M => true
    | Sseq ?A ?B => true
    | Sup => true
    | _ => false
  end
.

Ltac run_eval :=
  match goal with
    | [ H : EvalTm ?A ?env ?D |- _ ] =>
      let r := isConcreteTm A in
      match r with
        | true => clear_inversion H
        | false => fail 
      end

    | [ H : EvalSb ?A ?env ?D |- _ ] =>
      let r := isConcreteSb A in
      match r with
        | true => clear_inversion H
        | false => fail 
      end

    | _ => fail
  end
.

Ltac run_evals :=
  repeat progress run_eval
.
Require Import Bool.

Ltac invert_interp :=
  match goal with
    | [ H : InterpType ?Dm ?Pa |- _ ] =>
      let rDm := isDomainTp Dm in
      match eval compute in rDm  with
        | true => clear_inversion H
        | false => fail
      end

    | [ H : InterpType (Dfun ?DA ?DF) ?Pa |- _ ] =>
        clear_inversion_of_interptype H
  end
.


Ltac invert_interps :=
  repeat progress invert_interp
.

Ltac guard_noeq A B :=
  match A with
    | B => fail 1
    | _ => idtac
  end
.

Ltac guard_eq A B :=
  match A with
    | B => idtac
    | _ => fail 1
  end
.

Ltac elim_deter :=
  match goal with

    | [ H1 : EvalTm ?A ?B ?R1 |- _ ] =>
      match goal with
        | [ H2 : EvalTm A B ?R2 |- _ ] =>
          guard_noeq R1 R2;
          replace R1 with R2 in *;
            [
              idtac; eauto
            | eapply EvalTm_deter; eauto
            ]
      end

    | [ H1 : EvalSb ?A ?B ?R1 |- _ ] =>
      match goal with
        | [ H2 : EvalSb A B ?R2 |- _ ] =>
          guard_noeq R1 R2;
          replace R1 with R2 in *;
            [
              idtac; eauto
            | eapply EvalSb_deter; eauto
            ]
      end
  end
.

Ltac elim_extdeter :=
  match goal with
    | [ H1: InterpType ?A ?PA |- _ ] =>
      match goal with
        | [ H2: InterpType A ?C |- _ ] =>
          guard_noeq PA C;
          assert (PA =~= C) by (eapply InterpType_extdeter; eauto)
      end
  end
.

Ltac elim_deters := 
  repeat progress elim_deter
.

Ltac compute_sth :=
  repeat progress (run_evals; elim_deters; invert_interps);
  clear_dups
.

Ltac with_valenv CONT := 
  match goal with
    | [ Henv : [?env0] === [?env1] #in# ?Gamma |- _ ] =>
      CONT env0 env1 Gamma Henv
  end
.


Ltac apply_valenv_ env0 env1 Gamma Henv := 
  match goal with 
    | [Ha: forall e0 e1, [e0] === [e1] #in# Gamma -> ?A |- _] =>
      let Ha' := fresh Ha in
      generalize Ha; intro Ha';
      specialize (Ha' env0 env1 Henv);
      generalize dependent Ha;
      set (Ha := 42);
      (try apply_valenv_ env0 env1 Gamma Henv);
      clear Ha; intro Ha;
      idtac
  end
.

Ltac apply_valenvs := 
  repeat (progress (with_valenv apply_valenv_; clear_dups))
.

  Ltac renamer :=
  let TMP := fresh "TMP" in
  match goal with

    | [ H1 : EvalTm ?A (Dext ?D ?d) ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "D" A "_" D "_" d in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac


     | [ H1 : EvalSb ?A (Dext ?D ?d) ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "D" A "_" D "_" d in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac

    | [ H1 : EvalTm ?A ?B ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "D" A "_" B in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac

    | [ H1 : EvalSb ?A ?B ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "D" A "_" B in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac

    | [ H1 : InterpUniv ?A ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "P" A in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac

    | [ H1 : InterpType ?A ?R1 |- _ ] =>
      rename R1 into TMP;
      let R1' := fresh "P" A in
      rename TMP into R1';
      generalize dependent H1;
      set (H1 := 42);
      (try renamer);
      clear H1; intro H1;
      idtac

  end
.


Ltac find_extdeter :=
  match goal with
    | [ H0 : InterpType ?DA ?PA0  |- _ ] =>
      match goal with
        |  [ H1 : InterpType DA ?PA1  |- _ ] =>
          guard_noeq PA0 PA1;
          match goal with
            | [ HE : PA0 =~= PA1 |- _ ] => fail 1
            | [ HE : PA1 =~= PA0 |- _ ] => fail 1
            | _ =>
              progress (
                assert (PA0 =~= PA1) by eauto;
                clear_dups
              )
          end
      end
  end
.

Ltac find_extdeters := repeat find_extdeter.

Ltac close_extdeter :=
  match goal with
    | [ HE : ?PA =~= ?PB  |- _ ] =>
      match goal with 
        | [He : ?a === ?b #in# PA |- _ ] =>
          progress (
            assert (a === b #in# PB) by (apply HE; eauto);
            clear_dups
          )
        | [He : ?a === ?b #in# PB |- _ ] =>
          progress (
            assert (a === b #in# PA) by (apply HE; eauto);
            clear_dups
          )
      end
  end
.

Ltac close_extdeters := repeat close_extdeter.

Ltac close_dom :=
  match goal with
    | [ HE : ?a === ?b #in# ?P  |- _ ] =>
      guard_noeq a b;
      progress (
        assert (a === a #in# P) by eauto;
        assert (b === b #in# P) by eauto;
        clear_dups
      )
  end
.

Ltac close_doms := repeat close_dom.

Ltac move_inpers := 
  match reverse goal with
    | [ H : ?a === ?b #in# ?PA |- _ ] =>
      guard_noeq a b;
      generalize dependent H; move_inpers; intro

    | [ H : ?a === ?b #in# ?PA |- _ ] =>
      guard_eq a b;
      generalize dependent H; set (H := 42);
      move_inpers; clear H; intro H; move H at top

    | [ |- _ ] => 
      idtac
  end
.

Ltac build_valenv Gamma A env0 env1 d0 d1 :=
  assert ([Dext env0 d0] === [Dext env1 d1] #in# Gamma,,A)
    by (eapply ValEnv_ext; eauto; red; eexists; repeat split; eauto)
.
