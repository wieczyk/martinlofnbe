(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for Pi-type (types)
 *)
Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_CONG_FUN: forall Gamma A A' B B',
  Gamma |= A === A' ->
  Gamma,, A |= B === B' ->
  Gamma |= TmFun A B === TmFun A' B'
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.

exists (Dfun DA_env0 (Dclo B env0)).
exists (Dfun DA'_env1 (Dclo B' env1)).
repeat split; eauto.

assert (exists PA, InterpType DA_env0 PA) by eauto.
program_simpl; renamer.
apply PerType_fun with PDA_env0; eauto.

(**)
intros.
assert ([Dext env0 a] === [Dext env1 a] #in# Gamma,,A).
apply ValEnv_ext with PDA_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; renamer.
exists DB_env0_a.
intuition.

(**)
intros.
assert ([Dext env0 a] === [Dext env1 a] #in# Gamma,,A).
apply ValEnv_ext with PDA_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; renamer.
exists DB'_env1_a.
intuition.

(**)

intros; renamer.
clear_inversion H7.
clear_inversion H10.
renamer.
intros.
assert ([Dext env0 a0] === [Dext env1 a1] #in# Gamma,,A).
apply ValEnv_ext with PDA_env0; auto.
red; eexists; repeat split; eauto.
find_extdeters; close_extdeters; assumption.
apply_valenvs; program_simpl; renamer.
compute_sth.
Qed.
Hint Resolve Soundness_of_EQ_TP_CONG_FUN.


(******************************************************************************
 *)
Lemma Helper_FUN_1: forall Gamma A B,
  Gamma,,A |= B ->
  Gamma    |= TmFun A B
.
Proof.
intros.

assert (Gamma |= A).
clear_inversion H.
clear_inversion H0.
eauto.

eauto.
Qed.
Hint Resolve Helper_FUN_1.


(******************************************************************************
 *)
Lemma Helper_FUN_2: forall Gamma A B M,
  Gamma,,A |= M : B ->
  Gamma    |= TmAbs A M : TmFun A B
.
Proof.
intros.

assert (Gamma |= TmFun A B) as HAB.
apply Helper_FUN_1; auto.
extract_vals; reds; auto.

constructor 1; auto; intros; extract_vals; reds; auto.

compute_sth; apply_valenvs.
program_simpl.
renamer.
reds.
compute_sth.
exists (Dclo M env0).
exists (Dclo M env1).



assert (exists PR, InterpType ( Dfun DA_env0 (Dclo B env0)) PR) as HPR.
apply InterpType_dom_PerType; eauto with *.
destruct HPR as [PR HPR].
exists PR; repeat split; auto.
eexists; eauto.
(**)
renamer.
inversion_of_interptype HPR; subst.
constructor 1.
(**)
intros.
assert ([Dext env0 a0] === [Dext env1 a1] #in# Gamma,,A) as HEnv.
constructor 2 with PA; auto.
exists DA_env0; repeat split; auto.
apply_valenvs; program_simpl; renamer.
destruct H7; destruct po_ro.
exists DM_env0_a0; exists DM_env1_a1.

destruct ro_resp_ex with a0 as [Y HY]; eauto.
exists Y. repeat split; auto. 

assert (InterpType DB_env0_a0 Y); auto.
apply H8 with a0; eauto.
red in H18. destruct H18 as [DD HDD].
destruct HDD.
elim_deter. clear DB_env0_a0. renamer.
assert (PDB_env0_a1 =~= PDB_env0_a0); eauto.
apply H16. auto.
Qed.
Hint Resolve Helper_FUN_2.

(******************************************************************************
 *)
Lemma Helper_FUN_3: forall Gamma A B,
  Gamma    |= TmFun A B ->
  Gamma,,A |= B
.
Proof.
intros.

constructor 1; auto; intros; extract_vals; reds; auto.
constructor 2; eauto.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
clear_inversion_of_pertype H6.
do 2 eexists; repeat split; eauto.
program_simpl.

apply_valenvs; program_simpl; compute_sth; renamer.
clear_inversion_of_pertype H10.
program_simpl; compute_sth; renamer.
find_extdeters. close_extdeters. close_doms. move_inpers.

destruct H7 with d0; eauto.
destruct H9 with d1; eauto.
clear_inversion H19; clear_inversion H20.
program_simpl; compute_sth; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Resolve Helper_FUN_3.

(******************************************************************************
 * Helper for non-dependent type NAT => NAT
 *)
Lemma Helper_FUN_NAT_NAT: forall Gamma,
  Gamma |= ->
  Gamma |= TmArr TmNat TmNat
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.


exists (Dfun Dnat $ Dclo (TmSb TmNat Sup) env0).
exists (Dfun Dnat $ Dclo (TmSb TmNat Sup) env1).
repeat split; unfold TmArr; auto.

eapply PerType_fun with PerNat; auto.
intros; exists Dnat; eauto. 
intros; exists Dnat; eauto.
intros.
clear_inversion H3.
clear_inversion H4.
clear_inversion H9.
clear_inversion H8.
compute_sth.
auto.
Qed.
Hint Resolve Helper_FUN_NAT_NAT.

(******************************************************************************
 *)
Lemma Helper_FUN_ARR: forall Gamma A B,
  Gamma |= A ->
  Gamma |= B ->
  Gamma |= TmArr A B
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.
compute_sth; apply_valenvs; program_simpl; eauto.
apply_valenvs; program_simpl; reds; compute_sth; renamer.

exists (Dfun DA_env0 $ Dclo (TmSb B Sup) env0).
exists (Dfun DA_env1 $ Dclo (TmSb B Sup) env1).
unfold TmArr.
repeat split; eauto.

assert (exists PA, InterpType DA_env0 PA) by eauto.
program_simpl; reds; compute_sth; renamer.

apply PerType_fun with PDA_env0; eauto.

intros.
clear_inversion H9; clear_inversion H13.
clear_inversion H17; clear_inversion H18.
program_simpl; reds; compute_sth; renamer.
Qed.
Hint Resolve Helper_FUN_ARR.

(******************************************************************************
 *)
Lemma Helper_APP: forall Gamma A B N,
  Gamma |= TmFun A B ->
  Gamma |= N : A ->
  Gamma |= TmSb B (Ssing N)
.
Proof.
intros.
unfold Ssing.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
destruct H13. program_simpl; compute_sth; renamer.
assert ([Dext env0 DN_env0] === [Dext env1 DN_env1] #in# Gamma,,A) as HEnv.
constructor 2 with PDA_env0; eauto.
eexists; eauto.

clear_inversion_of_pertype H10; renamer.
clear_inversion H6.

assert (PDA_env0 =~= PDA_env1) as HX1; eauto.

destruct H9 with DN_env0 as [a0 Ha0]; eauto.
apply HX1; eauto.
eapply Per_domL; eauto.

destruct H11 with DN_env1 as [a1 Ha1]; eauto.
apply HX1; eauto.
eapply Per_domL; eauto.
exists a0; exists a1; repeat split; auto.

apply evalSb with (Dext env0 DN_env0); eauto.
clear_inversion Ha0; auto.

apply evalSb with (Dext env1 DN_env1); eauto.
clear_inversion Ha1; auto.
eauto.
Qed.
Hint Resolve Helper_APP.

(******************************************************************************
 *)
Lemma Helper_FUN_SB: forall Gamma A B S Delta,
  Gamma     |= [S] : Delta ->
  Delta,, A |= B ->
  Delta     |= A ->
  Gamma     |= TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar))
.
Proof.
intros.

constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.
exists (Dfun DA_DS_env0 $ Dclo (TmSb B (Sext (Sseq S Sup) TmVar)) env0).
exists (Dfun DA_DS_env1 $ Dclo (TmSb B (Sext (Sseq S Sup) TmVar)) env1).
repeat split; eauto.


assert (exists PA, InterpType DA_DS_env0 PA) by eauto.
program_simpl; compute_sth; renamer.
apply PerType_fun with PDA_DS_env0; auto.

(**)

intros.
assert ( [Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
exists DB_DS_env0_a. intuition.
apply appClo.
eapply evalSb; eauto.

(**)

intros.
assert ( [Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
exists DB_DS_env1_a. apply appClo.
eapply evalSb; eauto.

(**)

intros.
assert ( [Dext DS_env0 a0] === [Dext DS_env1 a1] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
find_extdeters. close_extdeters. auto.
apply_valenvs; program_simpl; compute_sth; renamer.


inversion_clear H14.
inversion_clear H15.

inversion_clear H17.
inversion_clear H14.
program_simpl; compute_sth; renamer.
Qed. 
Hint Resolve Helper_FUN_SB.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_SB_FUN: forall Gamma Delta S A B,
  Gamma |=  [S]:Delta ->
  Delta |= A ->
  Delta,, A |= B ->
  Gamma |= TmSb (TmFun A B) S ===  TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar))
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.

exists (Dfun DA_DS_env0 $ Dclo B DS_env0 ).
exists (Dfun DA_DS_env1 $ Dclo (TmSb B (Sext (Sseq S Sup) TmVar)) env1).
repeat split; eauto.

assert (exists PA, InterpType DA_DS_env0 PA) by eauto.
program_simpl; compute_sth; renamer.
apply PerType_fun with PDA_DS_env0; auto.

(**)

intros.
assert ( [Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
exists DB_DS_env0_a. intuition.

(**)

intros.
assert ( [Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
exists DB_DS_env1_a. apply appClo.
eapply evalSb; eauto.

(**)

intros.
assert ( [Dext DS_env0 a0] === [Dext DS_env1 a1] #in# Delta,,A ).
apply ValEnv_ext with PDA_DS_env0; auto.
red; eexists; repeat split; eauto.
find_extdeters; close_extdeters; assumption.
apply_valenvs; program_simpl; compute_sth; renamer.
inversion_clear H14.
inversion_clear H15.
inversion_clear H14.
compute_sth.
Qed.
Hint Resolve Soundness_of_EQ_TP_SB_FUN.
