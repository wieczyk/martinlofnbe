(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for unit type (types)
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.
Require Import Nbe.SoundnessOfJudgements.WfTp_UnivTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_UnivTypeJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_UNIV_UNIT_F: forall Gamma,
  Gamma |= ->
  Gamma |= TmUnit: TmUniv
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
exists Dunit.
exists Dunit.
exists PerUniv.
split; auto.
exists Duniv.
split; auto.
Qed.
Hint Resolve Soundness_of_UNIV_UNIT_F.


(******************************************************************************
 *)
Lemma Soundness_of_UNIT_F: forall Gamma,
  Gamma |= ->
  Gamma |= TmUnit
.
Proof.
intros.
eapply Soundness_of_UNIV_ELEM.
eauto.
Qed.
Hint Resolve Soundness_of_UNIT_F.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_SB_UNIT: forall Gamma Delta sb,
  Gamma |=  [sb]:Delta ->
  Gamma |= TmSb TmUnit sb === TmUnit : TmUniv
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
do 2 eexists; exists PerUniv.
repeat split; eauto.
do 1 eexists; eauto.
Qed.
Hint Resolve Soundness_of_EQ_TP_SB_UNIT.


