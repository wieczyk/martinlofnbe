(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgements for natural numbers (types)
 *)
Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.
Require Import Nbe.SoundnessOfJudgements.WfTp_UnivTypeJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_NAT_F: forall Gamma,
  Gamma |= ->
  Gamma |= TmNat: TmUniv
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
exists Dnat.
exists Dnat.
exists PerUniv.
split; auto.
exists Duniv.
split; auto.
Qed.
Hint Resolve Soundness_of_NAT_F.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_TP_SB_NAT: forall Gamma Delta sb,
  Gamma |=  [sb]:Delta ->
  Gamma |= TmSb TmNat sb === TmNat : TmUniv
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
do 2 eexists; exists PerUniv; repeat split; eauto.
exists Duniv; split; auto.
Qed.
Hint Resolve Soundness_of_EQ_TP_SB_NAT.

