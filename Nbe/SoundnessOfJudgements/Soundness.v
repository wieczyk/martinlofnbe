(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Proof of soundness of judgements
 *
 * All cases are moved to lemmas in imported modules
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.
Require Import Nbe.SoundnessOfJudgements.WfTp_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtSb_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WfCxt_Judgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_FunTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_FunTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_NatTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_NatTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_UnitTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_UnitTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_EmptyTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_EmptyTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_UnivTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_UnivTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_GeneralJudgements.
Require Import Nbe.SoundnessOfJudgements.WtSb_GeneralJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_GeneralJudgements.


(******************************************************************************
 * Soundness for model
 *)

Theorem Soundness_model:
  (forall Gamma,
    Gamma |-- ->
    Gamma |= ) /\
  (forall Gamma A,
    Gamma |-- A ->
    Gamma |= A) /\
  (forall Gamma t A,
    Gamma |-- t : A ->
    Gamma |= t : A) /\
  (forall Gamma sb Delta,
    Gamma |-- [sb] : Delta ->
    Gamma |= [sb] : Delta) /\
  (forall Gamma Delta,
    |-- {Gamma} === {Delta} ->
    |= {Gamma} === {Delta}) /\
  (forall Gamma  A B,
    Gamma |-- A === B ->
    Gamma |= A === B) /\
  (forall Gamma t0 t1 A,
    Gamma |-- t0 === t1 : A ->
    Gamma |= t0 === t1 : A) /\
  (forall Gamma sb0 sb1 Delta,
    Gamma |-- [sb0] === [sb1] : Delta ->
    Gamma |= [sb0] === [sb1] : Delta)
.
Proof.
apply System_ind; intros; eauto 2.
Qed.

Definition Valid_for_WfCxt := proj1 Soundness_model.
Definition Valid_for_WfTp  := proj1 (proj2 Soundness_model).
Definition Valid_for_WtTm  := proj1 (proj2 (proj2 Soundness_model)).
Definition Valid_for_WtSb  := proj1 (proj2 (proj2 (proj2 Soundness_model))).
Definition Valid_for_WfCxtEq  := proj1 (proj2 (proj2 (proj2 (proj2 Soundness_model)))).
Definition Valid_for_WfTpEq  := proj1 (proj2 (proj2 (proj2 (proj2 (proj2 (Soundness_model)))))).
Definition Valid_for_WtTmEq  := proj1 (proj2 (proj2 (proj2 (proj2 (proj2 (proj2 Soundness_model)))))).
Definition Valid_for_WtSbEq  := proj2 (proj2 (proj2 (proj2 (proj2 (proj2 (proj2 Soundness_model)))))).

