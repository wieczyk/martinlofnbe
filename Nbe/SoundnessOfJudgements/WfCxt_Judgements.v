(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgements for contexts
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.


(******************************************************************************
 *)
Lemma Valid_EQ_CXT_REFL: forall Gamma,
  Gamma |= ->
  |= {Gamma} === {Gamma}.
Proof.
intros.
clear_inversion H.
econstructor 1; intros; eauto.
split; intros; eauto.

econstructor 1; intros; eauto.
split; intros; eauto.
Qed.
Hint Resolve Valid_EQ_CXT_REFL.

(******************************************************************************
 *)
Lemma Valid_EQ_CXT_EXT: forall Gamma A Delta B,
  Gamma |= A ->
  Delta |= B ->
  Gamma |= A === B ->
  |= {Gamma} === {Delta} ->
  |= {Gamma,,A} === {Delta,,B}
.
Proof.
intros.
econstructor 1; intros.

econstructor 2.
clear_inversion H.
eauto.
eauto.

econstructor 2.
clear_inversion H0.
eauto.
eauto.


split; intros.

clear_inversion H3.

assert ( [env2] === [env3] #in# Delta ) as HX1.
clear_inversion H2.
eapply H5.
eauto.
(**)
econstructor 2; eauto.
red.

assert ( [env3] === [env2] #in# Gamma ) as HX2.
clear_inversion H.
assert (PER (ValEnv Gamma)) as HX3.
eauto.
symmetry.
eauto.


assert ( [env2] === [env2] #in# Gamma) as HX3.
assert (PER (ValEnv Gamma)) as HX4.
clear_inversion H.
eauto.

eapply PER_Transitive.
eauto.
eauto.


assert ( [env2] === [env2] #in# Delta) as HX4.
clear_inversion H2.
eapply H5.
eauto.

clear_inversion H0.
specialize (H4 _ _ HX4).
program_simpl.
renamer.
elim_deters.
clear_dups.


clear_inversion H1.

specialize (H4 _ _ HX3).
program_simpl.
renamer.
eexists.
renamer. elim_deters. clear_dups.
split; eauto.


red in H7.
program_simpl.
renamer.
elim_deters.

(************)

clear_inversion H3.

assert ( [env2] === [env3] #in# Gamma ) as HX1.
clear_inversion H2.
eapply H5.
eauto.

econstructor 2; eauto.
red.

assert ( [env3] === [env2] #in# Delta ) as HX2.
clear_inversion H0.
assert (PER (ValEnv Delta)) as HX3.
eauto.
symmetry.
eauto.


assert ( [env2] === [env2] #in# Delta) as HX3.
assert (PER (ValEnv Delta)) as HX4.
clear_inversion H0.
eauto.

eapply PER_Transitive.
eauto.
eauto.


assert ( [env2] === [env2] #in# Gamma) as HX4.
clear_inversion H2.
eapply H5.
eauto.

clear_inversion H1.
specialize (H4 _ _ HX4).
program_simpl.
renamer.
elim_deters.
clear_dups.


clear_inversion H0.

specialize (H4 _ _ HX3).
program_simpl.
renamer.
eexists.
renamer. elim_deters. clear_dups.
split; eauto.


red in H7.
program_simpl.
renamer.
elim_deters.

eapply InterpType_resp_PerType.
eauto.
symmetry.
eauto.
Qed.
Hint Resolve Valid_EQ_CXT_EXT.
