(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of equivalence judgements for terms
 *)
Set Implicit Arguments.


Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

(******************************************************************************
 * Helper for SYM (symmetry)
 *)

Lemma Helper_SYM: forall Gamma t t' A,
  Gamma |= t === t' : A ->
  Gamma |= t' === t : A
.
Proof.
intros.
clear_inversion H.
clear_inversion H0.
constructor 1; eauto.
intros.

assert ([env1] === [env0] #in# Gamma).
assert (PER (ValEnv Gamma)). apply ValEnv_is_Per; auto.
symmetry; auto.

destruct H1 with env1 env0 as [dtm0 HH]; auto.
destruct HH as [dtm1 HH].
destruct HH as [PA HH].
destruct HH as [HPA HH].
destruct HH as [Hdtm0 HH].
destruct HH as [Hdtm1 HH].
destruct HPA as [DA HPA].
destruct HPA as [HDA HPA].

destruct H2 with env0 env1 as [Da HHH]; auto.
destruct HHH as [Db HHH].
destruct HHH as [HDa HHH].
destruct HHH as [HDb HHH].
assert (DA = Db). eapply EvalTm_deter; eauto. subst.

assert (exists PB, InterpType Da PB).
apply InterpType_dom_PerType. eapply Per_domL; eauto.
destruct H4 as [PB HPB].

assert (PA =~= PB); eauto.
exists dtm1. exists dtm0. exists PB. repeat split; eauto.
exists Da. repeat split; eauto.

assert (PER (InRel PB)). eauto.
symmetry. apply H4. auto.
Qed.
Hint Resolve Helper_SYM.

(******************************************************************************
 * Helper for TRANS (Transitivity)
 *)

Lemma Helper_TRANS: forall Gamma t t' t'' A,
  Gamma |= t === t' : A ->
  Gamma |= t' ===  t'': A ->
  Gamma |= t  ===  t'': A 
.
Proof.
intros.

clear_inversion H.
clear_inversion H0.
clear_inversion H1.

constructor 1; auto.
intros.
assert (PER (ValEnv Gamma)). apply ValEnv_is_Per; auto.
assert ([env1] === [env0] #in# Gamma).
symmetry; auto.
assert ([env0] === [env0] #in# Gamma).
 eapply Per_domL; eauto.

destruct H0 with env0 env0 as [x Hx]; eauto.
destruct Hx as [x' Hx].
destruct Hx as [PX Hx].
destruct Hx as [HPX Hx].

assert (exists Px, InterpType x Px).
apply InterpType_dom_PerType. eapply Per_domL; eauto.
eauto.
assert (exists Px', InterpType x' Px').
apply InterpType_dom_PerType. eapply Per_domL; eauto.
destruct H7 as [Px HPx].
destruct H8 as [Px' HPx'].
assert (Px =~= Px'). eauto.

destruct H2 with env0 env0 as [dtm0 HH]; auto.
destruct HH as [dtm1 HH].
destruct HH as [PA HH].
destruct HH as [HPA HH].
destruct HH as [Hdtm0 HH].
destruct HH as [Hdtm1 Htt].
destruct HPA as [DA HPA].
destruct HPA as [HDA HPA].

destruct H3 with env0 env1 as [dtm0' HH]; auto.
destruct HH as [dtm1' HH].
destruct HH as [PA' HH].
destruct HH as [HPA' HH].
destruct HH as [Hdtm0' HH].
destruct HH as [Hdtm1' HH].
destruct HPA' as [DA' HPA'].
destruct HPA' as [HDA' HPA'].

assert (DA' = DA). eapply EvalTm_deter; eauto. subst. clear_dups.
assert (DA = x). eapply EvalTm_deter; eauto. subst. clear_dups.
assert (dtm0' = dtm1). eapply EvalTm_deter; eauto. subst. clear_dups.
exists dtm0. exists dtm1'. exists Px.
repeat split; eauto.
red. exists x. repeat split; auto.

assert (PER (InRel Px)). eauto.
assert (PER (InRel Px')). eauto.

assert (Px =~= PA). eauto.
assert (Px' =~= PA'). eauto.

apply PER_Transitive with dtm1; eauto. auto.
apply H10. eauto.
apply H7. apply H11. auto.
Qed.
Hint Resolve Helper_TRANS.


(******************************************************************************
 * Helper EQ_ValidL/R 
 *)

Lemma Helper_EQ_ValidL: forall Gamma M1 M2 A,
  Gamma |= M1 === M2 : A ->
  Gamma |= M1 : A
.
Proof.
intros.
cut (Gamma |= M2 === M1 : A); intros; eauto.
Qed.
Hint Resolve Helper_EQ_ValidL.

Lemma Helper_EQ_ValidR: forall Gamma M1 M2 A,
  Gamma |= M1 === M2 : A ->
  Gamma |= M2 : A
.
Proof.
intros.
cut (Gamma |= M2 === M1 : A); intros; eauto.
Qed.
Hint Resolve Helper_EQ_ValidR.
