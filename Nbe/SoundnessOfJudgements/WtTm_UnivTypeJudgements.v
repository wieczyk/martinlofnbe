(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for universe (terms)
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.
Require Import Nbe.SoundnessOfJudgements.WfTp_UnivTypeJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_UNIV_ELEM: forall Gamma T1 T2,
   Gamma |= T1 === T2 : TmUniv ->
   Gamma |= T1 === T2
.
Proof.
intros.

clear_inversion H.
clear_inversion H0.
constructor 1; auto.
intros.

edestruct H1 as [dtm0 [dtm1 [PA] ] ]; eauto.
decompose record H3.
red in H4; decompose record H4.
clear_inversion H9.
clear_inversion H10.
exists dtm0; exists dtm1.
repeat split; auto.
Qed.
Hint Immediate Soundness_of_EQ_UNIV_ELEM.


(******************************************************************************
 *)
Lemma Soundness_of_UNIV_ELEM: forall Gamma T,
   Gamma |= T : TmUniv ->
   Gamma |= T
.
Proof.
intros.
eauto.
Qed.
Hint Resolve Soundness_of_UNIV_ELEM.

(******************************************************************************
 *)
Lemma Soundness_of_Cxt_from_WfTpEq: forall Gamma T1 T2,
  Gamma |= T1 === T2 -> Gamma |=
.
Proof.
intros.
destruct H.
auto.
Qed.
Hint Resolve Soundness_of_Cxt_from_WfTpEq.

(******************************************************************************
 *)
Lemma unhask_ValTpEq: forall Gamma env0 env1 A B,
  [env0] === [env1] #in# Gamma ->
  Gamma |= A === B ->
  exists DA DB, EvalTm A env0 DA /\ EvalTm B env1 DB /\ DA === DB #in# PerType
.
Proof.
intros.
clear_inversion H0.
auto.
Qed.

(******************************************************************************
 *)
Lemma unhask_ValTmEq: forall Gamma env0 env1 t s T,
  [env0] === [env1] #in# Gamma ->
  Gamma |= t === s : T ->
  exists dt ds PT, InterpTypePer T env0 PT /\ EvalTm t env0 dt /\ EvalTm s env1 ds /\ dt === ds #in# PT
.
Proof.
intros.
clear_inversion H0.
auto.
Qed.

(******************************************************************************
 *)
Lemma unhask_ValSbEq: forall Gamma env0 env1 sb0 sb1 Delta,
  [env0] === [env1] #in# Gamma ->
  Gamma |= [sb0] === [sb1] : Delta ->
  exists sbenv0 sbenv1, EvalSb sb0 env0 sbenv0 /\ EvalSb sb1 env1 sbenv1 /\ [sbenv0] === [sbenv1] #in# Delta
.
Proof.
intros.
clear_inversion H0.
auto.
Qed.


(******************************************************************************
 *)
Ltac extractValidJudgementForEnvironment HJ Henv := 
match type of Henv with
| [?env0] === [?env1] #in# ?Gamma =>
  match type of HJ with
    | Gamma |= ?A === ?B =>
      let DA := fresh "DA" in
      let DB := fresh "DB" in
      let HEvalA := fresh "H" in
      let HEvalB := fresh "H" in
      let HDT := fresh "H" in
      destruct (unhask_ValTpEq Henv HJ) as [DA [DB [HEvalA [HEvalB HDT]]]]

    | Gamma |= [?A] === [?B] : ?Delta =>
      let sbenv0 := fresh "sbenv0" in
      let sbenv1 := fresh "sbenv0" in
      let HEval0 := fresh "H" in
      let HEval1 := fresh "H" in
      let HDT := fresh "H" in
      destruct (unhask_ValSbEq Henv HJ) as [sbenv0 [sbenv1 [HEvalA [HEvalB HDT]]]]


    | Gamma |= ?t0 === ?t1 : ?T =>
      let dt0 := fresh "dt0" in
      let dt1 := fresh "dt0" in
      let PT := fresh "PT" in
      let HInterpT := fresh "H" in
      let HEvalt0 := fresh "H" in
      let HEvalt1 := fresh "H" in
      let HPER := fresh "H" in
      destruct (unhask_ValTmEq Henv HJ) as [dt [dt1 [PT [HInterpT [HEvalt0 [HEvalt1 HPER] ] ] ] ] ]

  end
end
.

(******************************************************************************
 *)
Lemma Soundness_of_UNIV_CONG_FUN_F: forall Gamma A A' B B',
  Gamma |= A === A': TmUniv ->
  Gamma,, A |= B === B' : TmUniv ->
  Gamma |= TmFun A B === TmFun A' B' : TmUniv
.
Proof.
intros.
constructor 1; eauto.
intros.

assert (Gamma |= A === A') as HX1 by eauto.

extractValidJudgementForEnvironment H H1.
extractValidJudgementForEnvironment HX1 H1.
compute_sth.
destruct H2 as [DT [HDT1 HDT2]].
clear_inversion HDT1.
clear_inversion HDT2.
renamer.

assert (exists PA, InterpType DA_env0 PA) as HX2 by eauto.
destruct HX2 as [PA HPA].

set (dy0 := Dfun DA_env0 (Dclo B env0)).
set (dy1 := Dfun DA'_env1 (Dclo B' env1)).
exists dy0.
exists dy1.
exists PerUniv.
unfold dy0; unfold dy1; repeat split; eauto.
exists Duniv; eauto.
(**)
eapply PerUniv_fun.
{
eauto.
}
{
eapply InterpUniv_from_InterpType; eauto.
}
{
intros.

assert ([Dext env0 a] === [Dext env1 a] #in# Gamma,,A) as HX3.
{
econstructor 2.
eauto.
red. eexists; eauto.
eauto.
}

extractValidJudgementForEnvironment H0 HX3.
eauto.
}
(**)
{
intros.

assert ([Dext env0 a] === [Dext env1 a] #in# Gamma,,A) as HX3.
{
econstructor 2.
eauto.
red. eexists; eauto.
eauto.
}

extractValidJudgementForEnvironment H0 HX3.
eauto.
}
(**)
{
intros.

assert (P =~= PA) as HX3.
{
eapply InterpType_extdeter.
eauto.
eauto.
}

assert ([Dext env0 a0] === [Dext env1 a1] #in# Gamma,,A) as HX4.
{
econstructor 2.
eauto.
red. eexists; eauto.
eapply HX3.
eauto.
}

extractValidJudgementForEnvironment H0 HX4.
clear_inversion H7.
clear_inversion H9.
compute_sth.
clear_inversion H10.
clear_inversion H7.
clear_inversion H9.
clear_inversion H10.
auto.
}
Qed.
Hint Immediate Soundness_of_UNIV_CONG_FUN_F.

(******************************************************************************
 *)
Lemma Soundness_of_UNIV_SB_FUN_F: forall Gamma Delta S A B,
  Gamma |=  [S]:Delta ->
  Delta |= A : TmUniv ->
  Delta,, A |= B : TmUniv ->
  Gamma |=  TmSb (TmFun A B) S === TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar)) : TmUniv
.
Proof.
intros.

constructor 1; eauto; intros.

assert (Delta |= A) as HX1 by eauto.

extractValidJudgementForEnvironment H H2.
extractValidJudgementForEnvironment H0 H5.
extractValidJudgementForEnvironment HX1 H5.


compute_sth.
clear_inversion H3.
clear_inversion H8.
clear_inversion H3.
clear_inversion H9.
renamer.

assert (exists PA, InterpUniv DA_sbenv0 PA) as HX2 by eauto.
destruct HX2 as [PA HPA].

exists (Dfun DA_sbenv0 (Dclo B DS_env0)).
exists (Dfun DA_sbenv1 (Dclo (TmSb B (Sext (Sseq S Sup) TmVar)) env1)).
exists PerUniv.

repeat split; eauto.
{
exists Duniv; split; auto.
}
{
eapply PerUniv_fun; eauto.
{
intros.

assert ([Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A) as HX3.
{
econstructor 2.
eauto.
red. eexists; eauto.
eauto.
}

extractValidJudgementForEnvironment H1 HX3.
eauto.
}

{
intros.
assert ([Dext DS_env0 a] === [Dext DS_env1 a] #in# Delta,,A) as HX3.
{
econstructor 2.
eauto.
red. eexists; eauto.
eauto.
}

extractValidJudgementForEnvironment H1 HX3.
exists dt1; eauto.
}

{
intros.
assert ([Dext DS_env0 a0] === [Dext DS_env1 a1] #in# Delta,,A) as HX3.
{
econstructor 2.
eauto.
red. eexists; eauto.
eauto.
}
extractValidJudgementForEnvironment H1 HX3.
clear_inversion H9.
clear_inversion H11.
clear_inversion H19.
clear_inversion H16.
clear_inversion H22.
clear_inversion H17.
clear_inversion H16.
elim_deters.
compute_sth.
renamer.
clear_inversion H12.
clear_inversion H9.
clear_inversion H11.
clear_inversion H12.
auto.
}
}
Qed.
Hint Immediate Soundness_of_UNIV_SB_FUN_F.
