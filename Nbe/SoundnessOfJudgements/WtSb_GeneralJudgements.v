(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgmenets for substitutions
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.


(******************************************************************************
 *)
Lemma Helper_SB_EXT: forall Gamma S Delta M A,
  Gamma |=  [S]:Delta ->
  Delta |= A ->
  Gamma |= M : TmSb A S ->
  Gamma |=  [Sext S M]:Delta,, A
.
Proof.

constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs. program_simpl. compute_sth. renamer.
apply_valenvs. program_simpl. compute_sth. renamer.
exists (Dext DS_env0 DM_env0).
exists (Dext DS_env1 DM_env1).
repeat split; eauto.
red in H11.
program_simpl; renamer.
apply ValEnv_ext with PH11; repeat split; eauto.
red.
inversion_clear H. renamer.
exists DA_DS_env0; repeat split; eauto.
compute_sth.

Qed.
Hint Resolve Helper_SB_EXT.


(******************************************************************************
 *)
Lemma Valid_SB_Eta: forall Gamma A,
  Gamma    |= A ->
  Gamma,,A |= [Sid] === [Sext Sup TmVar] : Gamma,,A
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
apply ValEnv_ext with PDA_env2; auto.
red; eexists; repeat split; eauto.
Qed.
Hint Resolve Valid_SB_Eta.

(******************************************************************************
 *)
Lemma Valid_EQ_SB_CONV: forall Gamma sb1 sb2 Delta Psi,
  |= {Psi} === {Delta} ->
  Gamma |= [sb1] === [sb2] : Psi ->
  Gamma |= [sb1] === [sb2] : Delta.
Proof.
intros.
clear_inversion H.
clear_inversion H0.
econstructor.
eauto.
eauto.
intros.
apply_valenvs.
program_simpl.
renamer.
exists Dsb1_env0.
exists Dsb2_env1.
repeat split;
eauto.

eapply H3.
eauto.
Qed.
Hint Resolve Valid_EQ_SB_CONV.

(******************************************************************************
 *)
Lemma Valid_SB_CONV: forall Gamma sb Delta Psi,
  |= {Psi} === {Delta} ->
  Gamma |= [sb] : Psi ->
  Gamma |= [sb] : Delta
.
Proof.
intros.
eauto.
Qed.
Hint Resolve Valid_SB_CONV.

(******************************************************************************
 *)
Lemma Soundness_of_SID: forall Gamma,
  Gamma |= ->
  Gamma |=  [Sid]:Gamma
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
do 2 eexists; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_SID.

(******************************************************************************
 *)
Lemma Soundness_of_SUP: forall Gamma A,
  Gamma |= A ->
  Gamma,,A |=  [Sup]:Gamma
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
do 2 eexists; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_SUP.

(******************************************************************************
 *)
Lemma Soundness_of_SSEQ: forall Gamma__0 Gamma__1 Gamma__2  S__1 S__2,
  Gamma__0 |=  [S__2]:Gamma__1 ->
  Gamma__1 |=  [S__1]:Gamma__2 ->
  Gamma__0 |=  [Sseq S__1 S__2]:Gamma__2
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_SSEQ.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_CONG_SSEQ: forall Gamma S1 S2 U1 U2 GammaS1 GammaS2,
  Gamma |=  [S2]=== [U2]:GammaS1 ->
  GammaS1 |=  [S1]=== [U1]:GammaS2 ->
  Gamma |=  [Sseq S1 S2]=== [Sseq U1 U2]:GammaS2
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_CONG_SSEQ.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_CONG_SEXT: forall Gamma S1 S2 Delta M1 M2 A,
  Gamma |=  [S1]=== [S2]:Delta ->
  Delta |= A ->
  Gamma |=  M1 === M2 : TmSb A S1 ->
  Gamma |=  [Sext S1 M1]=== [Sext S2 M2]:Delta,, A
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
clear_inversion H16; clear_inversion H17.
program_simpl; compute_sth; reds; renamer.

apply ValEnv_ext with PDA_DS1_env0; eauto.
red; eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_CONG_SEXT.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_EXT: forall Gamma S1 S2 GammaS1 GammaS2 M A,
  Gamma |=  [S1]:GammaS1 ->
  GammaS1 |=  [Sext S2 M]:GammaS2,, A ->
  Gamma |=  [Sseq (Sext S2 M) S1]=== [Sext (Sseq S2 S1) (TmSb M S1)] : GammaS2,, A
.
Proof.
intros.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.

do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_EXT.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_SUP: forall Gamma S M A Delta,
  Gamma |=  [S]:Delta ->
  Delta |= A ->
  Gamma |= M : TmSb A S ->
  Gamma |=  [Sseq Sup (Sext S M)]=== [S]:Delta
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_SUP.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_IDL: forall Gamma S GammaS,
  Gamma |=  [S]:GammaS ->
  Gamma |=  [Sseq Sid S]=== [S]:GammaS
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_IDL.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_IDR: forall Gamma S GammaS,
  Gamma |=  [S]:GammaS ->
  Gamma |=  [Sseq S Sid]=== [S]:GammaS
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_IDR.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB_COMM: forall Gamma S1 S2 S3 GammaS1 GammaS2 GammaS3,
  Gamma |=  [S3]:GammaS1 ->
  GammaS1 |=  [S2]:GammaS2 ->
  GammaS2 |=  [S1]:GammaS3 ->
  Gamma |=  [Sseq (Sseq S1 S2) S3]=== [Sseq S1 (Sseq S2 S3)]:GammaS3
.
Proof.
intros.


constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB_COMM.
