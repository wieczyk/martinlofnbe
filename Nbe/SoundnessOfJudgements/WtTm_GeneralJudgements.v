(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments (terms)
 *)
Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

Require Import Nbe.SoundnessOfJudgements.WfTp_GeneralJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_EquivalenceJudgements.
Require Import Nbe.SoundnessOfJudgements.WtSb_EquivalenceJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_HYP: forall Gamma T,
  Gamma,, T |= ->
  Gamma,, T |= TmVar : TmSb T Sup
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
constructor 1; auto; intros; extract_vals; reds; auto.

compute_sth; apply_valenvs; program_simpl.
compute_sth; apply_valenvs; program_simpl.
do 3 eexists.
repeat split; eauto.
eexists; eauto.

compute_sth; apply_valenvs; program_simpl.
compute_sth; apply_valenvs; program_simpl.
do 3 eexists.
repeat split; eauto.
eexists; eauto.
Qed.
Hint Resolve Soundness_of_HYP.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONV: forall Gamma M1 M2 A B,
  Gamma |=  M1 === M2 : A ->
  Gamma |= A === B ->
  Gamma |=  M1 === M2 : B
.
Proof.
constructor 1; auto; intros; extract_vals; reds; eauto.
apply_valenvs; program_simpl; compute_sth; renamer.
red in H13; program_simpl; compute_sth; renamer.
do 3 eexists; repeat split; eauto.

assert (PER $ ValEnv Gamma) by eauto.
assert ([env1] === [env0] #in# Gamma) by (symmetry; auto).
apply_valenvs; program_simpl; compute_sth; renamer.
red. exists DB_env0; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_EQ_CONV.


(******************************************************************************
 *)
Lemma Soundness_of_CONV: forall Gamma A B t,
  Gamma |= t : A ->
  Gamma |= A === B ->
  Gamma |= t : B
.
Proof.
intros.
eauto.
Qed.
Hint Resolve Soundness_of_CONV.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONG_SB: forall Gamma M1 M2 A S1 S2 GammaS,
  Gamma |=  [S1]=== [S2]:GammaS ->
  GammaS |=  M1 === M2 : A ->
  Gamma |=  TmSb M1 S1 === TmSb M2 S2 : TmSb A S1
.
Proof.
intros.

assert (Gamma |= TmSb A S1).
assert (GammaS |= M1 : A) by eauto.
assert (Gamma |= [S1] : GammaS) by eauto.
apply Helper_SB_F with GammaS; eauto.
inversion H1; eauto.

constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.
apply_valenvs; program_simpl; compute_sth; renamer.
reds; program_simpl; compute_sth; renamer.
move_inpers.
do 3 eexists; repeat split; eauto.
red; eexists; repeat split; eauto.
clear_inversion H9.
clear_inversion H10.
program_simpl; compute_sth; renamer.
Qed.
Hint Resolve Soundness_of_EQ_CONG_SB.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBVAR: forall Gamma S Delta M A,
  Gamma |=  [S]:Delta ->
  Delta |= A ->
  Gamma |= M : TmSb A S ->
  Gamma |=  TmSb TmVar (Sext S M) === M : TmSb A S
.
Proof.
intros.


assert (Delta |= A) by (inversion H0; auto).
assert (Gamma |= TmSb A S) by eauto.

constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
compute_sth; renamer.
move_inpers.
clear_inversion H23. clear_inversion H11.
program_simpl; compute_sth; reds; renamer; move_inpers.
do 2 eexists; exists PDA_DS_env0; repeat split; eauto.
red. eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SBVAR.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBID: forall Gamma M A,
  Gamma |= M : A ->
  Gamma |=  TmSb M Sid === M : A
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
move_inpers.
repeat eexists; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SBID.

