(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Proof that relation for environments is PER relation
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.

(******************************************************************************
 * (_ === _ #in# Gamma) is PER
 *)

Instance ValEnv_is_Per Gamma (H : Gamma |=) : PER(ValEnv Gamma).
Proof.
constructor 1.

repeat red.
intros.
induction H0; auto.

extract_vals.
specialize (IHValEnv H5).
red in H1; program_simpl; renamer.
assert (PER (InRel PDA_env0)) by eauto.
apply_valenvs.
program_simpl; renamer.
compute_sth; renamer.

constructor 2 with PDA_env0. auto.
red; exists DA_env1. split; auto.
eapply InterpType_resp_PerType.
eauto.
symmetry. auto.
symmetry. auto.

(**)

induction Gamma.

red.
intros.

clear_inversion H0.
clear_inversion H1.
auto.

(*-*)
extract_vals.
specialize (IHGamma H2).
red in IHGamma.

red; intros.

clear_inversion H.
clear_inversion H1.
red in H6.
program_simpl; renamer.
assert (PER (InRel PDa_env0)) by eauto.
constructor 2 with PDa_env0.
eapply IHGamma; eauto.
red.
eexists; repeat split; eauto.
apply PER_Transitive with d1; auto.
red in H11.

apply_valenvs.
specialize (H0 env0 env1 H5).
program_simpl; renamer.
compute_sth; renamer.
assert (PDa_env0 =~= PDa_env1) as EQ by eauto.
apply EQ.
auto.
Qed.
Hint Resolve ValEnv_is_Per.
