(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of equivalence judgments for substitutions
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.


(******************************************************************************
 *)
Lemma Helper_SB_SYM: forall Gamma S1 S2 Delta,
  Gamma |= [S1] === [S2] : Delta ->
  Gamma |= [S2] === [S1] : Delta
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.
assert (PER $ ValEnv Gamma) by eauto.
assert ([env1] === [env0] #in# Gamma) by (symmetry; auto).
apply H3 in H4.
program_simpl; renamer.
exists DS2_env0.
exists DS1_env1.
repeat split; eauto.
assert (PER $ ValEnv Delta) by eauto.
symmetry. auto.
Qed.
Hint Resolve Helper_SB_SYM.

(******************************************************************************
 *)
Lemma Helper_SB_TRANS: forall Gamma S1 S2 S3 Delta,
  Gamma |= [S1] === [S2] : Delta ->
  Gamma |= [S2] === [S3] : Delta ->
  Gamma |= [S1] === [S3] : Delta
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
assert (PER $ ValEnv Gamma) by eauto.
assert (PER $ ValEnv Delta) by eauto.
assert ([env0] === [env0] #in# Gamma).
eapply Per_domL; eauto.
apply H6 in H5.
apply H4 in H1.
program_simpl; compute_sth; renamer.
exists DS1_env0.
exists DS3_env1.
repeat split; auto.
apply PER_Transitive with DS2_env0; eauto.
Qed.
Hint Resolve Helper_SB_TRANS.

(******************************************************************************
 *)
Lemma Helper_EQ_SB_ValidL: forall Gamma S1 S2 Delta,
  Gamma |= [S1] === [S2] : Delta ->
  Gamma |= [S1] : Delta
.
Proof.
intros.
cut (Gamma |= [S2] === [S1] : Delta); intros; eauto.
Qed.
Hint Resolve Helper_EQ_SB_ValidL.

(******************************************************************************
 *)
Lemma Helper_EQ_SB_ValidR: forall Gamma S1 S2 Delta,
  Gamma |= [S1] === [S2] : Delta ->
  Gamma |= [S2] : Delta
.
Proof.
intros.
cut (Gamma |= [S2] === [S1] : Delta); intros; eauto.
Qed.
Hint Resolve Helper_EQ_SB_ValidR.
