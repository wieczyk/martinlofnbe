(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Characterization theorem for reflexion and reification
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNf.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.PerNat.
Require Import Nbe.Model.PerUnit.
Require Import Nbe.Model.PerEmpty.
Require Import Nbe.Model.Interp.
Require Import Nbe.Model.Interp_Facts.
Require Import Nbe.Model.PerUniv.
Require Import Nbe.Model.PerNeUniv.
Require Import Nbe.Model.InterpUniv.
Require Import Nbe.Model.InterpUniv_Facts.
Require Import Nbe.Model.CharacterizationOfPerUniv.

(******************************************************************************
 * Theorem
 *)

Theorem ReflectReify_Characterization: forall T T' ,
  T === T' #in# PerType -> forall PT,
  InterpType T PT ->
  (forall e e', e === e' #in# PerNe -> Dup T e === Dup T' e' #in# PT) /\
  (forall d d', d === d' #in# PT -> Ddown T d === Ddown T' d' #in# PerNf) /\
  (DdownN T === DdownN T' #in# PerNf)
.
Proof.
intros T T' HT.
induction HT;intros PT HPT; try solve
   [ eapply ReflectReify_Characterization_of_PerUniv; eauto
   ].

(******************************* Neutral types *)
+{
assert (Dup Duniv e === Dup Duniv e' #in# PerUniv) as HX1 by eauto.
eapply ReflectReify_Characterization_of_PerUniv; eauto.
}



(******************************* Universe *)
+{
split; [ | split ]; intros.
-{ (* Reflect *)
clear_inversion HPT.
eapply PerUniv_ne.
auto.
}
-{ (* Reify *)
clear_inversion HPT.
assert (exists P, InterpUniv d P) as HX1 by eauto.
destruct HX1 as [P].

eapply ReifyTp_Characterization_of_PerUniv; eauto.
}
-{ (* ReifyTp *)
clear_inversion HPT.
split; intros; exists TmUniv; eauto.
}
}

(******************************* Fun type  *)
+{
repeat split; intros; inversion_of_interptype HPT; eauto.
-{ (* Reflect *)
constructor; intros.
assert (PA0 =~= PA) as HX1 by eauto.
assert (a0 === a1 #in# PA) as HX2 by (eapply HX1; eauto).
destruct H1 with (a := a0) as [DB0]; eauto.
destruct H2 with (a := a1) as [DB1]; eauto.

set (y0 := (Dup DB0 (Dapp e (Ddown DA a0)))).
set (y1 := (Dup DB1 (Dapp e' (Ddown DA' a1)))).
exists y0; exists y1.
clear_inversion H8.
clear_inversion po_ro.
destruct ro_resp_ex with (a := a0) as [P]; eauto.
exists P.
setoid_rewrite Dup_Dfun; subst y0; subst y1;
repeat split; eauto.

edestruct H4 as [IHReflect]; eauto.
eapply IHReflect.
constructor; intros.

assert (exists ve, RbNe m e ve /\ RbNe m e' ve) as HX3.
{
destruct H5.
destruct H5 with m.
eauto.
}

assert (exists va, RbNf m (Ddown DA a0) va /\ RbNf m (Ddown DA' a1) va) as HX4.
{
edestruct IHHT with (PT := PA0) as [_ [HReflect _] ]; eauto.
specialize (HReflect _ _ H6).
clear_inversion HReflect.
destruct H14 with m as [va].
eauto.
}

destruct HX3 as [ve [Hve1 Hve2] ].
destruct HX4 as [va [Hva1 Hva2] ].
eexists; eauto.
}
-{ (* Reify *)
assert (exists W, RbNf m (DdownN DA) W /\ RbNf m (DdownN DA') W) as HX1.
{
edestruct IHHT as [_ [_ IHReifyTp] ]; eauto.
clear_inversion IHReifyTp.
eauto.
}

set (dvar := Dup DA (Dvar m)).
set (dvar' := Dup DA' (Dvar m)).

assert (dvar === dvar' #in# PA0) as HX2.
{
edestruct IHHT as [IHReflect _]; eauto.
}

assert (PA =~= PA0) as HX3 by eauto.

assert (dvar === dvar' #in# PA) as HX4 by (eapply HX3; eauto).

assert (exists DB, App DF dvar DB) as HX5 by eauto.
assert (exists DB', App DF' dvar' DB') as HX6 by eauto.


destruct HX5 as [DB].
destruct HX6 as [DB'].

clear_inversion H5.
edestruct H12 as [y0 [y1 [Y ] ] ]; eauto.
decompose record H5.
rename y0 into y.
rename y1 into y'.

assert (exists w, RbNf (S m) (Ddown DB y) w /\ RbNf (S m) (Ddown DB' y') w) as HX7.
{
edestruct H4 as [_ [IHReify _]]; eauto.
edestruct (IHReify _ _ H17); eauto.
}

destruct HX7 as [w [Hw1 Hw2] ].
destruct HX1 as [W [HW1 HW2] ].
subst dvar; subst dvar'.
exists (TmAbs W w); split; eapply reifyNf_abs; eauto.
}
-{ (* ReifyTp *)

assert (exists W, RbNf m (DdownN DA) W /\ RbNf m (DdownN DA') W) as HX1.
{
edestruct IHHT as [_ [_ IHReifyTp] ]; eauto.
clear_inversion IHReifyTp.
eauto.
}

set (dvar := Dup DA (Dvar m)).
set (dvar' := Dup DA' (Dvar m)).

assert (dvar === dvar' #in# PA0) as HX2.
{
edestruct IHHT as [IHReflect _]; eauto.
}

assert (PA =~= PA0) as HX3 by eauto.

assert (dvar === dvar' #in# PA) as HX4 by (eapply HX3; eauto).

assert (exists DB, App DF dvar DB) as HX5 by eauto.
assert (exists DB', App DF' dvar' DB') as HX6 by eauto.


destruct HX5 as [DB].
destruct HX6 as [DB'].

assert (exists PB, InterpType DB PB) as HX8 by eauto.

assert (exists F, RbNf (S m) (DdownN DB) F /\ RbNf (S m) (DdownN DB') F) as HX7.
{
destruct HX8.
edestruct H4 as [_ [_ IHReifyTp] ]; eauto.
destruct IHReifyTp.
eauto.
}

destruct HX7 as [w [Hw1 Hw2] ].
destruct HX1 as [W [HW1 HW2] ].
subst dvar; subst dvar'.
exists (TmFun W w); split; eapply reifyNf_fun; eauto.
}
}
Qed.

(******************************************************************************
 *)

Definition Reflect_Characterization
  T T' (HT: T === T' #in# PerType) PT (HPT: InterpType T PT)
  := proj1 (ReflectReify_Characterization HT HPT)
.

Definition Reify_Characterization
  T T' (HT: T === T' #in# PerType) PT (HPT: InterpType T PT)
  := proj1 (proj2 (ReflectReify_Characterization HT HPT))
.

Definition ReifyTp_Characterization
  T T' (HT: T === T' #in# PerType) PT (HPT: InterpType T PT)
  := proj2 (proj2 (ReflectReify_Characterization HT HPT))
.

