(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Interpretation function for universe of small types
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNf.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.PerNat.
Require Import Nbe.Model.PerUnit.
Require Import Nbe.Model.PerEmpty.
Require Import Nbe.Model.PerNeUniv.

(******************************************************************************
 * Interpretation function for Universe
 *)

Inductive InterpUniv: D -> relation D -> Prop :=
| InterpUniv_ne:
  forall de, de === de #in# PerNe ->
  InterpUniv (Dup Duniv de) PerNeUniv

| InterpUniv_nat: 
  InterpUniv Dnat PerNat

| InterpUniv_empty:
  InterpUniv Dempty PerEmpty

| InterpUniv_unit:
  InterpUniv Dunit PerUnit

| InterpUniv_fun: forall DA DF PA PF,
  InterpUniv DA PA ->
  ProperPerProd PA PF ->
  (forall a PB DB, a === a #in# PA -> App DF a DB -> PF a PB -> InterpUniv DB PB) ->
  (forall a, a === a #in# PA -> exists DB, App DF a DB) ->
  InterpUniv (Dfun DA DF) (RelProd PA PF)
. 
Hint Constructors InterpUniv.

(******************************************************************************
 *)

Definition InterpUnivPer (A:Tm) (env:DEnv) (PA:relation D) : Prop := 
  exists DA, EvalTm A env DA /\ InterpUniv DA PA
.

