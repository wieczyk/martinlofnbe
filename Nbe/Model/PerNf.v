(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * PER relation for normal-like values
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

(******************************************************************************
 * PER for Normal forms
 *)

Inductive PerNf : relation DNf :=
| PerNf_intro: forall e e',
  (forall m, exists v, RbNf m e v /\ RbNf m e' v) -> e === e' #in# PerNf
.

(******************************************************************************
 *)
Instance PerNf_is_PER : PER (InRel PerNf).
Proof.
constructor 1.
red; intros; auto.
destruct H.
apply PerNf_intro.
firstorder.

red; intros x y z Hxy Hyz.
inversion Hxy; simpl.
inversion Hyz; simpl.
subst.
constructor 1; intros.
specialize H with m.
specialize H2 with m.
program_simpl.
replace H with H2 in *.
firstorder.
symmetry; eapply RbNf_deter; eauto.
Qed.
Hint Resolve PerNf_is_PER.

