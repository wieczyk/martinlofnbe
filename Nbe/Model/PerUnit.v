(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * PER relation for unit type
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNe.


(******************************************************************************
 * Relation
 *
 * Due to eta-rule for unit type all values of unit-type are equal
 *)

Inductive PerUnit : relation D :=
| PerUnit_unit: forall d d',
  d === d' #in# PerUnit
(*
| PerUnit_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Dunit e === Dup Dunit e' #in# PerUnit
*)
.
Hint Constructors PerUnit.

(******************************************************************************
 *)
Instance PerUnit_is_PER : PER (InRel PerUnit).
constructor 1; red; intros; auto.
Qed.
Hint Resolve PerUnit_is_PER.

