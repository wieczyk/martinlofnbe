(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Definition of interpretation function for types and relation for all types
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNf.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.PerNat.
Require Import Nbe.Model.PerUnit.
Require Import Nbe.Model.PerEmpty.
Require Import Nbe.Model.PerUniv.
Require Import Nbe.Model.PerNeUniv.
Require Import Nbe.Model.InterpUniv.

(******************************************************************************
 * PER for Type and Interp
 *)
Inductive InterpType: D -> relation D -> Prop :=
| InterpType_ne:
  forall de, de === de #in# PerNe ->
  InterpType (Dup Duniv de) PerNeUniv

| InterpType_nat: 
  InterpType Dnat PerNat

| InterpType_empty:
  InterpType Dempty PerEmpty

| InterpType_unit:
  InterpType Dunit PerUnit

| InterpType_univ:
  InterpType Duniv PerUniv

| InterpType_fun: forall DA DF PA PF,
  InterpType DA PA ->
  ProperPerProd PA PF ->
  (forall a PB DB, a === a #in# PA -> App DF a DB -> PF a PB -> InterpType DB PB) ->
  (forall a, a === a #in# PA -> exists DB, App DF a DB) ->
  InterpType (Dfun DA DF) (RelProd PA PF)
.
Hint Constructors InterpType.

Inductive PerType : relation D :=
| PerType_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Duniv e === Dup Duniv e' #in# PerType

| PerType_nat: 
  Dnat === Dnat #in# PerType

| PerType_unit:
  Dunit === Dunit #in# PerType

| PerType_empty:
  Dempty === Dempty #in# PerType

| PerType_univ:
  (forall d1 d2, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType) -> 
  Duniv === Duniv #in# PerType

| PerType_fun: forall DA DF DA' DF' PA,
  DA === DA' #in# PerType ->
  InterpType DA PA -> 
  (forall a, a === a #in# PA -> exists DB, App DF a DB) ->
  (forall a, a === a #in# PA -> exists DB', App DF' a DB') ->
  (forall a0 a1 DB0 DB1 P , InterpType DA P -> a0 === a1 #in# P -> App DF a0 DB0 -> App DF' a1 DB1 ->
    DB0 === DB1 #in# PerType
  ) ->
  Dfun DA DF === Dfun DA' DF' #in# PerType
.
Hint Constructors PerType.

(******************************************************************************
 *)

Scheme PerType_dind := Induction for PerType Sort Prop.

Definition InterpTypePer (A:Tm) (env:DEnv) (PA:relation D) : Prop := 
  exists DA, EvalTm A env DA /\ InterpType DA PA
.

