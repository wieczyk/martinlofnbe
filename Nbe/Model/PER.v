(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Auxiliary definitions for PER relations. We are basinc on PER TypeClass
 * from Standard Library
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Domain.
Require Import Nbe.Utils.

(******************************************************************************
 * Carrier for notation 
 *)
Definition InRel (D:Type) (R:relation D) := R.

Notation "d === d' #in# R" := (InRel R d d') (at level 75, no associativity).

(******************************************************************************
 * Extensional equality for PERs
 *)

Definition binRelEq (D:Type) (R1:relation D) (R2:relation D) :=
  (forall x y, R1 x y <-> R2 x y).

Notation "R1 =~= R2" := (binRelEq R1 R2) (at level 75, no associativity).

Instance binRelEq_EQ (D:Type) : Equivalence (@binRelEq D).
Proof.
constructor 1; red; intros; red; intros.
split; auto.
red in H.
split; auto; apply H.

red in H. red in H0.
split; auto; intro Z. 
apply H0; apply H; auto.
apply H; apply H0; auto.
Qed.

Hint Unfold binRelEq.

Lemma binRelEq_refl: forall P : relation D, 
  P =~= P.
Proof.
red; split; intros; auto.
Qed.
Hint Resolve binRelEq_refl.

(******************************************************************************
 *)
Lemma Per_domL (D:Type) (R:relation D) (HP:PER R) : forall x y,
  x === y #in# R -> x === x #in# R
.
Proof.
intros.
cut (y === x #in# R); try intro Hyx; intuition.
apply (PER_Transitive x y); 
intuition.
Qed.
Hint Resolve Per_domL.

(******************************************************************************
 *)
Lemma Per_domR (D:Type) (R:relation D) (HP:PER R) : forall x y,
  x === y #in# R -> y === y #in# R
.
Proof.
intros.
cut (y === x #in# R); try intro Hyx; intuition.
apply (PER_Transitive y x); 
intuition.
Qed.
Hint Resolve Per_domR.

(******************************************************************************
 * Representation of families of relations
 *)
Definition rel_oper (D:Type) := D -> relation D -> Prop.

Record ProperRelOper (A:relation D) (F:rel_oper D) :=
  Mk_ProperRelOper {
    ro_resp_arg: forall a0 a1 Y, a0 === a1 #in# A -> F a0 Y -> F a1 Y;
    ro_resp_ex:  forall a, a === a #in# A -> exists B, F a B;
    ro_resp_det: forall a0 a1 Y0 Y1, a0 === a1 #in# A -> F a0 Y0 -> F a1 Y1 -> Y0 =~= Y1
  }
.
Hint Constructors ProperRelOper.

(******************************************************************************
 * Product of relations (for modeling PI-type)
 *)
Inductive RelProd (A:relation D) (F:rel_oper D) : relation D :=
| RelProd_intro: forall f0 f1,
  (forall a0 a1, a0 === a1 #in# A -> exists y0 y1 Y,
    F a0 Y /\
    App f0 a0 y0 /\ App f1 a1 y1 /\
    y0 === y1 #in# Y) ->
  f0 === f1 #in# RelProd A F
.

(******************************************************************************
 * Required properties for being PER product 
 *)
Record ProperPerProd (A:relation D) (F:rel_oper D) :=
  Mk_ProperPerOper {
    po_ro      :> ProperRelOper A F;
    po_per_codom: forall a Y, a === a #in# A -> F a Y -> PER Y;
    po_per_dom :> PER A
  }
.
Hint Constructors ProperPerProd.

(******************************************************************************
 *)
Instance ProperPerProd_is_PER A F (H:ProperPerProd A F): PER (RelProd A F)
.
Proof.
destruct H.
destruct po_ro0.
constructor 1; red; intros.

clear_inversion H.
constructor 1.
intros.
destruct H0 with a1 a0.
symmetry; assumption.
program_simpl.
exists H1, x0, H2.
repeat split; eauto.
apply ro_resp_arg0 with a1; auto. symmetry; assumption.

assert (PER H2).
apply po_per_codom0 with a1; eauto with *.
symmetry; auto.

(* trans *)

clear_inversion H.
clear_inversion H0.
constructor 1; intros.

specialize (H1 a0 a0).
destruct H1 as [y0 H1]; eauto.
destruct H1 as [y1 H1]; eauto.
destruct H1 as [Y  H1]; eauto.

specialize (H a0 a1).
destruct H as [y0' H]; eauto.
destruct H as [y1' H]; eauto.
destruct H as [Y'  H]; eauto.

program_simpl.

replace y0' with y1 in *.
assert (PER Y).
apply po_per_codom0 with a0; eauto.

exists y0, y1', Y; repeat split; auto.
apply PER_Transitive with y1; auto.

specialize (ro_resp_det0 a0 a0 Y Y').
apply ro_resp_det0; eauto.

eapply App_deter; eauto.
Qed.
Hint Resolve ProperPerProd_is_PER.
