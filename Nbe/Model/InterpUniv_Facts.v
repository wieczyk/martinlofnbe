(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Basic facts about interpretation of small types (InterpUniv) 
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNf.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.PerNat.
Require Import Nbe.Model.PerUnit.
Require Import Nbe.Model.PerEmpty.
Require Import Nbe.Model.PerUniv.
Require Import Nbe.Model.PerNeUniv.
Require Import Nbe.Model.InterpUniv.


(******************************************************************************
 * InterpType gives PER relations
 *)

Lemma InterpUniv_pers: forall d R, 
  InterpUniv d R -> PER R
.
Proof.
intros.
induction H. 
auto.
auto.
auto.
auto.
auto.
Qed.
Hint Resolve InterpUniv_pers.

(******************************************************************************
 *)
Lemma InterpUniv_RelProd_extdeter: forall DA DF PA PF PA0 PF0,
 InterpUniv DA PA ->
 ProperPerProd PA PF ->
 (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA -> App DF a DB -> PF a PB -> InterpUniv DB PB) ->
 (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA ->
       App DF a DB ->
       PF a PB -> forall R2 : relation D, InterpUniv DB R2 -> PB =~= R2) ->
  (forall a : D, a === a #in# PA -> exists DB : D, App DF a DB) ->
  (forall R2 : relation D, InterpUniv DA R2 -> PA =~= R2) ->
  InterpUniv DA PA0 ->
  ProperPerProd PA0 PF0 ->
  (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA0 -> App DF a DB -> PF0 a PB -> InterpUniv DB PB) ->
  (forall a : D, a === a #in# PA0 -> exists DB : D, App DF a DB) ->
 RelProd PA PF =~= RelProd PA0 PF0
.
Proof.
intros.
assert (PA =~= PA0) as HX1 by eauto.

red; split; intro.
-{
constructor; intros.

assert (a0 === a1 #in# PA) as HX2.
eapply HX1; eauto.

clear_inversion H9.
rename x into f.
rename y into f'.
specialize (H11 _ _ HX2).
decompose record H11.
rename x into y0.
rename x0 into y1.
rename x1 into Y.

destruct H6.
destruct po_ro.

destruct ro_resp_ex with (a := a0) as [G].
eauto.


exists y0; exists y1; exists G.
repeat split; eauto.
+{

destruct H8 with (a := a0) as [DB0]; eauto.
destruct H8 with (a := a1) as [DB1]; eauto.
eapply H2 with (a:= a0) (PB := Y) (R2 := G); eauto.
}

}

-{
constructor; intros.

assert (a0 === a1 #in# PA0) as HX2.
eapply HX1; eauto.

clear_inversion H9.
rename x into f.
rename y into f'.
specialize (H11 _ _ HX2).
decompose record H11.
rename x into y0.
rename x0 into y1.
rename x1 into Y.

destruct H0.
destruct po_ro.

destruct ro_resp_ex with (a := a0) as [G].
eauto.


exists y0; exists y1; exists G.
repeat split; eauto.
+{

destruct H8 with (a := a0) as [DB0]; eauto.
destruct H8 with (a := a1) as [DB1]; eauto.

assert (G =~= Y) as HX3.
eapply H2 with (a := a0); eauto.

eapply HX3; eauto.
}

}
Qed.


(******************************************************************************
 * InterpUniv is extensionaly deterministic
 *)

Lemma InterpUniv_extdeter: forall d R1,
  InterpUniv d R1 -> forall R2, InterpUniv d R2 -> R1 =~= R2.
Proof.
intros d R1 HR1.
induction HR1;
 try solve
   [ intros; inversion H; subst; reflexivity
   | intros; inversion H0; subst; reflexivity
   ]; intros.

(* Fun *)
clear_inversion H3.
eauto using InterpUniv_RelProd_extdeter.
Qed.
Hint Resolve InterpUniv_extdeter.

(******************************************************************************
 * InterpUniv respects PerUniv
 *)

Lemma InterpUniv_resp_PerUniv: forall DA DB P,
  InterpUniv DA P ->
  DA === DB #in# PerUniv ->
  InterpUniv DB P
.
Proof.
intros until P.
intro HInterp.
revert DB.

induction HInterp; intros; try solve
  [ clear_inversion H0; eauto
  | clear_inversion H; eauto
  ].

(* Fun *)
clear_inversion H3.

assert (PA0 =~= PA) by eauto.

eapply InterpUniv_fun; eauto.
-{
intros.

rename DB into DB'.
edestruct H8 as [DB].
eapply H3.
eauto.
eauto.
}

-{
intros.
edestruct H9.
eauto.
eapply H3.
eauto.
eauto.
}
Qed.
Hint Resolve InterpUniv_resp_PerUniv.

(******************************************************************************
 *)

Instance PerUniv_is_PER : PER (InRel PerUniv).
constructor 1; red. 

+{ (* Symmetry *)
intros.
induction H; auto.
-{
eapply PerUniv_ne.
symmetry; eauto.
}

-{
eapply PerUniv_fun; eauto.
intros.

assert (PA =~= P) as HX1 by eauto.

eapply H4; eauto.
assert (PER PA) by eauto.
symmetry; eauto.
eapply HX1; eauto.
}

}

+{
intros x y z.
intro Hxy.
revert z.
induction Hxy; intros z Hyz; clear_inversion Hyz; eauto.
-{
eapply PerUniv_ne.
eapply PER_Transitive; eauto.
}

-{
assert (PA =~= PA0) as HX1 by eauto.
eapply PerUniv_fun; eauto.
{
intros.
edestruct H10.
eapply HX1; eauto.
eauto.
}

{
intros.

assert (P =~= PA0) as HX2 by eauto.
assert (PER P) as HX3 by eauto.

rename DF'0 into DF''.
rename DB1 into DB2.
edestruct H2 with (a := a1) as [DB1].
{
eapply HX1; eapply HX2; eauto.
eapply Per_domR; eauto.
}

assert (DB1 === DB2 #in# PerUniv)  as HX4 by eauto.
eauto.
}
}
}
Qed.
Hint Resolve PerUniv_is_PER.

(******************************************************************************
 * PerUniv is domain for InterpUniv
 *)
Definition InterpUniv_RelProd_F dF da P := exists DB, App dF da DB /\ InterpUniv DB P.

Lemma InterpUniv_dom_PerUniv: forall d, 
  d === d #in# PerUniv -> exists y, InterpUniv d y.
Proof.
intros.
induction H; eauto.

exists (RelProd PA (InterpUniv_RelProd_F DF)).
eapply InterpUniv_fun; eauto.
{
constructor.
{
constructor.
{
intros.
red in H6; red.
decompose record H6.
rename x into DB0.
edestruct H1 with (a := a1) as [DB1]; eauto.
edestruct H2 with (a := a1) as [DB1']; eauto.

eexists; split; eauto.
eapply InterpUniv_resp_PerUniv; eauto.
eapply PER_Transitive with (DB1').
eapply H3; eauto.
symmetry.
eapply H3 with (a0 := a1); eauto.
}

{ 
intros.
destruct H2 with (a := a) as [DB']; auto.
destruct H1 with (a := a) as [DB]; auto.
edestruct H4 with (a0 := a) (a1 := a) as [Y]; eauto.
exists Y.
red. exists DB; split; eauto.
eapply InterpUniv_resp_PerUniv; eauto.
symmetry; eauto.
}

{
intros.
red in H6; red in H7.
decompose record H6.
decompose record H7.
rename x into DB0. 
rename x0 into DB1.
eapply InterpUniv_extdeter; eauto.
eapply InterpUniv_resp_PerUniv; eauto.
symmetry.
destruct H2 with (a := a1) as [DB1']; eauto.
eapply PER_Transitive with (DB1').
eapply H3; eauto.
symmetry; eapply H3 with (a0 := a1); eauto.
}
}

{
intros.
red in H6.  decompose record H6.
eauto.
}

{
eauto.
}
}

{
intros.
red in H7.
decompose record H7.
rename DB into DB'.
rename x into DB.
eapply InterpUniv_resp_PerUniv.
eauto.
eapply H3; eauto.
}
Qed.
Hint Resolve InterpUniv_dom_PerUniv.

(******************************************************************************
 * InterpUniv is proper functor over PerUniv
 *)

Definition InterpUniv_ProperRelOper: ProperRelOper PerUniv InterpUniv.
constructor 1; eauto.
Defined.


