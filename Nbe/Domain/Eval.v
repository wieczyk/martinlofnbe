(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.

Require Import Nbe.Domain.Def.
Require Import Nbe.Syntax.
Require Import Nbe.Utils.

(******************************************************************************
 * Evaluator for terms
 *)
Inductive EvalTm: Tm -> DEnv -> D -> Prop :=

| evalVar : forall d env,
  EvalTm (TmVar) (Dext env d) d

| evalAbs : forall a t env,
  EvalTm (TmAbs a t) env (Dclo t env)

| eval0 : forall env,
  EvalTm Tm0 env D0

| evalS : forall env t d,
  EvalTm t env d ->
  EvalTm (TmS t) env (DS d)

| evalApp : forall tf df tx dx dr env,
  EvalTm tf env df ->
  EvalTm tx env dx ->
  App df dx dr ->
  EvalTm (TmApp tf tx) env dr

| evalSb: forall s env envs t d,
  EvalSb s env envs ->
  EvalTm t envs d ->
  EvalTm (TmSb t s) env d

| eval1 : forall env,
  EvalTm Tm1 env D1

| evalUnit : forall env,
  EvalTm TmUnit env Dunit

| evalNat: forall denv,
  EvalTm TmNat denv Dnat

| evalFun: forall A B denv DA,
  EvalTm A denv DA ->
  EvalTm (TmFun A B) denv (Dfun DA $ Dclo B denv)

| evalEmpty: forall denv,
  EvalTm TmEmpty denv Dempty

| evalAbsurd: forall A DA denv,
  EvalTm A denv DA ->
  EvalTm (TmAbsurd A) denv (Dabsurd DA)

| evalUniv: forall denv,
  EvalTm TmUniv denv Duniv

(******************************************************************************
 * Evaluator for substitutions
 *)
with EvalSb: Sb -> DEnv -> DEnv -> Prop :=

| evalId : forall env,
  EvalSb Sid env env

| evalUp : forall d env,
  EvalSb Sup (Dext env d) env

(*
| evalUpId : 
  EvalSb Sup Did Did
*)

| evalSeq : forall env0 env1 env2 s1 s2,
  EvalSb s2 env0 env1 ->
  EvalSb s1 env1 env2 ->
  EvalSb (Sseq s1 s2) env0 env2

| evalExt : forall env0 envs s t d,
  EvalSb s env0 envs ->
  EvalTm t env0 d ->
  EvalSb (Sext s t) env0 (Dext envs d)

(******************************************************************************
 * Application
 *)
with App: D -> D -> D -> Prop :=

| appClo  : forall tm denv dx dr,
  EvalTm tm (Dext denv dx) dr ->
  App (Dclo tm denv) dx dr

| appFun : forall DA DF DB e a,
  App DF a DB ->
  App (DupX (Dfun DA DF) e) a (Dup DB (Dapp e (Ddown DA a)))

| appNeAbsurd: forall DA u,
  App (Dabsurd DA) (DupN u) (Dup DA $ DneAbsurd (DdownN DA) u)
.

Hint Constructors EvalTm.
Hint Constructors EvalSb.
Hint Constructors App.

(******************************************************************************
 * Induction scheme
 *)

Scheme EvalTm_mind := Induction for EvalTm Sort Prop
with   EvalSb_mind := Induction for EvalSb Sort Prop
with   App_mind    := Induction for App    Sort Prop
.

Combined Scheme Evals_ind from EvalTm_mind,  EvalSb_mind, App_mind.

(******************************************************************************
 * Defined relations are deterministic
 *)

Fact Evals_deter: 
  (forall tm env d1, 
    EvalTm tm env d1 -> forall d2,
    EvalTm tm env d2 ->
    d1 = d2) /\
  (forall sb env denv1,
    EvalSb sb env denv1 -> forall denv2,
    EvalSb sb env denv2 -> 
    denv1 = denv2) /\
  (forall df dx dy1,
    App df dx dy1 -> forall dy2,
    App df dx dy2 ->
    dy1 = dy2)
.
Proof.
apply Evals_ind; 
  try first
    [
      intros A B C D E; inversion E; subst; reflexivity
    | intros A B C D; inversion D; subst; reflexivity
    | intros A B C; inversion C; subst; reflexivity
    ]
.

{
intros.
clear_inversion H0.
specialize (H _ H2).
subst.
eauto.
}

{
intros tf df tx dx dr env Hdf IHdf Hdx IHdx Hdr IHdr.
intros d2 Hd2.
inversion Hd2; subst.
rewrite IHdf with df0 in *; auto.
rewrite IHdx with dx0 in *; auto.
}

{
intros.
inversion H1; subst.
rewrite H0 with d2; auto.
rewrite H with envs0; auto.
}

{
intros.
inversion H0; subst.
rewrite H with DA0 in *; auto.
}

{
intros.
clear_inversion H0.
rewrite H with DA0; auto.
}

{
intros.
inversion H1; subst.
apply H0.
rewrite H with env4; auto.
}

{
intros.
inversion H1; subst.
rewrite H with envs0 in *; auto.
rewrite H0 with d0; auto.
}

{
intros tm denv dx dr Hdr.
intros IHd2 dy2 Hdy2.
inversion Hdy2; subst.
rewrite IHd2 with dy2; auto.
}

{
intros.
clear_inversion H0.
rewrite <- H with DB0; auto.
}
Qed.


Definition EvalTm_deter := proj1 Evals_deter.
Definition EvalSb_deter := proj1 (proj2 Evals_deter).
Definition App_deter    := proj2 (proj2 Evals_deter).

Hint Resolve EvalTm_deter.
Hint Resolve EvalSb_deter.
Hint Resolve App_deter.

(******************************************************************************
 * Evaluator for contexts
 *)

Inductive EvalCxt : Cxt -> DEnv -> Prop :=
 | EvalCxt_empty:
   EvalCxt nil Did

 | EvalCxt_ext: forall Gamma A denv DA,
   EvalCxt Gamma denv ->
   EvalTm  A denv DA ->
   EvalCxt (Gamma,,A) (Dext denv $ Dup DA $ Dvar $ length Gamma)
.
Hint Constructors EvalCxt.

(******************************************************************************
 * Evaluator for context is deterministic relation
 *)

Fact EvalCxt_deter: forall Gamma env1,
  EvalCxt Gamma env1 -> forall env2,
  EvalCxt Gamma env2 -> env1 = env2
.
Proof.
intros Gamma env1 Henv1.
induction Henv1; intros; auto.

inversion H; subst; auto.
clear_inversion H0.

rewrite IHHenv1 with denv0 in *; auto.
replace DA with DA0; auto.
eapply EvalTm_deter; eauto.
Qed.
