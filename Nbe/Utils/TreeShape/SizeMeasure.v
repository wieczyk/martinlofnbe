(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Concrete measure: size of a derivation
 *)

Require Import Arith.
Require Import Arith.Max.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Nbe.Utils.ArithFuncs.
Require Import Nbe.Utils.TreeShape.Def.
Require Import Nbe.Utils.TreeShape.MeasureAxioms.
Require Import Nbe.Utils.TreeShape.MeasureFacts.

Require Import NPeano.
Import Nat.

(******************************************************************************
 * Definitions
 *)
Fixpoint tree_size ts :=
match ts with
  | Leaf =>
    1

  | Node1 ts1 =>
    S (tree_size ts1)

  | Node2 ts1 ts2 =>
    splus2 (tree_size ts1) (tree_size ts2)

  | Node3 ts1 ts2 ts3 =>
    splus3 (tree_size ts1) (tree_size ts2) (tree_size ts3)

  | Node4 ts1 ts2 ts3 ts4 =>
    splus4 (tree_size ts1) (tree_size ts2) (tree_size ts3) (tree_size ts4)
end.


Module SizeMeasure <: Measure.
  Definition mu := tree_size.
End SizeMeasure.


(******************************************************************************
 * Justification of basic axioms
 *)

Module BasicSizeMeasureAxioms <: BasicMeasureAxioms (SizeMeasure).
Import SizeMeasure.

Fact comm_node2: forall a b,
  mu (Node2 a b) = mu (Node2 b a).
Proof.
intros.
simpl.
rewrite splus2_comm.
reflexivity.
Qed.
Hint Resolve comm_node2: measure_db.

Fact comm_node3_1: forall a b c,
  mu (Node3 a b c) = mu (Node3 c a b).
Proof.
intros.
simpl.
rewrite splus3_comm_2.
auto.
Qed.
Hint Resolve comm_node3_1: measure_db.

Fact comm_node3_2: forall a b c,
  mu (Node3 a b c) = mu (Node3 c b a).
Proof.
intros.
simpl.
rewrite splus3_comm_1.
auto.
Qed.
Hint Resolve comm_node3_2: measure_db.

Fact subtree_node1: forall a,
  mu a < mu (Node1 a).
Proof.
intros.
simpl.
eauto with arith.
Qed.
Hint Resolve subtree_node1 : measure_db.

Fact subtree_node2_1: forall a b,
  mu a < mu (Node2 a b).
Proof.
intros.
simpl.
eauto with arith2_db.
Qed.
Hint Resolve subtree_node2_1 : measure_db.

Fact subtree_node3_1: forall a b c,
  mu a < mu (Node3 a b c).
Proof.
intros.
simpl.
unfold splus3.
eauto with arith.
Qed.
Hint Resolve subtree_node3_1 : measure_db.
End BasicSizeMeasureAxioms.

(******************************************************************************
 * Justification of order preserving axioms
 *)

Module OrderPreservingSizeMeasureAxioms <: OrderPreservingMeasureAxioms(SizeMeasure).
Import SizeMeasure.

Fact order_node1: forall a b,
  mu a < mu b ->
  mu (Node1 a) < mu (Node1 b)
.
Proof.
intros.
simpl.
eauto with arith.
Qed.
Hint Resolve order_node1 : measure_db.

Fact order_node2_1: forall a1 c b1,
  mu a1 < mu b1 ->
  mu (Node2 a1 c) < mu(Node2 b1 c)
.
Proof.
intros.
simpl.
unfold splus2.
eapply lt_n_S.
eauto with arith.
Qed.
Hint Resolve order_node2_1 : measure_db.

Fact order_node3_1: forall a1 b1 c d,
  mu a1 < mu b1 ->
  mu (Node3 a1 c d) < mu (Node3 b1 c d)
.
Proof.
intros.
simpl.
unfold splus3.
eapply lt_n_S.
setoid_rewrite <- plus_assoc.
eauto with arith.
Qed.
Hint Resolve order_node3_1 : measure_db.

End OrderPreservingSizeMeasureAxioms.

(******************************************************************************
 * Justification of structure-related axioms
 *)

Module StructureSizeMeasureAxioms <: StructureMeasureAxioms(SizeMeasure).
Import SizeMeasure.

Fact struct_node2_1: forall a1 a2,
  mu (Node1 a1) < mu (Node2 a1 a2)
.
Proof.
intros.
simpl.
unfold splus2.
eapply lt_n_S.
rewrite plus_comm.
destruct a2; simpl; eauto with arith.
Qed.
Hint Resolve struct_node2_1: measure_db.

Fact struct_node3_1: forall a1 a2 a3,
  mu (Node2 a1 a2) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
simpl.
unfold splus3.
unfold splus2.
eapply lt_n_S.
setoid_rewrite plus_comm at 2.
destruct a3; simpl; eauto with arith.
Qed.
Hint Resolve struct_node3_1 : measure_db.

End StructureSizeMeasureAxioms.


(******************************************************************************
 * Exports
 *)

Module BasicSizeMeasureFacts := BasicMeasureFacts(SizeMeasure)(BasicSizeMeasureAxioms).
Module OrderPreservingMeasureFacts := OrderPreservingMeasureFacts(SizeMeasure)(BasicSizeMeasureAxioms)(OrderPreservingSizeMeasureAxioms).
Module StructureSizeMeasureFacts := StructureMeasureFacts(SizeMeasure)(BasicSizeMeasureAxioms)(StructureSizeMeasureAxioms).

Export BasicSizeMeasureAxioms.
Export BasicSizeMeasureFacts.

Export OrderPreservingSizeMeasureAxioms.
Export OrderPreservingMeasureFacts.

Export StructureSizeMeasureAxioms.
Export StructureSizeMeasureFacts.

(*Global Opaque tree_size.*)
