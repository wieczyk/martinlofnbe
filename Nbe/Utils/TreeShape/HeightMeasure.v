(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Concrete measure: height of derivation
 *)

Require Import Arith.
Require Import Arith.Max.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Nbe.Utils.TreeShape.Def.
Require Import Nbe.Utils.TreeShape.MeasureAxioms.
Require Import Nbe.Utils.TreeShape.MeasureFacts.
Require Import Nbe.Utils.ArithFuncs.

Require Import NPeano.
Import Nat.

(******************************************************************************
 * Definitions
 *)
Fixpoint tree_height ts :=
match ts with
  | Leaf =>
    0

  | Node1 ts1 =>
    S (tree_height ts1)

  | Node2 ts1 ts2 =>
    smax2 (tree_height ts1) (tree_height ts2)

  | Node3 ts1 ts2 ts3 =>
    smax3 (tree_height ts1) (tree_height ts2) (tree_height ts3)

  | Node4 ts1 ts2 ts3 ts4 =>
    smax4 (tree_height ts1) (tree_height ts2) (tree_height ts3) (tree_height ts4)
end.

Module HeightMeasure <: Measure.
  Definition mu := tree_height.
End HeightMeasure.


Module HeightMeasureAxioms <: BasicMeasureAxioms (HeightMeasure).
Import HeightMeasure.

Fact comm_node2: forall a b,
  mu (Node2 a b) = mu (Node2 b a).
Proof.
intros.
simpl.
rewrite smax2_comm.
reflexivity.
Qed.
Hint Resolve comm_node2: measure_db.

Fact comm_node3_1: forall a b c,
  mu (Node3 a b c) = mu (Node3 c a b).
Proof.
intros.
simpl.
eauto with arith2_db.
Qed.
Hint Resolve comm_node3_1: measure_db.

Fact comm_node3_2: forall a b c,
  mu (Node3 a b c) = mu (Node3 c b a).
Proof.
intros.
simpl.
eauto with arith2_db.
Qed.
Hint Resolve comm_node3_2: measure_db.

Fact subtree_node1: forall a,
  mu a < mu (Node1 a).
Proof.
intros.
simpl.
eauto with arith.
Qed.
Hint Resolve subtree_node1 : measure_db.

Fact subtree_node2_1: forall a b,
  mu a < mu (Node2 a b).
Proof.
intros.
simpl.
eauto with arith2_db.
Qed.
Hint Resolve subtree_node2_1 : measure_db.

Fact subtree_node3_1: forall a b c,
  mu a < mu (Node3 a b c).
Proof.
intros.
eauto with arith2_db.
Qed.
Hint Resolve subtree_node3_1 : measure_db.

(*
Axiom lifted_node_1_2_1: forall a b,
  mu (Node1 a) < mu (Node2 (Node1 a) b).

Axiom lifted_node_1_1_2: forall a b,
  mu (Node1 a) < mu (Node1 (Node2 a b)).

Axiom lifted_node_1_2_2: forall a b c,
  mu (Node1 a) < mu (Node2 (Node2 a b) c).

Axiom lifted_node_1_2_3: forall a b c d,
  mu (Node1 a) < mu (Node2 (Node3 a b c) d).

Axiom lifted_node_1_3_2: forall a b c d,
  mu (Node1 a) < mu (Node3 (Node2 a b) c d).
*)


End HeightMeasureAxioms.

Module BasicHeightMeasureFacts := BasicMeasureFacts(HeightMeasure)(HeightMeasureAxioms).

Export HeightMeasureAxioms.
Export BasicHeightMeasureFacts.
