(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Axioms of an abstract measurement
 *)

Require Import Nbe.Utils.TreeShape.Def.
Require Import Arith.
Require Import Arith.Lt.
Require Import Arith.Le.


(******************************************************************************
 * Measure
 *)

Module Type Measure.
  Parameter mu : TreeShape -> nat.
End Measure.


Global Create HintDb measure_db.

(******************************************************************************
 * Measure
 *)

Ltac measure_solver :=
match goal with
  | _ => 
    solve [ eauto with measure_db ] 

  | |- ?a <= ?b =>
    eapply lt_le_weak; measure_solver
end
.

(******************************************************************************
 * Basic Comm Axioms
 *)
Module Type BasicMeasureAxioms (M : Measure).
Import M.

Parameter comm_node2: forall a b,
  mu (Node2 a b) = mu (Node2 b a).

Parameter comm_node3_1: forall a b c,
  mu (Node3 a b c) = mu (Node3 c a b).

Parameter comm_node3_2: forall a b c,
  mu (Node3 a b c) = mu (Node3 c b a).

Parameter subtree_node1: forall a,
  mu a < mu (Node1 a).

Parameter subtree_node2_1: forall a b,
  mu a < mu (Node2 a b).

Parameter subtree_node3_1: forall a b c,
  mu a < mu (Node3 a b c).

End BasicMeasureAxioms.

Module Type OrderPreservingMeasureAxioms (M : Measure).
Import M.

Parameter order_node1: forall a b,
  mu a < M.mu b ->
  mu (Node1 a) < mu (Node1 b)
.

Parameter order_node2_1: forall a1 c b1,
  mu a1 < M.mu b1 ->
  mu (Node2 a1 c) < mu (Node2 b1 c)
.

Parameter order_node3_1: forall a1 b1 c d,
  mu a1 < M.mu b1 ->
  mu (Node3 a1 c d) < mu (Node3 b1 c d)
.

End OrderPreservingMeasureAxioms.

Module Type StructureMeasureAxioms (M : Measure).
Import M.

Parameter struct_node2_1: forall a1 a2,
  mu (Node1 a1) < mu (Node2 a1 a2)
.

Parameter struct_node3_1: forall a1 a2 a3,
  mu (Node2 a1 a2) < mu (Node3 a1 a2 a3)
.

End StructureMeasureAxioms.

