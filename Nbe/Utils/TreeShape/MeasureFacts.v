(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Derivable facts about an abstract measure
 *)

Require Import Arith.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Nbe.Utils.TreeShape.Def.
Require Import Nbe.Utils.TreeShape.MeasureAxioms.


(******************************************************************************
 * Basic facts.
 *)

Module BasicMeasureFacts (M : Measure) (MB : BasicMeasureAxioms(M)).
Import MB.
Import M.

Fact subtree_node2_2: forall a b,
  mu b < mu (Node2 a b).
Proof.
intros.
rewrite comm_node2.
apply subtree_node2_1.
Qed.
Hint Resolve subtree_node2_2 : measure_db.

Fact subtree_node3_2: forall a b c,
  mu b < mu (Node3 a b c).
Proof.
intros.
rewrite comm_node3_1.
rewrite comm_node3_1.
apply subtree_node3_1.
Qed.
Hint Resolve subtree_node3_2 : measure_db.

Fact subtree_node3_3: forall a b c,
  mu c < mu (Node3 a b c).
Proof.
intros.
rewrite comm_node3_1.
apply subtree_node3_1.
Qed.
Hint Resolve subtree_node3_3 : measure_db.


End BasicMeasureFacts.

(******************************************************************************
 * Order preserving facts.
 *)

Module OrderPreservingMeasureFacts (M : Measure) (MB : BasicMeasureAxioms(M)) (MOP : OrderPreservingMeasureAxioms(M)).
Import MB.
Import MOP.
Import M.

Fact order_node2_2: forall a1 c b1,
  mu a1 < mu b1 ->
  mu (Node2 c a1) < mu(Node2 c b1)
.
Proof.
intros.
setoid_rewrite comm_node2.
apply order_node2_1.
assumption.
Qed.
Hint Resolve order_node2_2: measure_db.

Fact order_node3_2: forall a1 b1 c d,
  mu a1 < mu b1 ->
  mu (Node3 c a1 d) < mu (Node3 c b1 d)
.
Proof.
intros.
setoid_rewrite <- comm_node3_1.
apply order_node3_1.
assumption.
Qed.
Hint Resolve order_node3_2 : measure_db.

Fact order_node3_3: forall a1 b1 c d,
  mu a1 < mu b1 ->
  mu (Node3 c d a1) < mu (Node3 c d b1)
.
Proof.
intros.
setoid_rewrite comm_node3_1.
apply order_node3_1.
assumption.
Qed.
Hint Resolve order_node3_3 : measure_db.


Fact order_node2_all: forall a1 b1 a2 b2,
  mu a1 < mu b1 -> mu a2 < mu b2 ->
  mu (Node2 a1 a2) < mu (Node2 b1 b2).
Proof.
intros.
eapply lt_trans.
eapply order_node2_1.
eauto.
eapply order_node2_2.
eauto.
Qed.
Hint Resolve order_node2_all : measure_db.

Fact order_node3_all: forall a1 b1 a2 b2 a3 b3,
  mu a1 < mu b1 -> mu a2 < mu b2 -> mu a3 < mu b3 ->
  mu (Node3 a1 a2 a3) < mu (Node3 b1 b2 b3).
Proof.
intros.
eapply lt_trans.
eapply order_node3_1.
eauto.
eapply lt_trans.
eapply order_node3_2.
eauto.
eapply order_node3_3.
eauto.
Qed.
Hint Resolve order_node3_all : measure_db.

(*
Fact order: forall a1 b1 a2 b2,
  mu (Node2 a1 b1) < mu (Node2 (Node2 a1 a2) (Node2 b1 b2)).
Proof.
intros.
eapply order_node2_all.
eapply subtree_node2_1.
eapply subtree_node2_1.
Qed.
*)
End OrderPreservingMeasureFacts.


Module StructureMeasureFacts (M : Measure) (MB:BasicMeasureAxioms(M)) (MS : StructureMeasureAxioms(M)).
Import MB.
Import MS.
Import M.

Fact struct_node2_2: forall a1 a2,
  mu (Node1 a2) < mu (Node2 a1 a2)
.
Proof.
intros.
rewrite comm_node2.
eapply struct_node2_1.
Qed.
Hint Resolve struct_node2_2: measure_db.

Fact struct_node3_2: forall a1 a2 a3,
  mu (Node2 a1 a3) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
rewrite comm_node3_2.
rewrite comm_node3_1.
apply struct_node3_1.
Qed.
Hint Resolve struct_node3_2: measure_db.

Fact struct_node3_3: forall a1 a2 a3,
  mu (Node2 a2 a1) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
rewrite comm_node2.
apply struct_node3_1.
Qed.
Hint Resolve struct_node3_3: measure_db.

Fact struct_node3_4: forall a1 a2 a3,
  mu (Node2 a2 a3) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
rewrite <- comm_node3_1.
apply struct_node3_1.
Qed.
Hint Resolve struct_node3_4: measure_db.

Fact struct_node3_5: forall a1 a2 a3,
  mu (Node2 a3 a1) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
rewrite comm_node2.
apply struct_node3_2.
Qed.
Hint Resolve struct_node3_5: measure_db.

Fact struct_node3_6: forall a1 a2 a3,
  mu (Node2 a3 a2) < mu (Node3 a1 a2 a3)
.
Proof.
intros.
rewrite comm_node2.
apply struct_node3_4.
Qed.
Hint Resolve struct_node3_6: measure_db.

End StructureMeasureFacts.

