(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Require Import Arith.
Require Import Arith.MinMax.
Require Import Arith.Max.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Arith.Mult.
Require Import Nbe.Utils.ArithFuncs.
Require Import Nbe.Utils.TreeShape.

(******************************************************************************
 * Definitions
 *)

Inductive tree_shape : Set :=
  | Leaf:
    tree_shape

  | Node1:
    tree_shape ->
    tree_shape

  | Node2:
    tree_shape ->
    tree_shape ->
    tree_shape

  | Node3:
    tree_shape ->
    tree_shape ->
    tree_shape ->
    tree_shape
.

(******************************************************************************
 * Measures
 *)

Fixpoint tree_height ts :=
match ts with
  | Leaf =>
    0

  | Node1 ts1 =>
    S (tree_height ts1)

  | Node2 ts1 ts2 =>
    smax2 (tree_height ts1) (tree_height ts2)

  | Node3 ts1 ts2 ts3 =>
    smax3 (tree_height ts1) (tree_height ts2) (tree_height ts3)
end.

Fixpoint tree_size ts :=
match ts with
  | Leaf =>
    0

  | Node1 ts1 =>
    S (tree_size ts1)

  | Node2 ts1 ts2 =>
    splus2 (tree_size ts1) (tree_size ts2)

  | Node3 ts1 ts2 ts3 =>
    splus3 (tree_size ts1) (tree_size ts2) (tree_size ts3)
end.

*)

