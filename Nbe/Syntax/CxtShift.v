(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.Inversion.
Require Import Nbe.Syntax.Validity.
Require Import Nbe.Utils.
Require Import Nbe.Syntax.ContextConversion.
Require Import Nbe.Syntax.SbInversion.

(******************************************************************************
 * CxtShift
 *)

Definition CxtShift Delta i Gamma := Delta |-- [Sups i] : Gamma
.
Hint Unfold CxtShift.

(******************************************************************************
 * Composing SUPs
 *)

Fact Sups_tail: forall i Gamma Delta,
 Gamma |-- [Sups (S i)] : Delta ->
  Gamma |-- [Sups (S i)] === [Sseq Sup (Sups i)] : Delta
.
Proof.
intro i.
induction i; intros.
{
simpl.
clear_inversion_on_sb H.
clear_inversion_on_sb H5.
convert_all_contexts HCXTEQ.

eapply EQ_SB_TRANS.
eapply EQ_SB_SIDL.
auto.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SIDR.
auto.
eapply EQ_SB_REFL.
auto.
}
(**)
{
simpl in H.
simpl.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.

assert (Gamma__1 |-- [Sseq (Sups i) Sup] === [Sseq Sup (Sups i)] : Delta).
{
apply IHi.
auto.
}

assert (Gamma__1 |-- [Sseq Sup (Sups i)] : Delta) by eauto.

clear_inversion_on_sb H1.
clear_inversion_on_sb H8.
clear_inversion_on_sb H5.
clear_inversion_on_sb H8.
rename A0 into B.
rename A1 into C.
 
assert (Gamma |-- [Sup] : Gamma__1) as HINV1.
{
eapply ContextConversion_sb with (Psi := (Gamma__1)).
eapply SUP.
eauto.
eauto.
}

eapply EQ_SB_TRANS.
{
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL.
eauto.
eapply H0.
}
{
eapply EQ_SB_COMM.
eauto.
eauto.
eapply ContextConversion_sb. 
eapply SUP.
eauto.
eauto.
}
}
Qed.

(******************************************************************************
 *)

Fact CxtShift_decomp: forall Gamma Psi i j,
  Gamma |-- ->
  CxtShift Psi (j + i) Gamma ->
  exists Delta,
    CxtShift Psi j Delta /\ CxtShift Delta i Gamma
.
Proof.
intros Gamma Psi i j Hcxt H.
red in H.
assert (Psi |--) by eauto.

generalize dependent Gamma.
generalize dependent Psi.
generalize dependent j.
induction j; intros; simpl.
{
simpl in *; exists Psi; split; auto; red.
}
(**)
{

simpl in H.
clear_inversion_on_sb H.
clear_inversion_on_sb H5.
assert (Gamma__1 |--) as HINV1 by eauto.

specialize (IHj _ HINV1 _ Hcxt H6).
destruct IHj as [Delta HDelta].
destruct HDelta.

exists Delta.
split; auto.
red.
simpl in *.
eapply SSEQ.
eapply ContextConversion_sb with (Psi := (Gamma__1)).
eapply SUP.
eauto.
eauto.
eauto.
}
Qed.

(******************************************************************************
 *)

Fact Sups_shape: forall i Gamma Delta T,
  Gamma |--  [Sups i]: Delta ,, T ->
    exists Psi B, |-- {Gamma} === {Psi,,B}
.
Proof.
induction i; intros.
{
simpl in H; clear_inversion_on_sb H; eauto.
}
{
simpl in H.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.

exists Gamma__1. exists A.
eauto.
}
Qed.

Fact Sups_mono: forall i Gamma Delta T C,
  Gamma,,T |-- [Sups i] : Delta,,C ->
  Gamma    |-- [Sups i] : Delta
.
Proof.
induction i; intros.
(**)
{
simpl in H.
clear_inversion_on_sb H.
clear_inversion H0.
simpl.
specialize (EQ_CXT_EXT_inv1 HCXTEQ); intros; eauto.
}
(**)
{
simpl in *.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.

edestruct Sups_shape with (i := i) (Gamma := Gamma__1) as [Psi [X]]. eauto.
subst.
 
eapply SSEQ.
eapply ContextConversion_sb with (Gamma := Psi,,X).
{
eapply SUP.
assert (Psi,,X |--) as HINV1 by eauto.
clear_inversion HINV1.
eauto.
}

eapply EQ_CXT_TRANS.
eauto.
eapply EQ_CXT_EXT_inv1.
eauto.

eapply IHi.
eapply ContextConversion_sb with (Gamma := Gamma__1).
eauto.
eauto.
}
Qed.

(******************************************************************************
 *)


Fact SUP_cxteq: forall Gamma Delta A,
  |-- {Gamma} === {Delta,,A} ->
  Gamma |-- [Sup] : Delta.
Proof.
intros.
assert (Delta,,A |--) as HINV1.
eauto.
clear_inversion HINV1.

eapply ContextConversion_sb.
eapply SUP.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Fact Sups_tails: forall i j Gamma Delta,
 Gamma |-- [Sups (i + j)] : Delta ->
  Gamma |-- [Sups (i + j)] === [Sseq (Sups j) (Sups i)] : Delta
.
Proof.
induction i; intros.
{
simpl in *; eauto.
}
(**)
{
rewrite plus_comm in *.
rewrite <- plus_Snm_nSm in *.
rewrite plus_comm in *.

specialize (IHi (S j) Gamma Delta H).
eapply EQ_SB_TRANS.
{
apply IHi.
}

assert (Gamma |--) as Hcxt by eauto.
assert (Delta |--) as Hcxt' by eauto.

specialize CxtShift_decomp; intro HCxtShift_decomp.
specialize (HCxtShift_decomp Delta Gamma (S j) i Hcxt' H).
destruct HCxtShift_decomp as [Psi].
destruct H0.

red in H0.
red in H1.
simpl in H1. 
clear_inversion_on_sb H1.
clear_inversion_on_sb H6.

eapply EQ_SB_TRANS.
{
simpl.
eapply EQ_SB_COMM.
eauto.
eapply SUP_cxteq. eauto.
eauto.
}

eapply EQ_SB_CONG_SSEQ; eauto.
apply EQ_SB_SYM.
apply Sups_tail.
simpl.
 
edestruct Sups_shape with (i := i) (Gamma := Gamma) as [Theta [T]]; eauto.

eapply SSEQ.
eapply SUP_cxteq.
eauto.

eapply Sups_mono.
eapply ContextConversion_sb.
eauto.
eauto.
}
Qed.

(******************************************************************************
 *)

Fact Sups_tails': forall i j Gamma Delta,
 Gamma |-- [Sseq (Sups j) (Sups i)] : Delta ->
  Gamma |-- [Sups (i + j)] === [Sseq (Sups j) (Sups i)] : Delta
.
Proof.
induction i; intros.
{
simpl in *.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.
convert_all_contexts (EQ_CXT_SYM HCXTEQ).
eauto.
}
(**)
{
simpl in H.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.
clear_inversion_on_sb H3.

assert (Gamma__0 |-- [Sseq (Sups j) (Sups i)] : Delta).
{
eapply SSEQ.
eauto.
eauto.
}

specialize (IHi j _ _ H0).
apply Sups_tails.
simpl.

eapply SSEQ.
eapply SUP_cxteq.
eauto.

eauto.
}
Qed.

(******************************************************************************
 *)


Fact Sups_tails_iff: forall Gamma i j Delta,
  Gamma |-- [Sseq (Sups j) (Sups i)] : Delta 
    <->
  Gamma |-- [Sups (i + j)] : Delta
.
Proof.
intros.
split; intro HP.
eapply WtSb_from_WtSbEq1.
apply Sups_tails'.
auto.

eapply WtSb_from_WtSbEq2.
apply Sups_tails.
auto.
Qed.

(******************************************************************************
 *)

Fact Sups_tails_iff': forall Gamma i j Delta,
  Gamma |-- [Sseq (Sups j) (Sups i)] : Delta 
    <->
  Gamma |-- [Sseq (Sups i) (Sups j)] : Delta 
.
Proof.
intros.
split; intro HP;
apply Sups_tails_iff;
rewrite plus_comm;
apply Sups_tails_iff;
auto.
Qed.

(******************************************************************************
 *)
Fact Sups_tail_iff: forall Gamma i Delta,
  Gamma |-- [Sseq (Sups i) Sup] : Delta
    <->
  Gamma |-- [Sseq Sup (Sups i)] : Delta
.
Proof.
intros.
split; intro HX1.
{
eapply WtSb_from_WtSbEq2.
eapply Sups_tail.
eauto.
}

assert (Gamma |-- [Sseq Sup (Sups i)] === [Sseq (Sups 1) (Sups i)] : Delta) as HX2.
{
clear_inversion_on_sb HX1.
eapply EQ_SB_CONG_SSEQ.
eauto.
simpl.
eapply EQ_SB_SYM.
eauto.
}

assert (Gamma |-- [Sseq (Sups 1) (Sups i)] : Delta) as HX3 by eauto.

assert (Gamma |-- [Sseq (Sups i) (Sups 1)] : Delta) as HX4.
{
eapply Sups_tails_iff'.
auto.
}

assert (Gamma |-- [Sseq (Sups i) Sup] === [Sseq (Sups i) (Sups 1)] : Delta) as HX5.
{
clear_inversion_on_sb HX4.
clear_inversion_on_sb H3.
clear_inversion_on_sb H6.


eapply EQ_SB_CONG_SSEQ.
simpl.
eauto.
eapply EQ_SB_REFL.
eapply ContextConversion_sb.
eauto.
eauto.
}

eauto.
Qed.

(******************************************************************************
 *)

Fact Sups_tails'': forall i j Gamma Delta,
 Gamma |-- [Sseq (Sups j) (Sups i)] : Delta ->
  Gamma |--  [Sseq (Sups i) (Sups j)] === [Sseq (Sups j) (Sups i)] : Delta
.
Proof.
intros.
eapply EQ_SB_TRANS.
eapply EQ_SB_SYM.
eapply Sups_tails'.
apply Sups_tails_iff'; auto.
rewrite plus_comm.
eapply Sups_tails'; auto.
Qed.


(******************************************************************************
 *)

Fact Sups_tail': forall i Gamma T Delta,
  CxtShift Delta i (Gamma,,T) ->
  Delta |-- [Sseq (Sups i) Sup] === [Sseq Sup (Sups i)] : Gamma
.
Proof.
intros.
red in H.

assert (Gamma,,T |--) as HGT by eauto.

clear_inversion HGT.



assert (Delta |-- [Sseq Sup (Sups i)] : Gamma) as Hsseq.
eapply SSEQ.
eauto.
simpl.
eapply SUP.
eauto.


eapply Sups_tail.
simpl.
eapply Sups_tail_iff.
eauto.
Qed.



(******************************************************************************
 * Basic properties
 *)

Fact CxtShift_comp1:  forall Gamma i Delta Psi,
  CxtShift Delta i Gamma -> CxtShift Psi 1 Delta ->
  CxtShift Psi (1+i) Gamma
.
Proof.
intros.
unfold CxtShift in *; simpl in *.
clear_inversion_on_sb H0.
clear_inversion_on_sb H6.
clear_inversion_on_sb H5.
eapply SSEQ; eauto.
eapply SB_CONV; eauto.
eapply ContextConversion_sb.
eapply SUP.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Fact CxtShift_comp: forall Gamma Delta i Psi j,
  CxtShift Delta i Gamma -> CxtShift Psi j Delta ->
  CxtShift Psi (j+i) Gamma
.
Proof.
intros.
unfold CxtShift in *.
generalize dependent Delta.
generalize dependent Gamma.
generalize dependent Psi.
(**)
induction j; simpl; intros.
{
clear_inversion_on_sb H0; auto.
convert_all_contexts (EQ_CXT_SYM HCXTEQ).
eauto.
}
(**)
{
clear_inversion_on_sb H0.
clear_inversion_on_sb H5.
eapply SSEQ; eauto.
eapply SUP_cxteq.
eauto.
}
Qed.  
Hint Resolve CxtShift_comp.


(******************************************************************************
 *)

Lemma CxtShift_deter: forall i Gamma Delta Psi,
  CxtShift Delta i Gamma -> CxtShift Delta i Psi ->
  |-- {Gamma} === {Psi}
.
Proof.
induction i.
{
intros.
clear_inversion_on_sb H.
clear_inversion_on_sb H0.
eauto.
}

{
intros.
red in H; simpl in H.
red in H0; simpl in H0.
clear_inversion_on_sb H.
clear_inversion_on_sb H0.
clear_inversion_on_sb H4.
clear_inversion_on_sb H5.

assert ( |-- {Gamma__0} === {Gamma__1} ) as HINV1.
{
eapply EQ_CXT_EXT_inv1.
eauto.
}

eapply IHi.
eauto.
eapply ContextConversion_sb.
eauto.

eauto.
}
Qed.

(******************************************************************************
 *)

Lemma Inversion_SBTP: forall Gamma t sb T,
  Gamma |-- TmSb t sb : T ->
    exists T', Gamma |-- T === TmSb T' sb
.
Proof.
intros.
remember (TmSb t sb).
generalize dependent t.
generalize dependent sb.
induction H; intros; try subst; try solve [ clear_inversion Heqt0 ].
(**)
{
clear_inversion Heqt0; exists A; eauto.
}
(**)
{
edestruct IHWtTm as [T'].
eauto.
exists T'.
eauto.
}
Qed.

(******************************************************************************
 *)

Lemma Inversion_SB: forall Gamma t sb T,
  Gamma |-- TmSb t sb : T ->
  exists  Delta C,
    Gamma |-- [sb] : Delta /\ Delta |-- t : C /\ Gamma |-- T === TmSb C sb
.
Proof.

intros.
remember (TmSb t sb) as X.
generalize dependent t.
generalize dependent sb.


induction H; intros; subst;
 try solve [clear_inversion HeqX].
{
clear_inversion HeqX.

exists Delta; exists A.
repeat (split; auto).
eauto.
}
(**)
{
edestruct IHWtTm as [Delta [C [HI1 [HI2 HI3]]]].
eauto.
exists Delta. exists C.
repeat (split; auto).
eauto.
}
Qed.


(******************************************************************************
 *)
  

Fact nc_Sups_tp_plus_eq: forall j i t Gamma Delta,
  CxtShift Gamma (i+j) Delta ->
  Delta |-- t ->
  Gamma |-- TmSb t (Sups (i + j))  ->
  Gamma |-- TmSb t (Sups (i + j)) === TmSb (TmSb t (Sups i)) (Sups j) 
.
Proof.
intros.
red in H.


assert (Delta |--) as HX1.
{
eauto.
}

rewrite plus_comm in H.
edestruct CxtShift_decomp as [Psi].
eauto.
eauto.
decompose record H2.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
rewrite plus_comm.
eapply Sups_tails.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Fact nc_Sups_tp_plus_eq': forall j i t Gamma Delta,
  CxtShift Gamma (i+j) Delta ->
  Delta |-- t ->
  Gamma |-- TmSb (TmSb t (Sups i)) (Sups j)  ->
  Gamma |-- TmSb t (Sups (i + j)) === TmSb (TmSb t (Sups i)) (Sups j) 
.
Proof.
intros.
eapply nc_Sups_tp_plus_eq.
eauto.
eauto.
eapply SB_F.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
 
Fact nc_Sups_plus_eq: forall j i t T Gamma,
  Gamma |-- TmSb t (Sups (i + j)) : T ->
  Gamma |-- TmSb t (Sups (i + j)) === TmSb (TmSb t (Sups i)) (Sups j) : T
.
Proof.

intros.
edestruct Inversion_SB as [Psi].
eauto.
decompose record H0.
rename x into C.


assert (Psi |--) as HX1.
eauto.

rewrite plus_comm in H2.
edestruct CxtShift_decomp as [Delta].
eauto.
eauto.
decompose record H3.


assert (Gamma |-- TmSb (TmSb C (Sups i)) (Sups j) === TmSb C (Sups (i + j))) as HX2.
{
eapply EQ_TP_SYM.
eapply nc_Sups_tp_plus_eq.
rewrite plus_comm.
eauto.
eauto.
eauto.
}

assert (Gamma |-- TmSb C (Sseq (Sups i) (Sups j)) === TmSb C (Sups (i + j))) as HX3.
{
eapply EQ_TP_CONG_SB.
rewrite plus_comm.
eapply EQ_SB_SYM.
eapply Sups_tails.
eauto.
eauto.
}

eapply EQ_CONV; [ | eapply EQ_TP_SYM; apply H4 ].

eapply EQ_SYM.
eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.
assumption.

eapply EQ_CONV.
eapply EQ_CONG_SB.
eapply EQ_SB_SYM.
rewrite plus_comm.
eapply Sups_tails.
eauto.
eauto.

assumption.
Qed.


(******************************************************************************
 *)

Fact Sups_plus: forall j i t T Gamma,
  Gamma |-- TmSb t (Sups (i + j)) : T ->
  Gamma |-- TmSb (TmSb t (Sups i)) (Sups j) : T
.
Proof.
intros.
specialize nc_Sups_plus_eq.
intros.
specialize (H0 j i t T Gamma H).
eauto.
Qed.

(******************************************************************************
 *)

Lemma Sups_weird: forall Gamma T Delta i,
  CxtShift Delta i (Gamma,,T) ->
  Delta |-- TmSb T (Sseq (Sups i) Sup) === TmSb T (Sseq Sup (Sups i))
.
Proof.
intros.
eapply EQ_TP_CONG_SB.
eapply Sups_tail'.
eauto.
red in H.
eapply EQ_TP_REFL.
assert (Gamma,,T |--).
eauto.
clear_inversion H0.
auto.
Qed.

(******************************************************************************
 *)
Lemma Sups_weird': forall Gamma T Delta i,
  CxtShift Delta i (Gamma,,T) ->
  Delta |-- TmSb T (Sseq (Sups i) Sup) === TmSb (TmSb T Sup) (Sups i)
.
Proof.
intros.

assert (Gamma,,T |--).
eauto.
clear_inversion H0.


eapply EQ_TP_TRANS.
eapply Sups_weird.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBSEQ.
eauto.
eapply SUP.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Lemma Sups_weird'': forall Gamma T A Delta i,
  CxtShift Delta i (Gamma,,T) ->
  Gamma,,T |-- TmSb T Sup === A ->
  Delta |-- TmSb T (Sseq (Sups i) Sup) === TmSb A (Sups i)
.
Proof.
intros.

assert (Gamma,,T |--).
eauto.
clear_inversion H1.

eapply EQ_TP_TRANS.
eapply Sups_weird'.
eauto.

eapply EQ_TP_CONG_SB.
eauto.
eauto.
Qed.

(******************************************************************************
 * Lengths
 *)

Fact EQ_CXT_length: forall Gamma Delta,
  |-- {Gamma} === {Delta} ->
  length Gamma = length Delta
.
Proof.
intros.
induction H.
eauto.
simpl.
rewrite IHWfCxtEq.
eauto.
Qed.

Fact CxtShift_length: forall i Gamma Delta ,
  CxtShift Delta i Gamma ->
  length Delta = length Gamma + i
.
Proof.
induction i; intros.
rewrite plus_0_r.
eapply EQ_CXT_length.
clear_inversion_on_sb H.
eauto.
(**)
red in H. simpl in H. clear_inversion_on_sb H.
(**)
clear_inversion_on_sb H4.
simpl.
specialize (IHi Gamma Gamma__1 H5).
erewrite EQ_CXT_length with (Delta := Gamma__1,,A).
simpl.
rewrite IHi.
auto with arith.

eauto.
Qed.

(******************************************************************************
 * TmVar
 *)

Fact minus_S: forall a,
  S a - a = 1
.
Proof.
induction a; auto.
Qed.

(******************************************************************************
 *)

Fact HYP_cxteq: forall Gamma T Delta,
  |-- {Delta} === {Gamma,,T} ->
  Delta |-- TmVar : TmSb T Sup.
Proof.
intros.
assert (Gamma,,T |-- ) as HINV1.
eauto.

eapply ContextConversion_tm.
eapply HYP.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Fact CxtShift_TmVarSups: forall i Delta Gamma T,
  CxtShift Delta i (Gamma,,T) ->
  Gamma,,T |-- TmVar : TmSb T Sup ->
  Delta |-- TmSb TmVar (Sups i) === tmVar (length Delta - length (Gamma,,T)) : TmSb T (Sups (S i))
.
Proof.
induction i; intros.
(**)
{
red in H.
clear_inversion_on_sb H.
clear_inversion H1.
erewrite EQ_CXT_length; eauto.
rewrite minus_diag.
simpl.
apply EQ_SBID.
eapply CONV.
eapply HYP_cxteq.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
apply EQ_SB_SIDL.
eapply SUP_cxteq.
eauto.
eauto.
}
(**)
{
red in H.
simpl in *.
clear_inversion_on_sb H.
clear_inversion_on_sb H5.

specialize (IHi Gamma__1 Gamma T H6).
specialize (CxtShift_length H6); intros.

simpl in *.
rewrite H1 in *.
simpl in IHi.
assert (length Gamma + S i = S (length Gamma + i)) by (auto with arith).
erewrite EQ_CXT_length; eauto.
simpl.
rewrite H1.
rewrite <- H2.
rewrite minus_plus in *.
simpl.
eapply EQ_CONV.
eapply EQ_TRANS.
{
apply EQ_SYM.
eapply EQ_SBSEQ.
eapply SUP_cxteq.
eauto.
eauto.
eauto.
}

eapply EQ_CONG_SB.
eapply EQ_SB_REFL.
eapply SUP_cxteq.
eauto.


assert (Gamma,,T |--) as HX1 by eauto.
clear_inversion HX1.
  
eapply EQ_CONV.
apply IHi.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
eapply Sups_tail.
eapply Sups_tail_iff.
eauto.
eauto.

assert (Gamma,,T |--) as HX1 by eauto.
clear_inversion HX1.
 
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SUP_cxteq.
eauto.
eauto.
eauto.
eapply EQ_TP_TRANS.
{
eapply EQ_TP_SBSEQ.
eapply SSEQ.
eapply SUP_cxteq.
eauto.
eauto.
eauto.
eauto.
}

eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
{
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.
eapply SUP_cxteq.
eauto.
eauto.
eauto.
}

eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL.
eapply SUP_cxteq.
eauto.
eapply EQ_SB_SYM.
eapply Sups_tail.
eapply Sups_tail_iff.
eauto.
eauto.
}
Qed.


