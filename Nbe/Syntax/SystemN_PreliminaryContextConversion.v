(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Preliminary context conversion theorem.
 *
 * It is a context-conversion theorem based on extended equality for typing
 * contexts.
 *)


Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.
Require Import Wf_nat.
Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_wfind.
Require Import Nbe.Utils.TreeShape.
Require Import Nbe.Utils.TreeShape.HeightMeasure.
Require Import Nbe.Utils.TreeShape.SizeMeasure.
Require Import Utils.Tactics.
Require Import Nbe.Utils.ArithFuncs.
Require Import Arith.
Require Import Arith.Le.
Require Import Arith.Lt.
Require Import Arith.Max.
Import NPeano.
Import Nat.



(******************************************************************************
 * Extended equivalence for typing contexts.
 *
 * It contains missing derivation that we cannot infer without validity
 * theorem
 *)
 
Inductive PreCxtConv : TreeShape -> Cxt -> Cxt -> Prop :=
| PreCxtConv_EMPTY: 
  PreCxtConv Leaf [] []

| PreCxtConv_EXT: forall shc1 shc2 shc3 shc4 shc5 Gamma Psi A B,
  PreCxtConv shc1 Gamma Psi ->
  Psi << shc2 >> |-- A === B ->
  Psi << shc3 >>  |-- A ->
  Psi << shc4 >>  |-- B ->
 << shc5 >> |-- {Gamma,,A} === {Psi,,B} ->
  PreCxtConv (Node3 shc1 shc2 shc3) (Gamma,,A) (Psi,,B)
.

(******************************************************************************
 *)

Lemma PreCxtConv_WfCxtEq: forall sh Gamma Psi, PreCxtConv sh Gamma Psi -> exists ssh, << ssh >> |-- {Gamma} === {Psi}.
Proof.
intros.
clear_inversion H.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Lemma PreCxtConv_WfCxtEq_rev: forall sh Gamma Psi, PreCxtConv sh Gamma Psi -> exists ssh, << ssh >> |-- {Psi} === {Gamma}.
Proof.
intros.
induction H.
eauto.

clear_inversion H3.
eauto.

edestruct IHPreCxtConv.
eauto.
Qed.


(******************************************************************************
 *)
Local Ltac resolveSimpleMeasure WFIH Phi ts HPCC :=
  edestruct WFIH with (Psi := Phi) (m := tree_height ts); 
  eauto;
  try measure_solver;
  eauto;
  program_simpl; eauto.


(******************************************************************************
 *)
Local Ltac directContextConversion WFIH Psi ts :=
match goal with

| [ H : ?Gamma << ts >> |-- ?T  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- T ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| [ H : ?Gamma << ts >> |-- ?T1 === ?T2  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- T1 === T2 ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| [ H : ?Gamma << ts >> |-- ?t : ?T  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- t : T ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| [ H : ?Gamma << ts >> |-- ?t1 === ?t2 : ?T  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- t1 === t2 : T ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| [ H : ?Gamma << ts >> |-- [?SB] : ?Delta  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- [SB] : Delta ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| [ H : ?Gamma << ts >> |-- [?SB1] === [?SB2] : ?Delta  |- _ ] => 
  let HPCC := fresh "HPCC0" in
  assert (exists shh1, Psi << shh1 >> |-- [SB1] === [SB2] : Delta ) as HPCC
    by (resolveSimpleMeasure WFIH Psi ts HPCC)

| _ =>
  fail 
end.


(******************************************************************************
 *)

Theorem PreliminaryContextConversion: forall n Gamma Psi ch dh, PreCxtConv ch Gamma Psi  -> n = tree_height dh ->
  (Gamma << dh >> |-- -> exists dhh, Psi << dhh >> |--) /\

  (forall T hhh, Gamma << dh >> |-- T ->  Psi << hhh >>|-- -> exists dhh, Psi << dhh >> |-- T) /\

  (forall t T hhh, Gamma << dh >> |-- t : T -> Psi << hhh >> |-- -> exists dhh, Psi << dhh >> |-- t : T) /\

  (forall sb Delta hhh, Gamma << dh >> |-- [sb] : Delta ->  Psi << hhh >> |-- -> 
  exists dhh , Psi << dhh >>  |-- [sb] : Delta) /\

  (forall T1 T2 hhh, Gamma << dh >> |-- T1 === T2 -> Psi << hhh >> |-- -> exists dhh, Psi << dhh >> |-- T1 === T2) /\

  (forall sb1 sb2 Delta hhh, Gamma << dh >> |-- [sb1] === [sb2] : Delta -> Psi << hhh >> |-- ->
    exists dhh, Psi << dhh >> |-- [sb1] === [sb2] : Delta) /\

  (forall t1 t2 T hhh, Gamma << dh >> |-- t1 === t2 : T -> Psi << hhh >> |-- -> exists dhh, Psi << dhh >> |-- t1 === t2 : T)
.
Proof.
wfind.
split; repeat split; intros.
(************  Cxt validity *)
+{
clear_inversion H1.
(**)
-{
clear_inversion H.
eauto.
}
(**) 
-{
clear_inversion H; eauto.

assert (exists hh1, Psi0 << hh1 >> |--) as HH1.
edestruct WFIH with (m := tree_height sh1); eauto.
measure_solver.

program_simpl; eauto.
}

}
(********** WfTp*)
+{
clear_inversion H1; try solve [ edestruct WFIH with (m := tree_height sh); eauto; measure_solver ].
(* from TmUniv *)
-{
directContextConversion WFIH Psi sh.
decompose record HPCC0.
eauto.
}
(* TmFun *)
-{
directContextConversion WFIH Psi sh1.


assert (exists hhh, Psi,,A << hhh >> |-- B ) as HX2.
{
program_simpl.
edestruct WFIH with (Psi := Psi,,A) (m := tree_height sh2); eauto; try measure_solver; program_simpl; eauto.
edestruct PreCxtConv_WfCxtEq. eauto.
econstructor 2; eauto.
}

eauto.
program_simpl.
eauto.
}

(* TmSb *)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
}
(********************* WtTm*)

+{
clear_inversion H1; 
try solve [ edestruct WFIH with (m := tree_height sh); eauto; program_simpl; eauto; edestruct H0; eauto ].

-{
directContextConversion WFIH Psi sh1.

assert (exists hhh, Psi << hhh >> |-- A ) as HX2.
{
destruct HPCC0.
eauto.
}

assert (exists hhh, Psi,,A << hhh >> |-- B : TmUniv) as HX3.
{
program_simpl.
edestruct WFIH with (Psi := Psi,,A) (m := tree_height sh2); eauto; try measure_solver; program_simpl; eauto.
edestruct PreCxtConv_WfCxtEq. eauto.
econstructor 2; eauto.
}

eauto.
program_simpl.
eauto.


}

-{
(* TmVar *)

clear_inversion H; eauto.

rename T0 into A.
rename B into AA.
rename Gamma0 into Gamma.

 
assert (exists hhh, Psi0 << hhh >> |-- ) as HX1.
{
clear_inversion H2.
eauto.
}

assert (exists hhh, Psi0 << hhh >> |-- AA === A) as HXX3.
{
program_simpl.
eauto.
}

assert (exists hhh, Psi0,,AA << hhh >> |-- TmSb AA Sup === TmSb A Sup) as HX3.
{
program_simpl.
eauto.
}

assert (exists hhh, Psi0,,AA << hhh >> |-- TmVar : TmSb AA Sup) as HX4.
{
program_simpl.
eauto.
}

program_simpl.
eauto.
}
 
(* TmApp *)
-{
directContextConversion WFIH Psi sh2.
directContextConversion WFIH Psi sh1.
program_simpl.

assert (exists hhh, Gamma,, A << hhh >> |--  /\ tree_height hhh <= tree_height sh3). 
{
eapply ContextValidityN_WfTp.
eauto.
}

program_simpl.
clear_inversion H7.

assert (exists hhh, Psi << hhh >> |-- A) as HX3.
{
edestruct WFIH with (Gamma := Gamma ) (m := tree_height sh4); eauto; try measure_solver; program_simpl; eauto.

eapply lt_trans with (m := tree_height sh3).
simpl in H8.
unfold smax2 in H8.
unfold max2 in H8.
rewrite succ_max_distr in H8.
red.
eapply max_lub_r.
eapply H8.
measure_solver.
}

program_simpl.

assert (exists hhh, PreCxtConv hhh (Gamma,,A) (Psi,,A)) as HX4.
{
eexists.

edestruct PreCxtConv_WfCxtEq. eauto.
econstructor 2; eauto.
}

assert (exists hhh, Psi ,, A << hhh >> |--  B) as HX5.
{
program_simpl.
edestruct WFIH with (Gamma := Gamma,,A)  (m := tree_height sh3); eauto; try measure_solver; program_simpl; eauto.
}

program_simpl.
eauto.

}

(* TmAbs *)
-{ 
directContextConversion WFIH Psi sh1.

 
assert (exists hhh, Psi,,A << hhh >> |-- M : B ) as HX2.
{
program_simpl.
edestruct WFIH with (Psi := Psi,,A) (m := tree_height sh2); eauto; try measure_solver; program_simpl.
edestruct PreCxtConv_WfCxtEq. eauto.
econstructor 2.
eauto.
eapply EQ_TP_REFL.
eauto.
eauto.
eauto.
econstructor 2; eauto.
eauto.
}

program_simpl.
eauto.
}

(* TmSb *)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}

-{

directContextConversion WFIH Psi sh1.

assert (exists hhh, Delta << hhh >> |--) as HX2.
{
program_simpl.
eapply SbContextValidity; eauto.
}

program_simpl.
eauto.
}
(* CONV *)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(* ABSURD *)

-{
directContextConversion WFIH Psi sh. 
program_simpl; eauto.
}

}
(*************************** WtSb *)
+{
clear_inversion H1. 

(* Sid*)
-{


assert (exists dhh, Psi << dhh >> |-- [Sid] : Psi) as HX1.
{
do 1 eexists.
repeat split.
eauto.
}

edestruct PreCxtConv_WfCxtEq_rev; eauto.
}

(* Sup *)

-{
clear_inversion H.

assert (exists dhh, Psi0,,B << dhh >> |-- [Sup] : Psi0) as HX1.
{
do 1 eexists.
repeat split.
eauto.
}

edestruct PreCxtConv_WfCxtEq_rev; eauto.

}
(* SSEQ *)
-{
edestruct WFIH with (m := tree_height sh1).
measure_solver.
eauto.
eauto.
program_simpl.

specialize (H6 _ _ _ H3 H2).
program_simpl. 
rename H6 into ssh1.
rename H10 into ssh2.

edestruct WFIH with (m := tree_height sh2).
measure_solver.
eauto.
eauto.

program_simpl.

assert (exists hhh, Delta << hhh >> |--) as HH.
{
eapply SbContextValidity.
eauto.
}
program_simpl.
 
eauto.
}

(* SEXT *)
-{ 
edestruct WFIH with (m := tree_height sh1).
measure_solver.
eauto.
eauto.
program_simpl.
specialize (H7 _ _ _ H3 H2).
program_simpl.

rename H7 into ssh1.
rename H11 into ssh2.

assert (exists hhh, Delta0 << hhh >> |--) as HH.
{
eapply SbContextValidity.
eauto.
}

program_simpl.
eauto.


directContextConversion WFIH Psi sh3.

program_simpl.
do 1 eexists.
repeat split.
eapply SEXT; eauto.
}

(* SB_CONV *)
-{
directContextConversion WFIH Psi sh1.
program_simpl.
eauto.
}
}
(******************************************* WfTpEq *)

+{
clear_inversion H1.
(**)
-{
directContextConversion WFIH Psi sh. 
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(* from TmUniv *)
-{
directContextConversion WFIH Psi sh.
decompose record HPCC0.
eauto.
}
(* TmFun *)
-{
assert (exists shh1, Psi << shh1 >> |-- A === A' ) as HX1.
{
edestruct WFIH with (m := tree_height sh1).
measure_solver.
eauto.
eauto.
program_simpl.
}

eauto.
program_simpl.

assert (exists shh1, Gamma ,, A << shh1 >> |-- /\ tree_height shh1 <= tree_height sh2) as HX2.
{
edestruct ContextValidityN_WfTpEq with (Gamma := Gamma,,A).
eauto.
program_simpl.
eexists.
split.
eauto.
eauto.
}

program_simpl.
clear_inversion H1.

assert (exists hhh, Psi << hhh >> |-- A) as HX2.
{
edestruct WFIH with (m := tree_height sh3); eauto.

eapply lt_trans with (m := tree_height sh2).
simpl in H5.
unfold smax2 in H5.
unfold max2 in H5.
rewrite succ_max_distr in H5.
red.
eapply max_lub_r.
eapply H5.
measure_solver.
program_simpl.
}

program_simpl.

assert (exists ccc, PreCxtConv ccc (Gamma,,A) (Psi,,A)) as HX3.
{
edestruct PreCxtConv_WfCxtEq. eauto.
eexists.
econstructor 2; eauto.
}

program_simpl.

assert (exists shh2, Psi,,A << shh2 >> |-- B === B' ) as HX4.
{
edestruct WFIH with (Gamma := Gamma,,A) (m := tree_height sh2).
measure_solver.
eauto.
eauto.
program_simpl.
eauto.
}

program_simpl.
eauto.
}
(* Tm SB *)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
}
(********************************* WfSbEq *)
+{

clear_inversion H1.
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(* EQ_SB_CONV *)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
rename Delta0 into Delta.
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh3.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh3.
program_simpl; eauto.
}
(**)
-{  
clear_inversion H.

rename Gamma0 into Gamma.
rename Psi0 into Psi.

assert (exists hhhh, Psi,,B << hhhh >>|-- [Sid] === [Sext Sup TmVar] : Psi,,B) as HY1.
{
assert (exists hhh, Psi << hhh >> |-- A ) as HX1.
{
edestruct WFIH with (m := tree_height sh); eauto.
}
program_simpl.
eauto.
}

program_simpl.
edestruct PreCxtConv_WfCxtEq_rev; eauto.
}

(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
}
(* WtTmEq *)
+{
clear_inversion H1.

(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
decompose record HPCC0; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.



assert (exists shh1, Gamma ,, A << shh1 >> |-- /\ tree_height shh1 <= tree_height sh2) as HX2.
{
edestruct ContextValidityN_WtTmEq with (Gamma := Gamma,,A).
eauto.
program_simpl.
eexists.
split.
eauto.
eauto.
}

program_simpl.
clear_inversion H0.


assert (exists hhh, Psi << hhh >> |-- A) as HX2.
{
edestruct WFIH with (m := tree_height sh3); eauto.

eapply lt_trans with (m := tree_height sh2).
simpl in H1.
unfold smax2 in H1.
unfold max2 in H1.
rewrite succ_max_distr in H1.
red.
eapply max_lub_r.
eapply H1.
measure_solver.
program_simpl.
}

program_simpl.

assert (exists ccc, PreCxtConv ccc (Gamma,,A) (Psi,,A)) as HX3.
{
edestruct PreCxtConv_WfCxtEq. eauto.
eexists.
econstructor 2; eauto.
}

program_simpl.

assert (exists shh2, Psi,,A << shh2 >> |-- B === B' : TmUniv) as HX4.
{
edestruct WFIH with (Gamma := Gamma,,A) (m := tree_height sh2).
measure_solver.
eauto.
eauto.
program_simpl.
eauto.
}

program_simpl.
eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh2.

assert (exists hhh, Psi << hhh >> |-- A1 === A1) as HX2.
{
program_simpl; eauto.
}

assert (exists hhh, Gamma ,, A1 << hhh >> |-- /\ tree_height hhh <= tree_height sh1) as HX3.
{
eapply ContextValidityN_WtTmEq.
eauto.
 }

assert (exists hhh, Psi << hhh >> |-- A1 ) as HX4.
{
program_simpl. 
clear_inversion H0.
edestruct WFIH with (m := tree_height sh3).
(* measure *)
{
simpl in *.
unfold smax2 in *.
unfold max2 in *.
rewrite succ_max_distr in H1.
constructor 2.
eapply le_trans.
eapply le_max_l.
eapply le_trans.
rewrite max_comm.
eauto.
eapply max_lub_l.
eauto.
}

eauto.
eauto.
program_simpl.
}
 
assert (exists hhh, Psi,,A1 << hhh >> |-- M1 === M2 : B ) as HX5.
{
program_simpl.
edestruct WFIH with (Gamma := Gamma,,A1) (Psi := Psi,,A1) (m := tree_height sh1); eauto; try measure_solver; program_simpl.
clear_inversion H1.
edestruct PreCxtConv_WfCxtEq; eauto.
econstructor 2; eauto.
eauto.
}

program_simpl.
eauto.
}
 
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh2.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1. 
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh. 
program_simpl; eauto.
}

-{
directContextConversion WFIH Psi sh2. 

assert (exists hhh, Gamma ,, A << hhh >> |-- /\ tree_height hhh <= tree_height sh1) as HX3.
{
eapply ContextValidityN_WtTm.
eauto.
}

assert (exists hhh, Psi << hhh >> |-- A ) as HX4.
{
program_simpl. 
clear_inversion H0.
edestruct WFIH with (m := tree_height sh3).
{
(* measure *)
simpl in *.
unfold smax2 in *.
unfold max2 in *.
rewrite succ_max_distr in H1.
constructor 2.
eapply le_trans.
eapply le_max_l.
eapply le_trans.
rewrite max_comm.
eauto.
eapply max_lub_l.
eauto.
}
eauto.
eauto.
program_simpl.
}

assert (exists hhh, Psi,,A << hhh >> |-- M : B ) as HX5.
{
program_simpl.
edestruct WFIH with (Gamma := Gamma,,A) (Psi := Psi,,A) (m := tree_height sh1); eauto; try measure_solver; program_simpl.
clear_inversion H1.
edestruct PreCxtConv_WfCxtEq; eauto.
econstructor 2; eauto.
eauto.
}

program_simpl.
eauto.
} 
(**)
-{
directContextConversion WFIH Psi sh1.
directContextConversion WFIH Psi sh3.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh1.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
(**)
-{
directContextConversion WFIH Psi sh.
program_simpl; eauto.
}
}
Qed.


(****************)
