(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Inversion lemmas for substitutions
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.Inversion.
Require Import Nbe.Syntax.Validity.
Require Import Nbe.Syntax.ContextConversion.
Require Import Nbe.Utils.

(******************************************************************************
 *)

Lemma Inversion_SBTP: forall Gamma t sb T,
  Gamma |-- TmSb t sb : T ->
    exists T', Gamma |-- T === TmSb T' sb
.
Proof.
intros.
remember (TmSb t sb).
generalize dependent t.
generalize dependent sb.
induction H; intros; try subst; try solve [ clear_inversion Heqt0 ].
(**)
{
clear_inversion Heqt0; exists A; eauto.
}
(**)
{
edestruct IHWtTm as [T'].
eauto.
exists T'.
eauto.
}
Qed.


(******************************************************************************
 *)

Lemma Inversion_SB: forall Gamma t sb T,
  Gamma |-- TmSb t sb : T ->
  exists  Delta C,
    Gamma |-- [sb] : Delta /\ Delta |-- t : C /\ Gamma |-- T === TmSb C sb
.
Proof.

intros.
remember (TmSb t sb) as X.
generalize dependent t.
generalize dependent sb.


induction H; intros; subst;
 try solve [clear_inversion HeqX].
{
clear_inversion HeqX.

exists Delta; exists A.
repeat (split; auto).
eauto.
}
(**)
{
edestruct IHWtTm as [Delta [C [HI1 [HI2 HI3]]]].
eauto.
exists Delta. exists C.
repeat (split; auto).
eauto.
}
Qed.

(******************************************************************************
 *)

Lemma CannotDeriveTermUniv: forall Gamma A,
  ~ Gamma |-- TmUniv : A
.
Proof.
intros.
intro.
remember TmUniv.
induction H; clear_inversion Heqt.
eauto.
Qed.


(******************************************************************************
 *)
Lemma Inversion_SB_SID_cxteq: forall Gamma Delta,
  Gamma |-- [Sid] : Delta ->
  |-- {Gamma} === {Delta}.
Proof.
intros.
remember Sid.
generalize dependent Heqs.
induction H; intros; try solve [clear_inversion Heqs].
eauto.
eauto.
Qed.


(******************************************************************************
 *)
Lemma Inversion_TPUNIV_Sid: forall Gamma T,
  Gamma |-- TmSb T Sid : TmUniv ->
  Gamma |-- T : TmUniv
.
Proof.

cut (forall Gamma T C, Gamma |-- TmSb T Sid : C -> Gamma |-- TmUniv === C -> Gamma |-- T : C).
{
intros; eapply H; eauto.
}

intros.
remember (TmSb T Sid) as t.
generalize dependent T.
induction H; intros; try solve [clear_inversion Heqt]; subst.

+{
clear_inversion Heqt; subst.


assert (|-- {Gamma} === {Delta}) as HX1.
{
eapply Inversion_SB_SID_cxteq.
eauto.
}

eapply ContextConversion_tm; eauto.
}

+{
eauto.
}
Qed.

(******************************************************************************
 *)
Lemma Inversion_TP_Sid: forall Gamma T,
  Gamma |-- TmSb T Sid ->
  Gamma |-- T.
Proof.
intros.

clear_inversion H.
+{
eapply UNIV_E.
eapply Inversion_TPUNIV_Sid.
eauto.
}

+{
eapply ContextConversion_tp;
eauto.
eapply EQ_CXT_SYM.
eapply Inversion_SB_SID_cxteq.
eauto.
}

Qed.

(******************************************************************************
 *)
Lemma Inversion_SB_SUP_cxteq: forall Gamma Delta,
  Gamma |-- [Sup] : Delta ->
  exists A, |-- {Gamma} === {Delta,,A} /\ Delta |-- A
.
Proof.
intros.
remember Sup.
generalize dependent Heqs.
induction H; intros; try solve [clear_inversion Heqs].
eexists.
eauto.
subst; eauto.
specialize (IHWtSb eq_refl).
program_simpl.
eexists IHWtSb.
split.
eapply EQ_CXT_TRANS.
eauto.
econstructor 2.
eauto.
eauto.
eapply ContextConversion_tp; eauto.
eauto.
eapply ContextConversion_tp; eauto.
Qed.

(******************************************************************************
 *)
Lemma EQ_CXT_EXT_inv1: forall Gamma A Delta B,
  |-- {Gamma,,A} === {Delta,,B} ->
  |-- {Gamma} === {Delta}
.
Proof.
intros.
clear_inversion H.
clear_inversion H2.
eauto.
eauto.
Qed.
Hint Resolve EQ_CXT_EXT_inv1.

(******************************************************************************
 *)
Module Helpers.

Ltac assert_different_hypothesis H1 H2 :=
match H1 with
  | H2 =>
    fail 1

  | _ =>
    idtac
  end
.

End Helpers.

(******************************************************************************
 *)

Module Lemmas.

Fact Inversion_SB_SEXT: forall Gamma A Delta SB M,
  Gamma |-- [Sext SB M ] : Delta,,A ->
  Gamma |-- [SB] : Delta /\
  Gamma |-- M : TmSb A SB /\
  Delta |-- A
.
Proof.
intros.
remember (Sext SB M) as s.
remember (Delta,,A) as G.
generalize dependent SB.
generalize dependent A.
generalize dependent Delta.
generalize dependent M.
induction H; intros; try solve [clear_inversion Heqs].
clear_inversion Heqs; subst.
clear_inversion HeqG; subst.
eauto.

subst.
clear_inversion H0.
eauto.

specialize (IHWtSb M Gamma0 A0 eq_refl SB eq_refl).
program_simpl.
repeat split; eauto.
Qed.

(******************************************************************************
 *)

Fact Inversion_SB_SEXT_cxteq: forall Gamma Psi SB M,
  Gamma |-- [Sext SB M ] : Psi ->
    exists Delta A,
      |-- {Psi} === {Delta,,A} /\
  Gamma |-- [SB] : Delta /\
  Gamma |-- M : TmSb A SB /\
  Delta |-- A
.
Proof.
intros.
remember (Sext SB M) as s.
generalize dependent SB.
generalize dependent M.
induction H; intros; try solve [clear_inversion Heqs].
clear_inversion Heqs; subst.
exists Delta. exists A.
split. eauto.
repeat split; eauto.

subst.

specialize (IHWtSb M SB eq_refl).
program_simpl.
do 2 eexists.
repeat split; eauto.
Qed.


(******************************************************************************
 *)

Lemma Inversion_SB_SSEQ: forall Gamma Delta sb1 sb2, 
  Gamma |-- [Sseq sb1 sb2] : Delta ->
  exists Psi,
    Gamma |-- [sb2] : Psi /\
    Psi |-- [sb1] : Delta
.
Proof.
intros.
remember (Sseq sb1 sb2).
generalize dependent sb1.
generalize dependent sb2.
induction H; intros; try solve [clear_inversion Heqs]; subst.
clear_inversion Heqs; subst.
eauto.

(**)
specialize (IHWtSb _ _ eq_refl).
program_simpl.
eauto.
Qed.

(******************************************************************************
 *)
Lemma Inversion_SB_SID_cxteq: forall Gamma Delta,
  Gamma |-- [Sid] : Delta ->
  |-- {Gamma} === {Delta}.
Proof.
intros.
remember Sid.
generalize dependent Heqs.
induction H; intros; try solve [clear_inversion Heqs].
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Lemma Inversion_SB_SUP_cxteq: forall Gamma Delta,
  Gamma |-- [Sup] : Delta ->
  exists A, |-- {Gamma} === {Delta,,A} /\ Delta |-- A
.
Proof.
intros.
remember Sup.
generalize dependent Heqs.
induction H; intros; try solve [clear_inversion Heqs].
eexists.
eauto.
subst; eauto.
specialize (IHWtSb eq_refl).
program_simpl.
eexists IHWtSb.
split.
eapply EQ_CXT_TRANS.
eauto.
econstructor 2.
eauto.
eauto.
eapply ContextConversion_tp; eauto.
eauto.
eapply ContextConversion_tp; eauto.
Qed.

End Lemmas.


(******************************************************************************
 *)
Module Identity.

Lemma wfcxt_from_id: forall Gamma Delta,
  Gamma |-- [Sid] : Delta ->
  Delta |--
.
Proof.
intros.
eauto.
Qed.

Ltac sbid_inversion HSID :=
  let H1 := fresh "H" in
  let H2 := fresh "HCXTEQ" in
  set (H1 := wfcxt_from_id HSID);
  set (H2 := Lemmas.Inversion_SB_SID_cxteq HSID);
  specialize H1;
  specialize H2
.

End Identity.


(******************************************************************************
 *)
Module Sequence.

Ltac sbseq_inversion HSSEQ := 
  let H3 := fresh "H" in
  let H4 := fresh "H" in
  let H5 := fresh "H" in
  let H6 := fresh "H" in
  let H1 := fresh "H" in
  let H2 := fresh "H" in
  let Gamma__1 := fresh "Gamma__1" in
  destruct (Lemmas.Inversion_SB_SSEQ HSSEQ) as [Gamma__1 [H1 H2]]
.

End Sequence.

(******************************************************************************
 *)

Module Extension.


Ltac sbext_inversion1 HSEXT := 
  let H1 := fresh "H" in
  let H2 := fresh "H" in
  let H3 := fresh "H" in
  destruct (Lemmas.Inversion_SB_SEXT HSEXT) as [H1 [H2 H3]]
.


Ltac sbext_inversion2 HSEXT :=
  let H1 := fresh "H" in
  let H2 := fresh "H" in
  let H3 := fresh "H" in
  let H4 := fresh "H" in
  let A := fresh "A" in
  let Delta := fresh "Delta" in
  destruct (Lemmas.Inversion_SB_SEXT_cxteq HSEXT) as [Delta [A [H1 [H2 [H3 H4]]]]]
.



Ltac sbext_inversion HSEXT :=
  first
    [ sbext_inversion1 HSEXT
    | sbext_inversion2 HSEXT
    ].

End Extension.

(******************************************************************************
 *)

Module VariableLift.

Ltac sbup_inversion HSUP :=
  let A := fresh "A" in
  let H1 := fresh "HCXTEQ" in
  let H2 := fresh "H" in
  destruct (Lemmas.Inversion_SB_SUP_cxteq HSUP) as [A [H1 H2]]
.

End VariableLift.

(******************************************************************************
 *)

Ltac inversion_on_sb HSB :=
  first
    [ Sequence.sbseq_inversion HSB
    | Extension.sbext_inversion HSB
    | VariableLift.sbup_inversion HSB
    | Identity.sbid_inversion HSB
    ]
.

(******************************************************************************
 *)
Ltac clear_inversion_on_sb HSB :=
  inversion_on_sb HSB;
  try (clear HSB)
.

