(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Context conversion theorem
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_equiv.
Require Import Nbe.Syntax.Inversion.
Require Import Nbe.Syntax.SystemN_ContextConversion.
Require Import Nbe.Utils.


(******************************************************************************
 *)

Theorem ContextConversion_tp: forall Gamma Delta T,
  Gamma |-- T ->
  |-- {Gamma} === {Delta} ->
  Delta |-- T.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- T) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- T).
eapply SystemN_ContextConversion.ContextConversion_tp; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.

(******************************************************************************
 *)
Theorem ContextConversion_tpeq: forall Gamma Delta T1 T2,
  Gamma |-- T1 === T2 ->
  |-- {Gamma} === {Delta} ->
  Delta |-- T1 === T2.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- T1 === T2) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- T1 === T2).
eapply SystemN_ContextConversion.ContextConversion_tpeq; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.


(******************************************************************************
 *)
Theorem ContextConversion_tm: forall Gamma Delta t T,
  Gamma |-- t : T ->
  |-- {Gamma} === {Delta} ->
  Delta |-- t : T.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- t : T) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- t : T).
eapply SystemN_ContextConversion.ContextConversion_tm; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.

(******************************************************************************
 *)
Theorem ContextConversion_tmeq: forall Gamma Delta t1 t2 T,
  Gamma |-- t1 === t2 : T ->
  |-- {Gamma} === {Delta} ->
  Delta |-- t1 === t2 : T.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- t1 === t2 : T) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- t1 === t2 : T).
eapply SystemN_ContextConversion.ContextConversion_tmeq; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.

(******************************************************************************
 *)
Theorem ContextConversion_sb: forall Gamma Delta sb Psi,
  Gamma |-- [sb] : Psi ->
  |-- {Gamma} === {Delta} ->
  Delta |-- [sb] : Psi.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- [sb] : Psi) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- [sb] : Psi).
eapply SystemN_ContextConversion.ContextConversion_sb; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.


(******************************************************************************
 *)
Theorem ContextConversion_sbeq: forall Gamma Delta sb1 sb2 Psi,
  Gamma |-- [sb1] === [sb2] : Psi ->
  |-- {Gamma} === {Delta} ->
  Delta |-- [sb1] === [sb2] : Psi.
Proof.
intros.

assert (exists ssh, Gamma << ssh >> |-- [sb1] === [sb2] : Psi) as HX1 by (eapply System_to_SystemN; eauto).
assert (exists ssh, << ssh >> |-- {Gamma} === {Delta}) as HX2 by (eapply System_to_SystemN; eauto).

program_simpl.

assert (exists n, Delta << n >> |-- [sb1] === [sb2] : Psi).
eapply SystemN_ContextConversion.ContextConversion_sbeq; eauto.

program_simpl.
eapply SystemN_to_System.
eauto.
Qed.


(******************************************************************************
 *)
Theorem ContextConversion_sigsb: forall Gamma Delta sb Psi,
  Psi |-- [sb] : Gamma ->
  |-- {Gamma} === {Delta} ->
  Psi |-- [sb] : Delta.
Proof.
intros.
eauto.
Qed.

(******************************************************************************
 *)
Theorem ContextConversion_sigsbeq: forall Gamma Delta sb1 sb2 Psi,
  Psi |-- [sb1] === [sb2] : Gamma ->
  |-- {Gamma} === {Delta} ->
  Psi |-- [sb1] === [sb2] : Delta.
Proof.
intros.
eauto.
Qed.


(******************************************************************************
 *)
Lemma EQ_CXT_SYM: forall Gamma Delta,
  |-- {Gamma} === {Delta} ->
  |-- {Delta} === {Gamma}.
Proof.
intros.
induction H.
eauto.
econstructor 2.
eauto.
eauto.
eauto.

eapply ContextConversion_tpeq.
eauto.
eauto.
Qed. 
Hint Resolve EQ_CXT_SYM.

(******************************************************************************
 *)
Fact EQ_CXT_TRANS: forall Gamma Delta Psi,
  |-- {Gamma} === {Delta} ->
  |-- {Delta} === {Psi} ->
  |-- {Gamma} === {Psi}
.
Proof.
intros.

generalize dependent Psi.
induction H; intros; eauto.
clear_inversion H3; eauto.

econstructor 2.
eauto.
eauto.
eauto.

eapply System.EQ_TP_TRANS.
eauto.

eapply ContextConversion_tpeq.
eauto.
eauto.
Qed.
Hint Resolve EQ_CXT_TRANS.


(******************************************************************************
 *)

Ltac change_hypothesis Hnew Hold :=
  let Htmp := fresh "H" in
  set (Htmp := Hnew);
  specialize Htmp;
  try (clear Hold; rename Htmp into Hold)
.

Ltac convert_context_ HCXTEQ Gamma Delta H :=
match type of H with
| Gamma |-- ?T =>
  change_hypothesis (ContextConversion_tp H HCXTEQ) H

| Gamma |-- ?t : ?T =>
  change_hypothesis (ContextConversion_tm H HCXTEQ) H

| Gamma |-- ?T1 === ?T2 =>
  change_hypothesis (ContextConversion_tpeq H HCXTEQ) H

| Gamma |-- ?t1 === ?t2 : ?T1 =>
  change_hypothesis (ContextConversion_tmeq H HCXTEQ) H

| Gamma |-- [?sb] : ?Psi =>
  change_hypothesis (ContextConversion_sb H HCXTEQ) H

| Gamma |-- [?sb1] === [?sb2] : ?Psi =>
  change_hypothesis (ContextConversion_sbeq H HCXTEQ) H

| ?Psi |-- [?sb] : Gamma =>
  change_hypothesis (ContextConversion_sigsb H HCXTEQ) H

| ?Psi |-- [?sb1] === [?sb2] : Gamma =>
  change_hypothesis (ContextConversion_sigsbeq H HCXTEQ) H
end.

Ltac convert_context HCXTEQ H :=
match type of HCXTEQ with
| |-- {?Gamma} === {?Delta} =>
  convert_context_ HCXTEQ Gamma Delta H 
end.

Ltac convert_all_contexts HCXTEQ :=
repeat match goal with
| [ H : ?T |- _ ] =>
   convert_context HCXTEQ H
end
.

Ltac convert_context_bi HCXTEQ H :=
  first
    [ convert_context HCXTEQ H
    | convert_context (EQ_CXT_SYM HCXTEQ) H
    ]
.

