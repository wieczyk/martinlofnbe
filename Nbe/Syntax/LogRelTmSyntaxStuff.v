(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Helpers
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Utils.

(******************************************************************************
 *)

Lemma TmVar_Sups_tmVar: forall i Delta  Gamma A,
  CxtShift Delta i (Gamma,,A) ->
  Delta |-- TmSb TmVar (Sups i) === tmVar i : TmSb (TmSb A Sup) (Sups i)
.
Proof.
induction i; intros.
red in H.
clear_inversion_on_sb H.
simpl.
clear_inversion H0.
eapply ContextConversion_tmeq with (Gamma := Gamma,,A); [ | eauto ].
apply EQ_SBID.
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eauto.

(**)
red in H; simpl in H.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.
rename A0 into B.
simpl.
eapply ContextConversion_tmeq with (Gamma := Gamma__1,,B); [ | eauto ].

specialize (IHi Gamma__1 Gamma A H5).
eapply EQ_CONV.
 
eapply EQ_TRANS.
eapply EQ_SYM.
eapply EQ_SBSEQ.

eauto.
eauto.
eauto.
eapply EQ_CONG_SB.
eauto.
apply IHi.

eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Lemma TmVar_Sups_tmVar': forall i Delta  Gamma A,
  CxtShift Delta i (Gamma,,A) ->
  Delta |-- TmSb TmVar (Sups i) === tmVar i : TmSb (TmSb A (Sups 1)) (Sups i)
.
Proof.
intros.
simpl in *.

assert (Gamma,,A |--).
eauto.
clear_inversion H0.

eapply EQ_CONV.
eapply TmVar_Sups_tmVar.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SIDL.
apply SUP.
eauto.
eauto.
Qed.

(******************************************************************************
 *)

Lemma F_tp_subst: forall Psi F j i a Gamma A Delta,
  Psi |--  [Sext (Sups (j + i)) a]:(Delta,,A) ->
  Delta,,A |-- F ->
  CxtShift Gamma i Delta ->
  CxtShift Psi j Gamma ->
  Psi |-- TmSb F (Sext (Sups (j + i)) a) ->
  Psi |-- TmSb F (Sext (Sups (j + i)) a) === TmSb (TmSb F (Sext (Sseq (Sups i) Sup) TmVar)) (Sext (Sups j) a)
.
Proof.

intros.
 
clear_inversion_on_sb H.

assert (Psi |-- [Sups (j + i)] === [Sseq (Sups i) (Sups j)] : Delta) as HS1.
{
apply Sups_tails; auto.
}

assert (Psi |-- [Sseq (Sups i) (Sups j)] : Delta) as HS2.
{
eauto.
}

assert (Psi |-- TmSb A (Sups (j + i))) as HS3.
{
eauto.
}

assert (Psi |-- TmSb A (Sups (j + i)) === TmSb (TmSb A (Sups i)) (Sups j)) as HS4.
{
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
}

assert (Psi |-- a : TmSb (TmSb A (Sups i)) (Sups j)) as HS5.
{
eapply CONV.
eauto.
eauto. 
}

clear_inversion_on_sb HS2.

assert ( Gamma__1,, TmSb A (Sups i) |--  [Sext (Sseq (Sups i) Sup) TmVar] : Delta,, A) as HS6.
{
apply SEXT; auto.
eauto.
eapply CONV.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
}


assert ( Psi |--  [Sext (Sups j) a] : Gamma__1,, TmSb A (Sups i) ) as HS7.
{
eapply SEXT; eauto.
}

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.

eapply EQ_TP_CONG_SB; [ | eauto ].

(*
clear H4.
clear H1.
clear Delta.
rename Delta0 into Delta.
*)

assert (Gamma__1 |-- TmSb A (Sups i)) as HXX2.
{
eapply SB_F.
eauto.
eauto.
}

(***)
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT; eauto.

eapply EQ_SB_CONG_SEXT; auto.

eapply EQ_SB_TRANS.
eapply EQ_SB_COMM. eauto. eauto. eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP; eauto.
eapply EQ_SB_REFL; eauto.

eauto.
(**)
eapply EQ_CONV.
eapply EQ_SBVAR; eauto.

(**)

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB; [ |  eapply EQ_TP_REFL; eauto ].
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM; eauto.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP; eauto.
eapply EQ_SB_REFL; eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.



(*
 *   TmSb F (Sext (Sups (j + i)) a)
 *
 *   by Sups_tails
 * = TmSb F (Sext (Sseq (Sups j) (Sups i)) a)  
 *
 *   TmSb (TmSb F (Sext (Sseq (Sups i) Sup) TmVar)) (Sext (Sups j) a)
 *
 *   by Sups def
 * = TmSb (TmSb F (Sext (Sups (S i)) TmVar)) (Sext (Sups j) a)
 *
 *   by EQ_TP_SBSEQ
 * = TmSb F (Sseq (Sext (Sups (S i)) TmVar)) (Sext (Sups j) a)
 *
 *   by EQ_TP_CONG_SB and EQ_SB_SEXT
 * = TmSb F (Sext (Sseq (Sups (S i) (Sext (Sups j) a))) (TmSb TmVar (Sext (Sups j) a)))
 *
 * = TmSb F (Sext (Sseq (Sups i) (Sups j)) a)
 *
 * = TmSb F (Sext (Sups (i + j)) a)
 *
 * = TmSb F (Sext (Sups (j + i)) a)
 *)
Qed.


(******************************************************************************
 *)
Lemma Inversion_FUN_E_ : forall Gamma N M T,
  Gamma |-- TmApp M N : T ->
  exists A F, Gamma |-- M : TmFun A F /\ Gamma |-- T === TmSb F (Ssing N) /\ Gamma |-- N : A
.
Proof.
intros.

remember (TmApp M N) as X1.
generalize dependent M.
generalize dependent N.

induction H; intros; try solve [ clear_inversion HeqX1 ].
+{
clear_inversion HeqX1.

exists A.
exists B.
split; auto.
eauto.
}
(**)
+{
subst.
destruct (IHWtTm _ _ eq_refl) as [A' [F' [HH1 [HH2 HH3]]]].
exists A'. exists F'.
split; auto.
eauto.
}
Qed.

(******************************************************************************
 *)
Lemma F_subst: forall Psi t F j i a,
  Psi |-- TmApp (TmSb t (Sups (j + i))) a : TmSb F (Sext (Sups (j + i)) a) ->

  Psi |-- TmApp (TmSb t (Sups (j + i))) a ===
    TmApp (TmSb (TmSb t (Sups i)) (Sups j)) a : TmSb F (Sext (Sups (j + i)) a)
.
Proof.

intros.

edestruct Inversion_FUN_E_ as [A' [F' [HH1 [HH2 HH3]]]].
eauto.

eapply EQ_CONV.
eapply EQ_CONG_FUN_E.
rewrite plus_comm.
eapply nc_Sups_plus_eq.
rewrite plus_comm.
eauto.
eauto.

apply EQ_TP_SYM.
auto.
Qed.

(******************************************************************************
 * Additional definitions
 *)

Fact CxtShift_type: forall Delta i Gamma T,
  CxtShift Delta i Gamma ->
  Gamma |-- T ->
  Delta |-- TmSb T (Sups i)
.
Proof.
intros.
red in H.
eauto.
Qed.

(******************************************************************************
 *)
Lemma WtSb_SIDR: forall Gamma sb Delta,
  Gamma |-- [Sseq sb Sid] : Delta ->
  Gamma |-- [sb] : Delta
.
Proof.
intros.
remember (Sseq sb Sid).
generalize dependent sb.
induction H; intros;
  try subst; try solve [clear_inversion Heqs].
clear_inversion Heqs.
clear_inversion_on_sb H.
eapply ContextConversion_sb. eauto.
eauto.

eauto.
Qed.

(******************************************************************************
 *)
Lemma WtSb_SUPR: forall Gamma sb Delta,
  Gamma |-- [Sseq Sup sb] : Delta ->
    exists A,  Gamma |-- [sb] : Delta,,A
.
Proof.
intros.
clear_inversion_on_sb H.
clear_inversion_on_sb H5.
eauto.
Qed.

(******************************************************************************
 *)
Lemma WtSb_SUPL: forall Gamma sb Delta,
  Gamma |-- [Sseq sb Sup] : Delta ->
    exists Psi A, |-- {Gamma} === {Psi,,A} /\ Psi |-- [sb] : Delta
.
Proof.
intros.
clear_inversion_on_sb H.
clear_inversion_on_sb H4.
eauto.
Qed.

(******************************************************************************
 *)
Lemma WtSb_SIDL: forall Gamma sb Delta,
  Gamma |-- [Sseq Sid sb] : Delta ->
  Gamma |-- [sb] : Delta
.
Proof.
intros.
remember (Sseq Sid sb).
generalize dependent sb.
induction H; intros;
  try subst; try solve [clear_inversion Heqs].
clear_inversion Heqs.
clear_inversion_on_sb H0.
eapply ContextConversion_sb.
eauto.
eauto.

eauto.
Qed.


(******************************************************************************
 *)
Lemma EQ_TP_SB_ARR: forall Gamma Delta A B sb,
  Gamma |-- [sb] : Delta ->
  Delta |-- A ->
  Delta |-- B ->
  Gamma |-- TmSb (TmArr A B) sb === TmArr (TmSb A sb) (TmSb B sb)
.
Proof.
intros.
unfold TmArr.
eapply EQ_TP_TRANS.
 
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_FUN.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT.  
eauto.
eapply H0.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply SB_F.
eauto.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SUP.
eauto.
eapply H0.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
eauto.

Qed.


