(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Utils.TreeShape.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_equiv.
Require Import Nbe.Syntax.SystemN_SimplePreliminaryContextConversion.

(******************************************************************************
 *)

Theorem SimplePreliminaryContextConversion_tp: forall Gamma A B T, 
  Gamma  |-- ->
  Gamma |-- A ->
  Gamma |-- B ->
  Gamma |-- A === B ->
  Gamma ,, A  |-- T ->
  Gamma ,, B   |-- T.
Proof.
intros.

assert( exists sh,  Gamma << sh >>  |-- ).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- A).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- B).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >>  |-- A === B). 
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma,,A << sh >> |-- T).
eapply System_to_SystemN; eauto.

program_simpl.

edestruct SystemN_SimplePreliminaryContextConversion.SimplePreliminaryContextConversion_tp with (A := A) (B := B) (T := T); eauto.
eapply SystemN_to_System.
eauto.
Qed.

(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tpeq: forall  Gamma A B T1 T2, 
  Gamma  |-- ->
  Gamma  |-- A ->
  Gamma  |-- B ->
  Gamma  |-- A === B ->
  Gamma ,, A  |-- T1 === T2 ->
  Gamma ,, B |-- T1 === T2.
Proof.
intros.

assert( exists sh,  Gamma << sh >>  |-- ).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- A).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- B).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >>  |-- A === B). 
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma,,A << sh >> |-- T1 === T2).
eapply System_to_SystemN; eauto.

program_simpl.

edestruct SystemN_SimplePreliminaryContextConversion.SimplePreliminaryContextConversion_tpeq with (A := A) (B := B) (T1 := T1); eauto.
eapply SystemN_to_System.
eauto.
Qed.


(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tm: forall Gamma A B t T, 
  Gamma  |-- ->
  Gamma  |-- A ->
  Gamma  |-- B ->
  Gamma  |-- A === B ->
  Gamma ,, A  |-- t : T ->
 Gamma ,, B  |-- t : T.
Proof.
intros.

assert( exists sh,  Gamma << sh >>  |-- ).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- A).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- B).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >>  |-- A === B). 
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma,,A << sh >> |-- t : T).
eapply System_to_SystemN; eauto.

program_simpl.

edestruct SystemN_SimplePreliminaryContextConversion.SimplePreliminaryContextConversion_tm with (A := A) (B := B) (T := T); eauto.
eapply SystemN_to_System.
eauto.
Qed.

(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tmeq: forall Gamma A B t1 t2 T, 
  Gamma  |-- ->
  Gamma |-- A ->
  Gamma  |-- B ->
  Gamma  |-- A === B ->
  Gamma ,, A  |-- t1 === t2 : T ->
 Gamma ,, B  |-- t1 === t2 : T.
Proof.
intros.

assert( exists sh,  Gamma << sh >>  |-- ).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- A).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >> |-- B).
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma << sh >>  |-- A === B). 
eapply System_to_SystemN; eauto.

assert (exists sh,  Gamma,,A << sh >> |-- t1 === t2 : T).
eapply System_to_SystemN; eauto.

program_simpl.

edestruct SystemN_SimplePreliminaryContextConversion.SimplePreliminaryContextConversion_tmeq with (A := A) (B := B) (T := T); eauto.
eapply SystemN_to_System.
eauto.
Qed.


