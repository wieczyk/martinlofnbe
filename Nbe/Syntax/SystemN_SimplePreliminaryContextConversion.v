(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Context conversion theorem for typing context that differ only on the last
 * position
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_wfind.
Require Import Nbe.Syntax.SystemN_PreliminaryContextConversion.
Require Import Nbe.Utils.TreeShape.
Require Import Utils.Tactics.
Require Import Nbe.Utils.ArithFuncs.
Require Import Arith.
Require Import Arith.Le.
Require Import Arith.Lt.
Require Import Arith.Max.

(******************************************************************************
 *)
Lemma PreCxtConv_refl: forall sh Gamma, Gamma << sh >>  |-- -> exists ssh, PreCxtConv ssh Gamma Gamma.
Proof.
intros.
induction H.
eexists Leaf.
constructor 1.

(**)
program_simpl.
eexists.
econstructor 2; eauto.
Qed.

(******************************************************************************
 *)
Lemma PreCxtConv_for1: forall sh1 sh2 sh3 sh4 Gamma A B,
  Gamma << sh1 >> |-- ->
  Gamma << sh2 >> |-- A === B ->
  Gamma << sh3 >> |-- A -> 
  Gamma << sh4 >> |-- B -> 
 exists ssh, PreCxtConv ssh (Gamma,,A) (Gamma,,B).
Proof.
intros.
edestruct PreCxtConv_refl; eauto.
eexists.
econstructor 2; eauto.
Qed.


(******************************************************************************
 *)

Theorem SimplePreliminaryContextConversion_tp: forall sh0 sh1 sh2 sh3 sh4 Gamma A B T, 
  Gamma << sh0 >> |-- ->
  Gamma << sh1 >> |-- A ->
  Gamma << sh2 >> |-- B ->
  Gamma << sh3 >> |-- A === B ->
  Gamma ,, A << sh4 >> |-- T ->
exists sssh,
 Gamma ,, B << sssh >> |-- T.
Proof.
intros.
edestruct PreCxtConv_for1; eauto.
edestruct PreliminaryContextConversion; eauto; program_simpl; eauto.
Qed.

(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tpeq: forall sh0 sh1 sh2 sh3 sh4 Gamma A B T1 T2, 
  Gamma << sh0 >> |-- ->
  Gamma << sh1 >> |-- A ->
  Gamma << sh2 >> |-- B ->
  Gamma << sh3 >> |-- A === B ->
  Gamma ,, A << sh4 >> |-- T1 === T2 ->
exists sssh,
 Gamma ,, B << sssh >> |-- T1 === T2.
Proof.
intros.
edestruct PreCxtConv_for1; eauto.
edestruct PreliminaryContextConversion; eauto; program_simpl; eauto.
Qed.


(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tm: forall sh0 sh1 sh2 sh3 sh4 Gamma A B t T, 
  Gamma << sh0 >> |-- ->
  Gamma << sh1 >> |-- A ->
  Gamma << sh2 >> |-- B ->
  Gamma << sh3 >> |-- A === B ->
  Gamma ,, A << sh4 >> |-- t : T ->
exists sssh,
 Gamma ,, B << sssh >> |-- t : T.
Proof.
intros.
edestruct PreCxtConv_for1; eauto.
edestruct PreliminaryContextConversion; eauto; program_simpl; eauto.
Qed.

(******************************************************************************
 *)
Theorem SimplePreliminaryContextConversion_tmeq: forall sh0 sh1 sh2 sh3 sh4 Gamma A B t1 t2 T, 
  Gamma << sh0 >> |-- ->
  Gamma << sh1 >> |-- A ->
  Gamma << sh2 >> |-- B ->
  Gamma << sh3 >> |-- A === B ->
  Gamma ,, A << sh4 >> |-- t1 === t2 : T ->
exists sssh,
 Gamma ,, B << sssh >> |-- t1 === t2 : T.
Proof.
intros.
edestruct PreCxtConv_for1; eauto.
edestruct PreliminaryContextConversion; eauto; program_simpl; eauto.
Qed.
