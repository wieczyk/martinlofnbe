
(******************************************************************************
 * Pawel Wieczorek
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.
Require Import Wf_nat.
Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Utils.TreeShape.
Require Import Utils.Tactics.

(*********************************************************************************)


Theorem ContextValidity: forall sh Gamma,
  (forall T, Gamma  << sh >> |-- T -> exists shh, Gamma << shh >> |--) /\
  (forall t T, Gamma  << sh >> |-- t : T -> exists shh, Gamma << shh >> |--) /\
  (forall sb Delta, Gamma  << sh >> |-- [sb] : Delta -> exists shh, Gamma << shh >> |--)
.
Proof.
treeshape_wfind tree_height.
repeat split; intros; eauto.
(************************************************ WfTp *)
+{
clear_inversion H0; eauto.
-{
edestruct WFIH with (m := tree_height sh0 ) (Gamma := Gamma).
eauto with arith2_db.
eauto.

decompose record H0.
eauto.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1 ) (Gamma := Gamma).
eauto with arith2_db.
eauto.
edestruct WFIH with (m := tree_height sh1) (Gamma := Gamma); program_simpl; eauto with arith2_db.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1).
eauto with arith measure_db.
eauto.
program_simpl.
}
}
(* ******************************* WtTm *)
+{

clear_inversion H0; eauto; try solve 
  [ edestruct WFIH with (m := tree_height sh1); program_simpl; eauto with arith2_db
  | edestruct WFIH with (m := tree_height sh0); program_simpl; eauto with arith2_db
 ]
 .

-{
edestruct WFIH with (m := tree_height sh1); try reflexivity.
eauto with arith2_db.
program_simpl.
}

}
(************************************WtSb*)
+{
clear_inversion H0; eauto; try solve 
  [ edestruct WFIH with (m := tree_height sh1); program_simpl; eauto 
  | edestruct WFIH with (m := tree_height sh0); program_simpl; eauto 
  ]
.
(**)
-{
assert (exists hhh, Delta << hhh >> |--) as HH.
{
edestruct WFIH with (m := tree_height sh0); program_simpl; eauto.
}
edestruct HH.
eauto.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1); program_simpl; eauto.
measure_solver.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1); program_simpl; eauto.
measure_solver.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1); program_simpl; eauto.
measure_solver.
}
}
Qed.

Lemma ContextValidity_WfTp: forall Gamma n T, Gamma  << n >> |-- T -> exists hh, Gamma << hh >> |--.
Proof.
intros.
edestruct ContextValidity.
eauto.
Qed.

Lemma ContextValidity_WtTm: forall Gamma n t T, Gamma  << n >> |-- t : T -> exists hh, Gamma << hh >> |--.
Proof.
intros.
edestruct ContextValidity; program_simpl.
Qed.

Lemma ContextValidity_WtSb: forall Gamma n sb Delta, Gamma << n >> |-- sb : Delta -> exists hh, Gamma << hh >> |--.
Proof.
intros.
edestruct ContextValidity; program_simpl.
Qed.

(*********************************************************************************)

Theorem EqContextValid: forall sh Gamma Delta,
  << sh >>|-- {Gamma} === {Delta} ->
   exists sh1, exists sh2, Gamma << sh1 >> |-- /\ Delta << sh2 >> |--
.
Proof.
intros.
clear_inversion H.
eauto.


assert (exists ssh1, Gamma0 << ssh1 >> |-- ) as HX1.
eapply ContextValidity_WfTp.
eauto.

assert (exists ssh2, Delta0 << ssh2 >> |-- ) as HX2.
eapply ContextValidity_WfTp.
eauto.

program_simpl.

eexists.
eexists.
split; eauto.
Qed.

Theorem EqContextValid1: forall sh Gamma Delta,
  << sh >>|-- {Gamma} === {Delta} ->
   exists sh1, Gamma << sh1 >> |--
.
Proof.
intros.
edestruct EqContextValid; eauto.
program_simpl; eauto.
Qed.

Theorem EqContextValid2: forall sh Gamma Delta,
  << sh >>|-- {Gamma} === {Delta} ->
   exists sh1, Delta << sh1 >> |--
.
Proof.
intros.
edestruct EqContextValid; eauto.
program_simpl; eauto.
Qed.

(*********************************************************************************)

Theorem SbContextValidity: forall sh Gamma,
  (forall sb Delta, Gamma  << sh >> |-- [sb] : Delta -> exists shh, Delta << shh >> |--)
.
Proof.
treeshape_wfind tree_height.
clear_inversion H0.
eauto.
eapply ContextValidity_WfTp.
eauto.

edestruct WFIH with (m := tree_height sh2).
measure_solver.
eauto.
eauto.
eauto.

edestruct WFIH with (m := tree_height sh1).
measure_solver.
eauto.
eauto.
eauto.


eapply EqContextValid2.
eauto.
Qed.

(*********************************************************************************)


Definition beta sh1 sh2 := tree_height sh1 <= tree_height sh2.
Require Import Lt.
Require Import Le.
Require Import NPeano.
Import Nat.
Require Import Arith.Max.


Theorem ContextValidityN: forall sh Gamma,
  (forall T, Gamma  << sh >> |-- T -> exists shh, Gamma << shh >> |-- /\ beta shh sh) /\
  (forall t T, Gamma  << sh >> |-- t : T -> exists shh, Gamma << shh >> |-- /\ beta shh sh) /\
  (forall sb Delta, Gamma  << sh >> |-- [sb] : Delta -> exists shh, Gamma << shh >> |-- /\ beta shh sh) /\
(*  (forall Gamma Delta, << sh >> |-- {Gamma} === {Delta} -> exists ssh, Gamma << ssh >> |-- /\ beta ssh  *)
  (forall T1 T2, Gamma  << sh >> |-- T1 === T2 -> exists shh, Gamma << shh >> |-- /\ beta shh sh) /\
  (forall sb1 sb2 Delta, Gamma  << sh >> |-- [sb1] === [sb2] : Delta -> exists shh, Gamma << shh >> |-- /\ beta shh sh) /\
  (forall t1 t2 T, Gamma  << sh >> |-- t1 === t2 : T -> exists shh, Gamma << shh >> |-- /\ beta shh sh)
.
Proof.

unfold beta.
treeshape_wfind tree_height.
repeat split; intros; eauto.
(* WfTp *)
+{
clear_inversion H0; eauto with arith.
-{
 
edestruct WFIH with (m := tree_height sh0) (Gamma := Gamma).
measure_solver.
eauto.
decompose record H0.
edestruct H2.
eauto.
program_simpl.
rename x into shh.

exists shh. 
split.
auto.
eapply le_trans.
eapply H8.
measure_solver.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1) (Gamma := Gamma).
measure_solver.
eauto.
edestruct H.
eauto.
program_simpl.
rename x into shh.

exists shh. 
split.
auto.
eapply le_trans.
eapply H4.
measure_solver.
}
(**)
-{
edestruct WFIH with (m := tree_height sh1) (Gamma := Gamma); program_simpl; eauto with arith2_db.
edestruct H3.
eauto.
program_simpl.
exists x.
split; auto.
eapply le_trans.
eapply H8.
measure_solver.
}
}
(***************** WtTm *)
+{

clear_inversion H0;
  try solve 
[
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tm; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma)  (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto;  try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto;  try measure_solver;
  edestruct IH_Tm; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]

|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split

].
}

(****************************** WtSb*)
+{
clear_inversion H0; eauto.

(**)
-{
edestruct WFIH with (Gamma := Delta) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto;  try measure_solver.
}
(**)
-{
edestruct WFIH with (Gamma := Delta) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto;  try measure_solver.
edestruct IH_Tp; eauto; program_simpl.
  eexists; split.
  eauto.

simpl.
eapply le_n_S.
eapply le_trans.
eapply ArithFuncs.max_lt_1.
eauto.
rewrite max_id.
eauto.
}
(**)
-{
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver.
edestruct IH_Sb; eauto; program_simpl.
eexists; split.
eauto.

eapply le_trans; eauto; measure_solver.
}
(**)
-{
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver.
edestruct IH_Sb; eauto; program_simpl.
eexists; split.
eauto.

eapply le_trans; eauto; measure_solver.
}
(**)
-{
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb IH_TpEq]]]; program_simpl; eauto; try measure_solver.
edestruct IH_Sb; eauto; program_simpl.
eexists; split.
eauto.

eapply le_trans; eauto; measure_solver.
}
}

(************************************** WfTpEq *)
+{
clear_inversion H0;
  try solve 
[
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tm; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma)  (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto;  try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]

|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_SbEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TmEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
].

}
(************************ WtSbEq *)
+{

clear_inversion H0;
  try solve 
[
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tm; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma)  (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto;  try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]

|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_SbEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
].
(**)

-{

edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_SbEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
.
}
(**)
-{
edestruct WFIH with (Gamma := Gamma0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver.
edestruct IH_Tp; eauto; program_simpl.
eexists; split.
eauto.
simpl.
eapply le_n_S.
eapply le_trans.
eapply ArithFuncs.max_lt_1.
eauto.
rewrite max_id.
eauto.
}
}
(************************************ WtTmEq *)
+{
clear_inversion H0;
  try solve 
[
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tm; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma)  (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
  edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto;  try measure_solver;
  edestruct IH_Tp; eauto; program_simpl;
  eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]

|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma0,,T0) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb  [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_SbEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Sb; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Tm; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh0) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TmEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh2) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TmEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
|
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh1) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TmEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
]
.

(**)
-{
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh2) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_TpEq; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
.
}

(**)
-{
edestruct WFIH with (Gamma := Gamma) (m := tree_height sh2) as [IH_Tp [IH_Tm [IH_Sb [IH_TpEq [IH_SbEq IH_TmEq]]]]]; program_simpl; eauto; try measure_solver;
edestruct IH_Tm; eauto; program_simpl;
eexists; split;
  [ eauto
  | eapply le_trans; eauto; measure_solver
  ]
.
}
}
Qed.

Lemma ContextValidityN_WfTp: forall sh Gamma T,
   Gamma  << sh >> |-- T -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; eauto.
Qed.

Lemma ContextValidityN_WfTpEq: forall sh Gamma T1 T2,
   Gamma  << sh >> |-- T1 === T2 -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; program_simpl; eauto.
Qed.


Lemma ContextValidityN_WtTm: forall sh Gamma t T, Gamma  << sh >> |-- t : T -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; program_simpl; eauto.
Qed.

Lemma ContextValidityN_WtTmEq: forall sh Gamma t1 t2 T, Gamma  << sh >> |-- t1 === t2 : T -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; program_simpl; eauto.
Qed.


Lemma ContextValidityN_WtSb: forall sh Gamma sb Delta, 
  Gamma  << sh >> |-- [sb] : Delta -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; program_simpl; eauto.
Qed.

Lemma ContextValidityN_WtSbEq: forall sh Gamma sb1 sb2 Delta, 
  Gamma  << sh >> |-- [sb1] === [sb2] : Delta -> exists shh, Gamma << shh >> |-- /\ beta shh sh.
Proof.
intros; edestruct ContextValidityN; program_simpl; eauto.
Qed.

(*********************************************************************************)


Lemma Inversion_WfCxtEq: forall Gamma Delta sh,
 << sh >> |-- {Gamma} === {Delta} ->
exists ssh, Delta << ssh >> |--
.
Proof.
intros.
induction H.
eauto.
program_simpl.
eauto.
Qed.
 

Theorem SbContextValidityEq: forall sh Gamma,
  (forall sb1 sb2 Delta, Gamma  << sh >> |-- [sb1] === [sb2] : Delta -> exists shh, Delta << shh >> |--)
.
Proof.
treeshape_wfind tree_height.
clear_inversion H0; try solve
[
eapply SbContextValidity;
eauto
]
.


edestruct WFIH with (m := tree_height sh0);
measure_solver; eauto.

edestruct WFIH with (m := tree_height sh1);
measure_solver; eauto.

edestruct Inversion_WfCxtEq.
eauto.
eauto.

edestruct WFIH with (m := tree_height sh2);
measure_solver; eauto.

edestruct WFIH with (m := tree_height sh1);
measure_solver; eauto.

assert (exists hhh, Gamma0 << hhh >> |-- ) as HX1.
eapply  ContextValidity_WfTp.
eauto.

program_simpl.
eauto.
Qed.

