(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Syntactical validity theorem for augmented system
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_equiv.
Require Import Nbe.Syntax.Validity.
Require Import Nbe.Utils.


(******************************************************************************
 *)

Lemma WfCxt_from_WfTp sh1 Gamma T (H : Gamma << sh1 >> |-- T) : exists ssh, Gamma << ssh >> |--.
Proof.
assert (Gamma |-- T) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfCxt_from_WfTp.
eauto.
Qed.
Hint Resolve WfCxt_from_WfTp.

(******************************************************************************
 *)
Lemma WfTp_from_WtTm sh1 Gamma t T (H : Gamma << sh1 >>  |-- t : T) : exists ssh, Gamma << ssh >>  |-- T.
Proof.
assert (Gamma |-- t : T) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfTp_from_WtTm.
eauto.
Qed.
Hint Resolve WfTp_from_WtTm.

(******************************************************************************
 *)
Lemma WfTp_from_WfTpEq1 sh Gamma T T' (H : Gamma << sh >>  |-- T === T') : exists ssh, Gamma << ssh >>  |-- T.
Proof.
assert (Gamma |-- T === T') as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfTp_from_WfTpEq1.
eauto.
Qed.
Hint Resolve WfTp_from_WfTpEq1.

(******************************************************************************
 *)
Lemma WfTp_from_WfTpEq2 sh Gamma T T' (H : Gamma << sh >>  |-- T === T') : exists ssh, Gamma << ssh >>  |-- T'.
Proof.
assert (Gamma |-- T === T') as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfTp_from_WfTpEq2.
eauto.
Qed.
Hint Resolve WfTp_from_WfTpEq2.

(******************************************************************************
 *)
Lemma WtTm_from_WtTmEq1 Gamma sh t t' T (H : Gamma << sh >>  |-- t === t' : T) : exists ssh, Gamma << ssh >>  |-- t : T.
Proof.
assert (Gamma |-- t === t' : T) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WtTm_from_WtTmEq1.
eauto.
Qed.
Hint Resolve WtTm_from_WtTmEq1.

(******************************************************************************
 *)
Lemma WtTm_from_WtTmEq2 Gamma sh t t' T (H : Gamma << sh >>  |-- t === t' : T) : exists ssh, Gamma << ssh >>  |-- t' : T.
Proof.
assert (Gamma |-- t === t' : T) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WtTm_from_WtTmEq2.
eauto.
Qed.
Hint Resolve WtTm_from_WtTmEq2.

(******************************************************************************
 *)
Lemma WtSb_from_WtSbEq1 Gamma sh s s' Delta (H : Gamma << sh >> |-- [s] === [s'] : Delta) : exists ssh, Gamma << ssh >>  |-- [s] : Delta.
Proof.
assert (Gamma |-- [s] === [s'] : Delta) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WtSb_from_WtSbEq1.
eauto.
Qed.
Hint Resolve WtSb_from_WtSbEq1.

(******************************************************************************
 *)
Lemma WtSb_from_WtSbEq2 Gamma sh s s' Delta (H : Gamma << sh >> |-- [s] === [s'] : Delta) : exists ssh, Gamma << ssh >>  |-- [s'] : Delta.
Proof.
assert (Gamma |-- [s] === [s'] : Delta) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WtSb_from_WtSbEq2.
eauto.
Qed.
Hint Resolve WtSb_from_WtSbEq2.


(******************************************************************************
 *)
Lemma WfSbCxt_from_WtSb Gamma sh s Delta (H : Gamma << sh >> |-- [s] : Delta) : exists ssh, Delta << ssh >> |--.
Proof.
assert (Gamma |-- [s] : Delta) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfSbCxt_from_WtSb.
eauto.
Qed.
Hint Resolve WfSbCxt_from_WtSb.

(******************************************************************************
 *)
Lemma WfCxt_from_WtSb Gamma sh s Delta (H : Gamma << sh >> |-- [s] : Delta) : exists ssh, Gamma << ssh >> |--.
Proof.
assert (Gamma |-- [s] : Delta) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfCxt_from_WtSb.
eauto.
Qed.
Hint Resolve WfCxt_from_WtSb.


(******************************************************************************
 *)
Lemma WfCxt_from_WfCxtEq1 sh Gamma Delta (H : << sh >> |-- {Gamma} === {Delta}) : exists ssh, Gamma << ssh >> |--.
Proof.
assert (|-- {Gamma} === {Delta}) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfCxt_from_WtSb.
eauto.
Qed.
Hint Resolve WfCxt_from_WfCxtEq1.

(******************************************************************************
 *)
Lemma WfCxt_from_WfCxtEq2 sh Gamma Delta (H : << sh >> |-- {Gamma} === {Delta}) : exists ssh, Delta << ssh >> |--.
Proof.
assert (|-- {Gamma} === {Delta}) as HX1 by (eapply SystemN_to_System; eauto).

eapply System_to_SystemN.
eapply Validity.WfCxt_from_WtSb.
eauto.
Qed.
Hint Resolve WfCxt_from_WfCxtEq2.
