(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.Lift.


(******************************************************************************
 *)
Theorem RelReify1_neutral_a: forall e e' Gamma T
  (i : e === e' #in# PerNe),
  Gamma ||- T #in# PerType_ne i ->
   exists A : Tm,
     RbNf (length Gamma) (DdownN (Dup Duniv e)) A /\ Gamma |-- T === A
.
Proof.
intros.
simpl in *.
decompose record H.

assert (CxtShift Gamma 0 Gamma) as HX1 by eauto.
specialize (H1 _ _ HX1).
destruct H1 as [tv [HTV1 HTV2]].
exists tv; split; eauto.

Qed.
Hint Immediate RelReify1_neutral_a.


(******************************************************************************
 *)
Theorem RelReify1_neutral_b: forall e e' Gamma T t dt
  (i : e === e' #in# PerNe),
  Gamma ||- T #in# PerType_ne i ->
  Gamma ||- t : T #aeq# dt #in# PerType_ne i ->
   exists v : Tm,
     RbNf (length Gamma) (Ddown (Dup Duniv e) dt) v /\ Gamma |-- t === v : T
.
Proof.
intros.
simpl in *.
decompose record H0.

assert (CxtShift Gamma 0 Gamma) as HX1 by eauto.
specialize (H5 _ _ HX1).
destruct H5 as [tv [HTV1 HTV2]].
exists tv; split; auto.


assert (Gamma |-- TmSb T (Sups 0) === T) as HX2 by eauto.
assert (Gamma |-- TmSb t (Sups 0) === t : T) as HX3 by eauto.
eauto.
Qed.
Hint Immediate RelReify1_neutral_b.

(******************************************************************************
 *)

Theorem RelReify1_neutral_c: forall e e' Gamma T t k 
  (i : e === e' #in# PerNe),
  Gamma ||- T #in# PerType_ne i ->
  k === k #in# PerNe ->
  (forall (Delta : Cxt) (i : nat),
       CxtShift Delta i Gamma ->
       exists tv : Tm,
         RbNe (length Delta) k tv /\
         Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
   Gamma ||- t : T #aeq# Dup (Dup Duniv e) k #in# PerType_ne i
.
Proof.
intros.
simpl in *.
decompose record H. 
rewrite Dup_Duniv in *.
rewrite Dup_DupN in *.
repeat (split; auto).
-{
intros.

specialize (H1 _ _ H4).
destruct H1 as [tv [HTV1 HTV2]].
exists tv; split; auto.
}
Qed.
Hint Immediate RelReify1_neutral_c.

