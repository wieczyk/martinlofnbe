(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Key lemma for logical relations: canonical form of an expression is
 * convertible to related expression
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.Lift.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.

Require Import Nbe.LogicalRelations.ConvertibleToReification.Nat.
Require Import Nbe.LogicalRelations.ConvertibleToReification.Unit.
Require Import Nbe.LogicalRelations.ConvertibleToReification.Fun.
Require Import Nbe.LogicalRelations.ConvertibleToReification.Empty.
Require Import Nbe.LogicalRelations.ConvertibleToReification.Univ.
Require Import Nbe.LogicalRelations.ConvertibleToReification.NeUniv.

(******************************************************************************
 *)
 
Theorem RelReify1: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT ->
    (exists A, RbNf (length Gamma) (DdownN DT) A /\ Gamma |-- T === A
    ) /\

    (forall t dt, Gamma ||- t : T #aeq# dt #in# HDT ->
      (exists v, RbNf (length Gamma) (Ddown DT dt) v /\ Gamma |-- t === v : T)
    ) /\

    (forall t k, k === k #in# PerNe ->
      (forall Delta i, CxtShift Delta i Gamma -> exists tv,
        RbNe (length Delta) k tv /\
        Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i))
      ->
      Gamma ||- t : T #aeq# (Dup DT k) #in# HDT
    )
.
Proof.
do 5 intro.
generalize dependent T.
generalize dependent Gamma.
induction HDT using PerType_dind; split; try split; intros; eauto.
Qed.

(******************************************************************************
 *)

Theorem RelType_Reify: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT ->
    (exists A, RbNf (length Gamma) (DdownN DT) A /\ Gamma |-- T === A)
.
Proof.
intros.
edestruct RelReify1; intuition; eauto.
Qed.


(******************************************************************************
 *)
Theorem RelTerm_Reify: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT ->
    (forall t dt, Gamma ||- t : T #aeq# dt #in# HDT ->
      (exists v, RbNf (length Gamma) (Ddown DT dt) v /\ Gamma |-- t === v : T)
    )
.
Proof.
intros.
edestruct RelReify1; intuition; eauto.
Qed.

(******************************************************************************
 *)
Theorem RelTerm_Back: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT ->
    (forall t k, k === k #in# PerNe ->
      (forall Delta i, CxtShift Delta i Gamma -> exists tv,
        RbNe (length Delta) k tv /\
        Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i))
      ->
      Gamma ||- t : T #aeq# (Dup DT k) #in# HDT
    )
.
Proof.
intros.
edestruct RelReify1; intuition; eauto.
Qed.

