(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

(******************************************************************************
 *)

Theorem RelReify1_Dnat_a: forall Gamma T, 
  Gamma ||- T #in# PerType_nat ->
  exists A : Tm, RbNf (length Gamma) (DdownN Dnat) A /\ Gamma |-- T === A
.
Proof.
intros.
exists TmNat; eauto.
Qed.
Hint Immediate RelReify1_Dnat_a.


(******************************************************************************
 *)
Theorem RelReify1_Dnat_b: forall Gamma T t dt,
  Gamma ||- T #in# PerType_nat ->
  Gamma ||- t : T #aeq# dt #in# PerType_nat ->
   exists v : Tm,
     RbNf (length Gamma) (Ddown Dnat dt) v /\ Gamma |-- t === v : T
.
Proof.
intros.
simpl in H0.
decompose record H0.
specialize (H4 Gamma 0).
destruct H4; try red; eauto.
exists x. simpl in *.
decompose record H2.
split; auto.
eapply EQ_TRANS.
eapply EQ_SYM.
eapply EQ_SBID; eauto.
eapply EQ_CONV; eauto.
Qed.
Hint Immediate RelReify1_Dnat_b.

(******************************************************************************
 *)
Theorem RelReify1_Dnat_c: forall Gamma T t k,
  Gamma ||- T #in# PerType_nat ->
  k === k #in# PerNe ->
  (forall (Delta : Cxt) (i : nat),
     CxtShift Delta i Gamma ->
     exists tv : Tm,
       RbNe (length Delta) k tv /\
       Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
   Gamma ||- t : T #aeq# Dup Dnat k #in# PerType_nat.
Proof.
simpl; repeat split; auto.
intros.
edestruct H1.
eauto.
program_simpl.
exists x.
split; auto.

eapply EQ_CONV.
apply H4.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
red in H2. eauto.
eauto.
eapply EQ_TP_SB_NAT.
eauto.
Qed.
Hint Immediate RelReify1_Dnat_c.
