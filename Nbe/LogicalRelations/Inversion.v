(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Basic inversion lemmas for logical relations
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

(******************************************************************************
 * RelTerm implies RelType
 *
 * The logical relation between judgement 'Gamma |-- t : T' and 'dt #in# (Interp DT)'
 * should be satisfied only when relation between 'Gamma |-- T' and 'DT' holds.
 *)

Fact RelTerm_implies_RelType: forall Gamma t T dt DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- t : T #aeq# dt #in# HDT ->
  Gamma ||- T #in# HDT
.
Proof.
intros.
generalize dependent Gamma.
generalize dependent t.
generalize dependent T.
generalize dependent dt.
induction HDT using PerType_dind; intros; try solve
  [ simpl in *; intros; program_simpl ].

+{ (* Fun *)
simpl in *; intros.
destruct H0 as [_H0 H0].
destruct H0 as [A [F [? [? ?]]]].
exists A, F. repeat split; program_simpl; auto.
program_simpl; auto.
intros.
edestruct H6 as [y]; eauto.
program_simpl.
}
Qed.
Hint Resolve RelTerm_implies_RelType.

(******************************************************************************
 *)

Fact RelTerm_has_InterpType: forall Gamma t T dt DT DT' (HDT: DT === DT' #in# PerType),
  Gamma ||- t : T #aeq# dt #in# HDT ->
  exists PT, InterpType DT PT 
.
Proof.
intros.
cut (Gamma ||- T #in# HDT); intros; eauto.
Qed.
Hint Resolve RelTerm_has_InterpType.

(******************************************************************************
 * RelTerm respects Interp
 *)

Fact RelTerm_resp_InterpType: forall Gamma t T dt DT DT' (HDT: DT === DT' #in# PerType) PT,
  Gamma ||- t : T #aeq# dt #in# HDT ->
 InterpType DT PT ->
 dt === dt #in# PT
.
Proof.
intros.
generalize dependent PT.
induction HDT using PerType_dind; intros; simpl in *; try solve
  [ program_simpl; clear_inversion H0; auto ].
(**)
+{ (* Universe *)
program_simpl; 
clear_inversion H1;
auto.
}
(**)
+{ (* Fun type *)
program_simpl.
find_extdeters.
apply H10.
eauto.
}
Qed.
Hint Resolve RelTerm_resp_InterpType.

(******************************************************************************
 * RelType implies type judgemement
 *)

Fact RelType_implies_WfTp: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT ->
  Gamma |-- T
.
Proof.
intros.
induction HDT using PerType_dind; intros; try solve [simpl in H; eauto].
+{ (* Neutral type *) 
simpl in H.
decompose record  H; auto.
}

+{ 
simpl in H.
decompose record H; auto.
eauto.
}
Qed.
Hint Resolve RelTerm_resp_InterpType.

(******************************************************************************
 *)
Lemma SBID_inv: forall t T Gamma,
  Gamma |-- TmSb t Sid : T ->
  Gamma |-- t : T
.
Proof.
intros.
remember (TmSb t Sid).
induction H; clear_inversion Heqt0.

clear_inversion_on_sb H.
eapply ContextConversion_tm.
eapply CONV. eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eauto.
eauto.

eapply CONV.
apply IHWtTm; eauto.
eauto.
Qed.
Hint Resolve SBID_inv.

(******************************************************************************
 * RelTerm implies term judgemement
 *)

Fact RelTerm_implies_WtTm: forall Gamma t T dt DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- t : T #aeq# dt #in# HDT ->
  Gamma |-- t : T
.
Proof.
intros.
induction HDT using PerType_dind; intros.

+{ (* Neutral type *)
simpl in H; program_simpl.
specialize (H2 Gamma 0).
destruct H2. red; eauto.
program_simpl.

simpl in *.
assert (Gamma |-- TmSb t Sid : T).
eapply WtTm_from_WtTmEq1; eauto.

apply SBID_inv. 
eauto.
}

(**)
+{ (* Nat type *)
simpl in H; program_simpl.
specialize (H1 Gamma 0).
destruct H1. red; eauto.
program_simpl.

simpl in *.
assert (Gamma |-- TmSb t Sid : TmNat).
eapply WtTm_from_WtTmEq1; eauto.

apply SBID_inv. 
apply CONV with TmNat. auto.
eauto.
}
(**)
+{ (* Unit type *)
simpl in H; program_simpl.
specialize (H1 Gamma 0).
destruct H1. red; eauto.
program_simpl.

simpl in *.
assert (Gamma |-- TmSb t Sid : TmUnit).
eapply WtTm_from_WtTmEq1; eauto.

apply SBID_inv. 
apply CONV with TmUnit. auto.
eauto.
}
(**)
+{ (* Empty type *)
simpl in H; program_simpl.
specialize (H1 Gamma 0).
destruct H1. red; eauto.
program_simpl.

simpl in *.
assert (Gamma |-- TmSb t Sid : TmEmpty).
{
eapply WtTm_from_WtTmEq1; eauto.
}

apply SBID_inv. 
apply CONV with TmEmpty. auto.
eauto.
}
(**)
+{ (* Universe *)
simpl in H.
program_simpl.

}
(**)
+{ (* Fun type *)
simpl in H.
program_simpl.
}
(**)
Qed.

(******************************************************************************
 * RelTerm implies type judgemement
 *)

Fact RelTerm_implies_WfTp: forall Gamma t T dt DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- t : T #aeq# dt #in# HDT ->
  Gamma |-- T
.
Proof.
intros.
eapply RelType_implies_WfTp.
eapply RelTerm_implies_RelType.
eauto.
Qed.

(******************************************************************************
 *)
Lemma RelSb_wellformed: forall Gamma sb Delta denv,
  Gamma ||- [sb] : Delta #aeq# denv ->
  Gamma |-- [sb] : Delta
.
Proof.
intros.
clear_inversion H; eauto.
Qed.

(******************************************************************************
 *)
Lemma RelSb_valenv: forall Gamma sb Delta denv,
  Gamma ||- [sb] : Delta #aeq# denv ->
  [denv] === [denv] #in# Delta
.
Proof.
intros.
induction H; auto.
econstructor 2; eauto.
red.
eexists; eauto.
Qed.

