(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Common.

(******************************************************************************
 *)
Lemma PerNat_is_closed_under_per: forall T Gamma DT'_ DT'' t dt dt' PT
  (H : DT'_ = Dnat)
  (HDT' : DT'_ === DT'' #in# PerType)
  (H0 : dt === dt' #in# PT)
  (H1 : InterpType Dnat PT),
   Gamma ||- t : T #aeq# dt #in# PerType_nat <->
   Gamma ||- t : T #aeq# dt' #in# HDT'
.
Proof.
intros.
destruct HDT'; try solve [clear_inversion H ].
clear_inversion H1.

split; intro;
close_doms; auto; split; auto; intros;
split; intros; simpl in *; program_simpl; auto;
edestruct H6 as [v]; program_simpl; eauto.

(* <- *)

exists v. split; auto.
apply Reflects_same with dt' PerNat; auto.
symmetry; auto.
Qed.
Hint Resolve PerNat_is_closed_under_per.
