(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

(******************************************************************************
 *)

Lemma Reflects_same: forall dt dt' DT PT n v,
  DT === DT #in# PerType ->
  InterpType DT PT ->
  dt === dt' #in# PT ->
  RbNf n (Ddown DT dt) v ->
  RbNf n (Ddown DT dt') v
.
Proof.
intros.

assert (Ddown DT dt === Ddown DT dt' #in# PerNf).
eapply Reify_Characterization; eauto.
clear_inversion H3.
destruct (H4 n) as [v'].
program_simpl.
assert (v = v').
eapply RbNf_deter; eauto.
subst.
assumption.
Qed.  
Hint Resolve Reflects_same.
