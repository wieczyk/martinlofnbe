(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Nbe.Syntax.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.ProofIrrelevance.
Require Import Nbe.LogicalRelations.Lift.


(******************************************************************************
 *)
Theorem Rel_Fundamental_Fun: forall Gamma A  B Delta SB denv DT DT' (HDT : DT === DT' #in# PerType),
  Gamma |-- A ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
      EvalTm A denv DT -> Delta ||- TmSb A SB #in# HDT) ->
  Gamma,, A |-- B ->
  ( forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma,, A #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
       EvalTm B denv DT -> Delta ||- TmSb B SB #in# HDT) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm (TmFun A B) denv DT ->
  Delta ||- TmSb (TmFun A B) SB #in# HDT
.
Proof.
intros.

assert (Delta |-- [SB] : Gamma) by (eapply RelSb_wellformed; eauto).

destruct HDT; try solve [clear_inversion H4].
simpl.
exists (TmSb A SB).
exists (TmSb B (Sext (Sseq SB Sup) TmVar)).
split.

eapply EQ_TP_SB_FUN.
eauto.
auto.
auto.

split.
eapply H0.
eauto.
clear_inversion H4.
auto.

(**)

intros.

rename i1 into HREC.
rename e into HappDF.
rename e0 into HappDF'.
rename i into HDA.
rename i2 into i.

assert (Delta0 |-- a : TmSb (TmSb A SB) (Sups i)) as HYY1.
eapply RelTerm_implies_WtTm; eauto.

assert (Delta0 |-- TmSb (TmSb A SB) (Sups i) === TmSb A (Sseq SB (Sups i))) as HYY2.
eapply EQ_TP_SBSEQ; eauto.

rename H2 into Hgoal.

assert (Delta0 |-- [Sext (Sseq SB (Sups i)) a] : Gamma,,A) as HYY3.
eapply SEXT; eauto. 

clear_inversion H4.
renamer.

assert ( [denv] === [denv] #in# Gamma) as HYY4.
eapply RelSb_valenv.
eauto.

assert ( [Dext denv da] === [Dext denv da] #in# Gamma,,A ) as HYY5.
econstructor 2.
eauto.
exists DA_denv.
split; eauto.
auto.

assert (Delta0 ||- [Sseq SB (Sups i)] : Gamma #aeq# denv) as HYY6.
eapply RelSb_ClosedUnderLift.
eauto.
eauto.

assert (Delta0 ||- a : TmSb A (Sseq SB (Sups i)) #aeq# da #in# HDA) as HYY7.
eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
eauto.

assert (Delta0 ||- [Sext (Sseq SB (Sups i)) a] : Gamma,,A #aeq# Dext denv da) as HYY8.
econstructor 2; eauto.

inversion Happ; subst.

move Hgoal at bottom.
specialize (Hgoal _ _ _ HYY8).
specialize (Hgoal _ _ (HREC da da DB DB' PDA_denv HPA Hda Happ Happ') H11).


assert (Delta ||- TmSb A SB #in# HDA) as HYYY1.
eauto.

(**)

assert (Delta |-- TmSb A SB) as HHH1.
eapply RelType_implies_WfTp.
eauto.

assert (Delta0 |-- TmSb (TmSb B (Sext (Sseq SB Sup) TmVar)) (Sext (Sups i) a) === TmSb B (Sext (Sseq SB (Sups i)) a)) as HYY10.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.

eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply RelType_implies_WfTp.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.

eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.

eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply RelType_implies_WfTp.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

assert ( Delta0 |-- a : TmSb (TmSb A SB) (Sups i)) as HZZZ0.
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta0 |--  [Sext (Sups i) a]: Delta,,TmSb A SB) as HZZZ1.
eapply SEXT.
eauto.
eauto.
auto.



assert (Delta0 |-- [Sseq (Sseq SB Sup) (Sext (Sups i) a)] === [Sseq SB (Sups i)] : Gamma ) as HZZ1.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eauto.

(**)
eapply EQ_SB_CONG_SEXT.
apply HZZ1.
eauto.
(**)

assert (Delta0 |-- TmSb TmVar (Sext (Sups i) a) === a
   : TmSb A (Sseq (Sseq SB Sup) (Sext (Sups i) a))) as HZZZ2.

eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
apply HZZ1.
eauto.
eauto.
eauto.

(**)


eapply RelType_ClosedForConversion; eauto.

Qed.




