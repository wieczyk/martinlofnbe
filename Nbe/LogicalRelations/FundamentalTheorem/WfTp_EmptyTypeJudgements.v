(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.

(******************************************************************************
 *)
Lemma Fundamental_EMPTY_F: forall Gamma Delta SB denv DT DT' (HDT : DT === DT' #in# PerType),
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmEmpty denv DT ->
  Delta ||- TmSb TmEmpty SB #in# HDT
.
Proof.
intros.
assert (Delta |-- [SB] : Gamma) by (eapply RelSb_wellformed; eauto).
destruct HDT; try solve [clear_inversion H1].
simpl.
eauto.
Qed.
Hint Immediate Fundamental_EMPTY_F.

