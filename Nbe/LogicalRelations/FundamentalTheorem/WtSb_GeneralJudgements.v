(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.Lift.


(******************************************************************************
 *)
Lemma Fundamental_SB_CONV: forall Gamma SB Delta denv,
    Gamma ||- [SB] : Delta #aeq# denv -> forall Psi,
     |-- {Delta} === {Psi} ->
    Gamma ||- [SB] : Psi #aeq# denv
.
Proof.

do 5 intro.
induction H; intros.
clear_inversion H0.
eauto.

(**)
clear_inversion H7; [ eauto | ].

assert (Delta |= A === B) as HX1.
eapply Valid_for_WfTpEq.
eauto.

assert (exists DB, EvalTm B denv DB /\ InterpType DB PX /\ DX' === DB #in# PerType ) as HX2.
clear_inversion HX1.
specialize (H8 _ _ H3).
program_simpl.
renamer.
elim_deters.
exists DB_denv. 
repeat split; eauto.
eapply PER_Transitive.
symmetry.
eauto.
eauto.

program_simpl.
rename HX2 into DB.
rename H9 into HDT.

assert (DB === DX' #in# PerType) as HDB.
symmetry.
eauto.
assert (Gamma ||- TmSb A sb #in# HDB) as HX3.
apply RelType_ClosedUnderPer_sym with (HDT1 := HDT).
eapply RelType_ClosedUnderPer.
eauto.

assert (Gamma ||- TmSb B sb #in# HDB) as HX4.
eapply RelType_ClosedForConversion.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eapply RelSb_wellformed.
eauto.
eauto.



assert (Gamma ||- M : TmSb A sb #aeq# dM #in# HDB) as HX5.
eapply RelTerm_ClosedUnderPer_sym with (HDT1 := HDT).
eapply H6.
eauto.
eapply RelTerm_ClosedUnderPer; eauto.

assert (Gamma ||- M : TmSb B sb #aeq# dM #in# HDB) as HX6.
eapply RelTerm_ClosedForConversion with (T := TmSb A sb).
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eapply RelSb_wellformed.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eapply RelTerm_implies_WtTm.
eauto.

assert (|= {Delta} === {Delta0}) as HX7.
eapply Valid_for_WfCxtEq.
eauto.

assert ([denv] === [denv] #in# Delta0).
clear_inversion HX7.
eapply H15.
eauto.

econstructor 2 with (M := M) (sb := sb) (HDX := HDB) (PX := PX); eauto.

Qed.
Hint Immediate Fundamental_SB_CONV.

(******************************************************************************
 *)
Lemma Fundamental_SID: forall Gamma Delta SB denv dsb,
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalSb Sid denv dsb ->
  Delta ||-  [Sseq Sid SB]:Gamma #aeq# dsb
.
Proof.
intros.

clear_inversion H1.
eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDL.
eapply RelSb_wellformed.
eauto.
auto.
Qed.
Hint Immediate Fundamental_SID.

(******************************************************************************
 *)
Lemma Fundamental_SSEQ: forall Gamma__0 Gamma__1 Gamma__2 S__1 S__2 Delta dsb denv SB,
  Gamma__0 |--  [S__2]:Gamma__1 ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma__0 #aeq# denv ->
      forall dsb : DEnv,
      EvalSb S__2 denv dsb -> Delta ||-  [Sseq S__2 SB]:Gamma__1 #aeq# dsb) ->
  Gamma__1 |--  [S__1]:Gamma__2 ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma__1 #aeq# denv ->
       forall dsb : DEnv,
       EvalSb S__1 denv dsb -> Delta ||-  [Sseq S__1 SB]:Gamma__2 #aeq# dsb) ->
  (Delta ||-  [SB]:Gamma__0 #aeq# denv) ->
  EvalSb (Sseq S__1 S__2) denv dsb ->
  Delta ||-  [Sseq (Sseq S__1 S__2) SB]:Gamma__2 #aeq# dsb
.
Proof.
intros.

clear_inversion H4.
renamer.

specialize (H0 _ _ _ H3 _ H7).
specialize (H2 _ _ _ H0 _ H10).

eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.
eapply RelSb_wellformed.
eauto.
eauto.
eauto.
eauto.
Qed.
Hint Immediate Fundamental_SSEQ.

(******************************************************************************
 *)
Lemma Fundamental_SUP: forall Gamma A Delta SB denv dsb,
  Gamma |-- A ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
      EvalTm A denv DT -> Delta ||- TmSb A SB #in# HDT) ->
  Delta ||-  [SB]:Gamma,, A #aeq# denv ->
  EvalSb Sup denv dsb ->
  Delta ||-  [Sseq Sup SB]:Gamma #aeq# dsb
.
Proof.
intros.

clear_inversion H2.
eapply RelSb_lift1_left.
eauto.
eauto.
Qed.
Hint Immediate Fundamental_SUP.

(******************************************************************************
 *)
Lemma Fundamental_SEXT: forall Gamma S Delta M A Delta0 SB denv dsb,
  Gamma |--  [S]:Delta ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
      Delta0 ||-  [SB]:Gamma #aeq# denv ->
      forall dsb : DEnv,
      EvalSb S denv dsb -> Delta0 ||-  [Sseq S SB]:Delta #aeq# dsb) ->
  Delta |-- A ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
       Delta0 ||-  [SB]:Delta #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
       EvalTm A denv DT -> Delta0 ||- TmSb A SB #in# HDT) ->
  Gamma |-- M : TmSb A S ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
       EvalTm (TmSb A S) denv DT ->
       EvalTm M denv dt ->
       Delta ||- TmSb M SB : TmSb (TmSb A S) SB #aeq# dt #in# HDT) ->
  Delta0 ||-  [SB]:Gamma #aeq# denv ->
  EvalSb (Sext S M) denv dsb ->
  Delta0 ||-  [Sseq (Sext S M) SB]:Delta,, A #aeq# dsb
.
Proof.
intros.


clear_inversion H6.
renamer.

assert (Delta |= A) as HvalA by (eapply Valid_for_WfTp; auto).

assert (Gamma |= [S] : Delta) as HvalS by (eapply Valid_for_WtSb; auto).

assert ([denv] === [denv] #in# Gamma) as Hvalenv.
eapply RelSb_valenv; eauto.

clear_inversion HvalS.
specialize (H8 _ _ Hvalenv).
destruct H8 as [denv0 [denv1 [HX1 [HX2 HX3]]]].
compute_sth.
renamer.

clear_inversion HvalA.
specialize (H10 _ _ HX3).

destruct H10 as [DA [DB [HDA1 [HDA2 HDA3]]]].
compute_sth.
renamer.

eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SYM.
eapply EQ_SB_SEXT.
eapply RelSb_wellformed.
eauto.
eauto.


specialize (H0 _ _ _ H5).
specialize (H0 _ H9).

assert (EvalTm (TmSb A S) denv DA_DS_denv) as HX4.
eauto.

specialize (H4 _ _ _ H5).
specialize (H4 _ _ HDA3 _ HX4 H12).

assert (Delta0 |-- [SB] : Gamma) as HY1 by (eapply RelSb_wellformed; eauto).

assert (Delta0 ||- TmSb M SB : TmSb A (Sseq S SB) #aeq# DM_denv #in# HDA3) as HX5.
eapply RelTerm_ClosedForConversion.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eapply CONV.
eapply Syntax.System.SB.
eauto.
eauto.
eapply EQ_TP_REFL.
eapply SB_F.
eauto.
eauto.


specialize (H2 _ _ _ H0).
specialize (H2 DA_DS_denv DA_DS_denv HDA3 HDA1).


assert (Gamma |= M : TmSb A S) as HvalTm by (eapply Valid_for_WtTm; auto).

clear_inversion HvalTm.
specialize (H10 _ _ Hvalenv).
destruct H10 as [dtm0 [dtm1 [PA [HD2 [HD3 [HD4 HD5]]]]]].
compute_sth. renamer.
auto.
destruct HD2 as [d [HD2' HD2'']].

econstructor 2 with (PX := PA).
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.

eauto.
 
(**)
auto.
auto.

clear_inversion HD2'.
auto. renamer.
compute_sth.

auto.
Qed.
Hint Immediate Fundamental_SEXT.

