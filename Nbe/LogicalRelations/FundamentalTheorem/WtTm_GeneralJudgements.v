(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)
Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Nbe.Syntax.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.ProofIrrelevance.
Require Import Nbe.LogicalRelations.Lift.


(******************************************************************************
 *)

Lemma Rel_Fundamental_Var: forall Delta Gamma T SB denv dt DT DT' (HDT : DT === DT' #in# PerType),
  Gamma,,T |-- ->
  Delta ||-  [SB]:Gamma,, T #aeq# denv ->
  EvalTm (TmSb T Sup) denv DT ->
  EvalTm TmVar denv dt ->
  Delta ||- TmSb TmVar SB : TmSb (TmSb T Sup) SB #aeq# dt #in# HDT
.
Proof.
intros.

clear_inversion H0.

clear_inversion H1.
clear_inversion H2.
clear_inversion H4.

renamer.

(* Coq8.4 -> Coq8.6: renamer traverses context in different direction *) 
rename DT_envs into DT_envs00. rename DT_envs0 into DT_envs. rename DT_envs00 into DT_envs0.

assert (DT_envs0 = DT_envs) as HY1.
eauto.

generalize dependent HDT.
rewrite HY1 in *.
clear HY1.
clear DT_envs0.
intros.
compute_sth.

assert (Delta |-- [sb] : Gamma) as HX1 by (eapply RelSb_wellformed; eauto).

assert (Delta |-- [SB] : Gamma,,T) as HX2 by eauto.

assert (Gamma |-- T) as HX3.
assert (Gamma,,T |--) as HY1 by eauto.
clear_inversion HY1; auto.

assert (Delta |-- M : TmSb T sb) as HX4.
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta |-- TmSb (TmSb T Sup) SB === TmSb (TmSb T Sup) (Sext sb M)) as HX5.
eapply EQ_TP_CONG_SB.
eauto.
eauto.

assert (Delta |-- TmSb (TmSb T Sup) SB === TmSb T (Sseq Sup (Sext sb M))) as HX6.
eapply EQ_TP_TRANS.
eapply HX5.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.



assert (Delta |-- TmSb (TmSb T Sup) SB === TmSb T sb) as HX7.
eapply EQ_TP_TRANS.
eapply HX6.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eauto.

assert (Delta |-- TmSb TmVar SB === M : TmSb (TmSb T Sup) SB) as HX8.
eapply EQ_TRANS.
eapply EQ_CONG_SB.
eauto.
eapply EQ_REFL; eauto.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.
eapply EQ_TP_SYM.
eapply HX7.


compute_sth; renamer.


assert (Delta ||- M : TmSb T sb #aeq# dt #in# HDT) as HX9.
eapply RelTerm_ClosedUnderPer_right; eauto.


apply RelTerm_ClosedForConversion with (T := TmSb T sb) (t := M).
eapply RelTerm_implies_RelType.
eauto.

eauto.
eauto.
eauto.
Qed.



(******************************************************************************
 *)
Lemma Fundamental_SB: forall Gamma M A S Delta Delta0 denv SB DT DT' (HDT : DT === DT' #in# PerType) dt,
  Gamma |--  [S]:Delta ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
      Delta0 ||-  [SB]:Gamma #aeq# denv ->
      forall dsb : DEnv,
      EvalSb S denv dsb -> Delta0 ||-  [Sseq S SB]:Delta #aeq# dsb) ->
  Delta |-- M : A ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
       Delta0 ||-  [SB]:Delta #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
       EvalTm A denv DT ->
       EvalTm M denv dt -> Delta0 ||- TmSb M SB : TmSb A SB #aeq# dt #in# HDT) ->
  Delta0 ||-  [SB]:Gamma #aeq# denv ->
  EvalTm (TmSb A S) denv DT ->
  EvalTm (TmSb M S) denv dt ->
  Delta0 ||- TmSb (TmSb M S) SB : TmSb (TmSb A S) SB #aeq# dt #in# HDT
.
Proof.
intros.

assert (Delta0 |-- [SB] : Gamma) as HX1 by (eapply RelSb_wellformed; eauto).

clear_inversion H4.
clear_inversion H5.
compute_sth.
renamer.

assert (Delta0 |-- TmSb (TmSb A S) SB === TmSb A (Sseq S SB)) as HX2.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

compute_sth. renamer.

eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.

eapply EQ_SYM.
eapply EQ_CONV.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.
eauto.
Qed.
Hint Immediate Fundamental_SB.

(******************************************************************************
 *)
Lemma Fundamental_CONV: forall Gamma t A B Delta SB denv DT DT' (HDT : DT === DT' #in# PerType) dt,
  Gamma |-- t : A ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm A denv DT ->
      EvalTm t denv dt -> Delta ||- TmSb t SB : TmSb A SB #aeq# dt #in# HDT) ->
  Gamma |-- A === B ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm B denv DT ->
  EvalTm t denv dt ->
  Delta ||- TmSb t SB : TmSb B SB #aeq# dt #in# HDT
.
Proof.
intros.

assert ([denv] === [denv] #in# Gamma) as Hvalenv.
eapply RelSb_valenv.
eauto.

assert (Gamma |= A === B) as Hvaleq.
apply Valid_for_WfTpEq; auto.

assert (Gamma |= t : A) as HvaltpA.
apply Valid_for_WtTm; auto.

assert (Delta |-- [SB]: Gamma) as HSB.
eapply RelSb_wellformed.
eauto.
 
clear_inversion Hvaleq.
specialize (H6 _ _ Hvalenv).

destruct H6 as [DA [DB [HDA [HDB HDT']]]].
compute_sth.
renamer.

clear_inversion HvaltpA.
specialize (H7 _ _ Hvalenv).
destruct H7 as [dtm0 [dtm1 [Hv1 [Hv2 [Hv3 [Hv4 Hv5]]]]]].
destruct Hv2 as [PT [HPT1 HPT2]].
compute_sth.
renamer.

specialize (H0 _ _ _ H2).
specialize (H0 _ _ HDT').
specialize (H0 _ HDA H4).

assert (Delta ||- TmSb t SB : TmSb A SB #aeq# Dt_denv #in# HDT) as Hstep1.
eapply RelTerm_ClosedUnderPer.
eauto.
eauto.
eauto.

eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eauto.
Qed.
Hint Immediate Fundamental_CONV.
