(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Fundamental theorem for logical relations
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ProofIrrelevance.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.Lift.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtSb_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_NatTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_NatTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_FunTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_FunTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_EmptyTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_EmptyTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_UnitTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_UnitTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_UnivTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_UnivTypeJudgements.


(******************************************************************************
 * Fundamental theorem for logical relations
 *)
Theorem Rel_Fundamental: 
    (forall Gamma, Gamma |-- -> True) /\

    (forall Gamma T, Gamma |-- T ->
      forall Delta SB denv,
      Delta ||- [SB] : Gamma #aeq# denv -> 
        forall DT DT', forall HDT : DT === DT' #in# PerType,
          EvalTm T denv DT ->
          Delta ||- TmSb T SB #in# HDT
    ) /\    

    (forall Gamma t T, Gamma |-- t : T -> 
      forall Delta SB denv,
      Delta ||- [SB] : Gamma #aeq# denv -> 
        forall DT DT', forall HDT : DT === DT' #in# PerType, forall dt,
          EvalTm T denv DT ->
          EvalTm t denv dt ->
          Delta ||- TmSb t SB : TmSb T SB #aeq# dt #in# HDT
    ) /\    

    (forall Gamma sb Psi, Gamma |-- [sb] : Psi ->
      forall Delta SB denv,
      Delta ||- [SB] : Gamma #aeq# denv -> 
       forall dsb,
         EvalSb sb denv dsb ->
         Delta ||- [Sseq sb SB] : Psi #aeq# dsb
    ) /\    
    (forall Gamma Delta, |-- {Gamma} === {Delta} -> True) /\    

    (forall Gamma T1 T2, Gamma |-- T1 === T2 -> True) /\    

    (forall Gamma t1 t2 T, Gamma |-- t1 === t2 : T -> True) /\    

    (forall Gamma sb1 sb2 Delta, Gamma |-- [sb1] === [sb2] : Delta ->  True)
.
Proof.
eapply System_ind; intros; auto; subst; eauto.

(* TP: TmFun *)
eapply Rel_Fundamental_Fun; eauto.

(* TmVar *)
eapply Rel_Fundamental_Var; eauto.

(* TmApp *)
eapply Rel_Fundamental_App; eauto.

(* TmAbs *)
eapply Rel_Fundamental_Abs; eauto.

(* TmS *)
eapply Rel_Fundamental_S; eauto.

(* TmAbsurd *)
eapply Rel_Fundamental_Absurd; eauto.

(**)
eapply Fundamental_SB_CONV.
eauto.
eauto.
Qed.

 
(******************************************************************************
 *)

Theorem Rel_Fundamental_Tp: 
    (forall Gamma T Delta SB denv DT (HDT : DT === DT #in# PerType),
      Gamma |-- T ->
      Delta ||- [SB] : Gamma #aeq# denv -> 
          EvalTm T denv DT ->
          Delta ||- TmSb T SB #in# HDT
    ).
Proof.
intros.
edestruct Rel_Fundamental; eauto.
program_simpl.
Qed.

(******************************************************************************
 *)
Theorem Rel_Fundamental_Tm:
    (forall Gamma t T Delta SB denv DT (HDT : DT === DT #in# PerType) dt,
      Gamma |-- t : T -> 
      Delta ||- [SB] : Gamma #aeq# denv -> 
          EvalTm T denv DT ->
          EvalTm t denv dt ->
          Delta ||- TmSb t SB : TmSb T SB #aeq# dt #in# HDT
    ).

Proof.
intros.
edestruct Rel_Fundamental; eauto.
program_simpl.
Qed.

