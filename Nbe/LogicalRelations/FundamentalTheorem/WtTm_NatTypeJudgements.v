(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.Lift.
Require Import Nbe.LogicalRelations.ConvertibleToReification.


(******************************************************************************
 *)
Lemma Fundamental_Tm0: forall Gamma Delta SB denv dt DT DT' (HDT : DT === DT' #in# PerType),
 Gamma |-- ->
 Delta ||-  [SB]:Gamma #aeq# denv ->
 EvalTm TmNat denv DT ->
 EvalTm Tm0 denv dt ->
 Delta ||- TmSb Tm0 SB : TmSb TmNat SB #aeq# dt #in# HDT
.
Proof.
intros.

assert (Delta |-- [SB] : Gamma) by (eapply RelSb_wellformed; eauto).
destruct HDT; try solve [clear_inversion H1].
simpl.
clear_inversion H2.
repeat (split; auto).
eauto.

intros.
exists Tm0.
simpl.
repeat (split; auto).


eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_CONG_SB.
eauto.
eapply EQ_SBNAT0.
eauto.
eauto.
eapply EQ_SBNAT0.
eauto.
Qed.
Hint Immediate Fundamental_Tm0.

(******************************************************************************
 *)
Theorem Rel_Fundamental_S: forall Gamma n Delta SB denv DT DT' (HDT : DT === DT' #in# PerType) dt,
  Gamma |-- n : TmNat ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm TmNat denv DT ->
      EvalTm n denv dt ->
      Delta ||- TmSb n SB : TmSb TmNat SB #aeq# dt #in# HDT) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmNat denv DT ->
  EvalTm (TmS n) denv dt ->
   Delta ||- TmSb (TmS n) SB : TmSb TmNat SB #aeq# dt #in# HDT
.
Proof.
intros.
assert (Delta |-- [SB] : Gamma) as HYY1 by (eapply RelSb_wellformed; eauto).

destruct HDT; try solve [clear_inversion H2].
simpl.

clear_inversion H3.

assert (Gamma |= n : TmNat) as HVALTP.
{
apply Valid_for_WtTm.
auto.
}

assert ( [denv] === [denv] #in# Gamma ) as Hdenv.
{
eapply RelSb_valenv.
eauto.
}
clear_inversion HVALTP.
specialize (H4 _ _ Hdenv).
program_simpl.
specialize (H0 _ _ _ H1).
specialize (H0 _ _ PerType_nat).
specialize (H0 _ H2 H5).
simpl in H0; program_simpl.
repeat (split; auto).
intros.
specialize (H13 _ _ H14).
program_simpl.
exists (TmS H13).
split.
-{
apply reifyNf_S. auto.
}
-{
eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eapply EQ_SBNATS.
eauto.
eauto.
eauto.


eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_SBNATS.
eauto.
eauto.
eauto.

eapply EQ_CONG_S.
eauto.

}
Qed.

