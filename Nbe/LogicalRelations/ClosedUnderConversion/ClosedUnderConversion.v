(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Proof that logical relations are closed under conversion on expressions
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.Nat.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.Empty.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.Unit.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.Fun.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.Univ.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.NeUniv.


(******************************************************************************
 * Closure under conversion
 *)

Theorem ClosedForConversion: forall Gamma T DT DT' (HDT: DT === DT' #in# PerType),
  Gamma ||- T #in# HDT -> forall T',
  Gamma |-- T === T' ->
  (Gamma ||- T' #in# HDT) /\
  (forall dt t t', Gamma ||- t : T #aeq# dt #in# HDT ->  Gamma |-- t === t' : T ->  Gamma ||- t' : T' #aeq# dt #in# HDT)
.
Proof.
do 5 intro.
generalize dependent Gamma.
generalize dependent T.
induction HDT using PerType_dind; intros; eauto.

Qed.

(******************************************************************************
 * Closure under conversion (unpacking)
 *)

Corollary RelType_ClosedForConversion: forall Gamma T DT DT' (HDT: DT === DT' #in# PerType) T',
  Gamma ||- T #in# HDT ->
  Gamma |-- T === T' ->
  (Gamma ||- T' #in# HDT)
.
Proof.
intros.
edestruct ClosedForConversion; eauto.
Qed.

Corollary RelTerm_ClosedForConversion: forall Gamma T DT DT' (HDT: DT === DT' #in# PerType) T' dt t t',
  Gamma ||- T #in# HDT -> 
  Gamma |-- T === T' ->
  Gamma ||- t : T #aeq# dt #in# HDT ->  Gamma |-- t === t' : T ->  Gamma ||- t' : T' #aeq# dt #in# HDT
.
Proof.
intros.
edestruct ClosedForConversion; eauto.
Qed.

(******************************************************************************
 * Closed under sb equality
 *)

Lemma RelSb_ClosedUnderEq: forall Gamma sb1 sb2 Delta denv,
  Gamma |-- [sb1] === [sb2] : Delta ->
  Gamma ||- [sb1] : Delta #aeq# denv ->
  Gamma ||- [sb2] : Delta #aeq# denv
.
Proof.
intros.
induction H0.
constructor 1; eauto.
(**)
econstructor 2; eauto.
Qed.

