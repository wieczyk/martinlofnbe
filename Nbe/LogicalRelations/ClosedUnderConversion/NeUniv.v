(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

(******************************************************************************
 *)
Lemma PerType_neuniv_is_closed_under_conversion: forall e e' T Gamma T'
  (i : e === e' #in# PerNe),
  (Gamma ||- T #in# PerType_ne i) ->
  (Gamma |-- T === T') ->
   Gamma ||- T' #in# PerType_ne i /\
   (forall (dt : D) (t t' : Tm),
    Gamma ||- t : T #aeq# dt #in# PerType_ne i ->
    Gamma |-- t === t' : T -> Gamma ||- t' : T' #aeq# dt #in# PerType_ne i).
Proof.   
intros.
split.
+{ (* Type *)
simpl; split; eauto.
intros.
simpl in H.
decompose record H.
edestruct H3 as [v]; eauto.
decompose record H4.
exists v.
split; auto.

eapply EQ_TP_TRANS; [ | eassumption ].
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
}

+{ (* Term *)
intros.
simpl.

split; [ | split; [ | split ] ]; eauto.
-{
intros.
simpl in H.
decompose record H.
edestruct H5 as [v]; eauto.
decompose record H6.
exists v.
split; auto.

eapply EQ_TP_TRANS; [ | eassumption ].
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
}

-{
intros.
simpl in H1.
decompose record H1.
edestruct H8 as [v]; eauto.
decompose record H7.
exists v; split; auto.

eapply EQ_CONV.
eapply EQ_TRANS; [ | eassumption ].
eapply EQ_CONG_SB; eauto.
eapply EQ_TP_CONG_SB; eauto.
}
}
Qed.
Hint Immediate PerType_neuniv_is_closed_under_conversion.
