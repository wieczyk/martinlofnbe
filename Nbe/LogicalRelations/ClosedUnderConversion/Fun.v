(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)
 
Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.


(******************************************************************************
 *)
Lemma PerType_fun_is_closed_under_conversion: forall DA DF DA' DF' PA T Gamma T'
  (i : DA === DA' #in# PerType)
  (i0 : InterpType DA PA)
  (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
  (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
  (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
  (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (T : Tm) (Gamma : Cxt),
      Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
      forall T' : Tm,
      Gamma |-- T === T' ->
      Gamma ||- T' #in# i1 a0 a1 DB0 DB1 P i i0 a a2 /\
      (forall (dt : D) (t t' : Tm),
       Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
       Gamma |-- t === t' : T ->
       Gamma ||- t' : T' #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2))
  (IHHDT : forall (T : Tm) (Gamma : Cxt),
          Gamma ||- T #in# i ->
          forall T' : Tm,
          Gamma |-- T === T' ->
          Gamma ||- T' #in# i /\
          (forall (dt : D) (t t' : Tm),
           Gamma ||- t : T #aeq# dt #in# i ->
           Gamma |-- t === t' : T -> Gamma ||- t' : T' #aeq# dt #in# i))
  (H0 : Gamma ||- T #in# PerType_fun i i0 e e0 i1)
  (H1 : Gamma |-- T === T'),
   Gamma ||- T' #in# PerType_fun i i0 e e0 i1 /\
   (forall (dt : D) (t t' : Tm),
    Gamma ||- t : T #aeq# dt #in# PerType_fun i i0 e e0 i1 ->
    Gamma |-- t === t' : T ->
    Gamma ||- t' : T' #aeq# dt #in# PerType_fun i i0 e e0 i1)
.
Proof.
repeat split.
simpl in *.

destruct H0 as [A [F [? ?]]].
exists A, F.
program_simpl.
repeat split; auto.
eapply EQ_TP_TRANS; eauto using H0.

(**)

eauto.

(**)

intros.
renamer.
rename i into HDA.
rename i1 into HDB.
rename i0 into HPA.
intros.

simpl.
rename H0 into K_T.
rename H2 into K_t.

set (H_t := K_t).
specialize (H_t).
simpl in H_t.
destruct H_t as [_H_t H_t].
destruct H_t as [A [F [PT ?]]].
program_simpl.
renamer.
intros.
exists A, F, PT.
repeat split; auto.
eapply EQ_TP_TRANS; eauto using H1.

intros.
find_extdeters.
close_extdeters.
close_doms.
renamer; intros.

edestruct (H6 Delta i a da DB DB' PDA0 HPA1 Hda Happ Happ') as [dy ?]; eauto.
exists dy.
program_simpl.
repeat split; auto.

move H at bottom.
specialize (H da da DB DB' PDA0).

specialize (H HPA1 Hda).
specialize (H Happ Happ').
specialize (H (TmSb F (Sext (Sups i) a)) Delta).

assert (Delta ||- TmSb F (Sext (Sups i) a) #in# HDB da da DB DB' PDA0 HPA1 Hda Happ Happ' ).
eapply RelTerm_implies_RelType; eauto.
specialize (H H15).
specialize (H (TmSb F (Sext (Sups i) a))).

assert (Delta |-- TmSb F (Sext (Sups i) a) === TmSb F (Sext (Sups i) a)).
eapply EQ_TP_REFL.

eapply RelType_implies_WfTp.
eauto.

specialize (H H16).
destruct H.
specialize (H17 dy (TmApp (TmSb t (Sups i)) a)).
specialize (H17 (TmApp (TmSb t' (Sups i)) a)).
apply H17.
auto.

(**)

assert (Gamma |-- A /\ Gamma,,A |-- F).
eapply Inversion_FUN_F.
eauto.
destruct H18.



assert (Delta |-- a : TmSb A (Sups i)).
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta |-- TmSb A (Sups i)).
eapply WfTp_from_WtTm.
eauto.

assert (Delta |-- a : TmSb (TmSb A (Sups i)) Sid).
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
auto.

assert (Delta,, TmSb A (Sups i) |-- TmVar : TmSb A (Sseq (Sups i) Sup)).
eapply CONV.
eapply HYP.
eauto.
eauto.

assert (Delta |--  [Sext Sid a]: Delta,,TmSb A (Sups i)) by eauto.


assert (Delta |-- TmSb F (Sext (Sups i) a) === TmSb (TmSb F (Sext (Sups (S i)) TmVar)) (Ssing a)).


eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
unfold Ssing.
eauto.
eapply SEXT.
simpl.
eapply SSEQ.
eapply SUP.
assumption.
apply H7.
eauto.
simpl.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
unfold Ssing.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eapply SEXT.
simpl.
eapply SSEQ.
eauto.
apply H7.
eauto.
simpl.
auto.

assert (   Delta,, TmSb A (Sups i) |--  [Sups (S i)]=== [Sseq Sup (Sups i)]:Gamma).
apply Sups_tail.
simpl.
eapply SSEQ.
eauto.
eauto.

eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_TRANS.

simpl.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.

eapply EQ_SB_REFL.
eauto.
eauto.
eauto.

eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
simpl.

eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.
eauto.
eauto.

(**)

eapply EQ_CONV.
eapply EQ_CONG_FUN_E.
eapply EQ_CONV.
eapply EQ_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eauto.

(**)

auto.

Qed.
Hint Immediate PerType_fun_is_closed_under_conversion.
