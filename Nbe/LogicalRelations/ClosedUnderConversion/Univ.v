(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.


(******************************************************************************
 *)
Lemma PerType_univ_is_closed_under_conversion: forall T Gamma T' i,
  (forall d1 d2 : D, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType) ->
  (forall (d1 d2 : D) (i0 : d1 === d2 #in# PerUniv) (T : Tm) (Gamma : Cxt),
      Gamma ||- T #in# i d1 d2 i0 ->
      forall T' : Tm,
      Gamma |-- T === T' ->
      Gamma ||- T' #in# i d1 d2 i0 /\
      (forall (dt : D) (t t' : Tm),
       Gamma ||- t : T #aeq# dt #in# i d1 d2 i0 ->
       Gamma |-- t === t' : T -> Gamma ||- t' : T' #aeq# dt #in# i d1 d2 i0)) ->
  Gamma ||- T #in# PerType_univ i ->
  Gamma |-- T === T' ->

   Gamma ||- T' #in# PerType_univ i /\
   (forall (dt : D) (t t' : Tm),
    Gamma ||- t : T #aeq# dt #in# PerType_univ i ->
    Gamma |-- t === t' : T -> Gamma ||- t' : T' #aeq# dt #in# PerType_univ i)

.
Proof.
simpl in *.

intros; repeat (apply conj); eauto; intros.

assert (Gamma |-- t === t') as HX1.
{
eapply EQ_TP_UNIV_ELEM.
eapply EQ_CONV; eauto.
}

decompose record H3.
rename x into HDU.

repeat (apply conj); eauto.
+{
intros.
specialize (H6 _ _ H9).
destruct H6 as [tv [HTV1 HTV2]].
exists tv; split; auto.

eapply EQ_SYM.
eapply EQ_TRANS.
eapply EQ_SYM.
eauto.
eapply EQ_CONV.
eapply EQ_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eassumption.
eauto.
}

+{
exists HDU.

specialize (H0 _ _ HDU).
specialize (H0 _ _ H8).
specialize (H0 _ HX1).
decompose record H0; auto.
}
Qed.
Hint Immediate PerType_univ_is_closed_under_conversion.


