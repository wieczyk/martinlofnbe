(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ContextConversion.
Require Import Nbe.Syntax.ContextConversion.
Require Import Nbe.Syntax.SbInversion.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff. 


(******************************************************************************
 *)

Theorem RelLifting: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall Delta i, CxtShift Delta i Gamma ->
    (Gamma ||- T #in# HDT -> Delta ||- TmSb T (Sups i) #in# HDT) /\
    (forall t dt, Gamma ||- t : T #aeq# dt #in# HDT -> Delta ||- TmSb t (Sups i) : TmSb T (Sups i) #aeq# dt #in# HDT)
.
Proof.
intros until HDT.
generalize dependent Gamma.
generalize dependent T.
induction HDT using PerType_dind; intros.

(* Neutral 1 *)
+{
split; intros.
-{
simpl in *.
intros.
decompose record H0.
split.
eapply SB_F; eauto.
intros.

edestruct H2 with (Delta := Delta0); eauto.
decompose record H4.
exists x.
split; auto.

eapply EQ_TP_TRANS; [ | eauto ].
eapply EQ_TP_SYM.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
}
(* Neutral 2 *)
-{
*{
intros.
simpl in *.
decompose record H0.
repeat split; eauto; intros.
{
edestruct H2 with (Delta := Delta0); eauto.
decompose record H6.
exists x.
split; auto.

eapply EQ_TP_TRANS; [ | eauto ].
eapply EQ_TP_SYM.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
}
*{
edestruct H5 with (Delta := Delta0); eauto.
decompose record H6.
exists x.
split; auto.

assert (Delta0 |-- TmSb T (Sups (i0 + i1)) === TmSb (TmSb T (Sups i0)) (Sups i1)) as HX1.
{
eapply nc_Sups_tp_plus_eq; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
}
rewrite plus_comm in HX1.
eapply EQ_CONV; [ | eapply HX1 ].
eapply EQ_TRANS; [ | eauto ].
eapply EQ_SYM.
rewrite plus_comm.
eapply nc_Sups_plus_eq; eauto.
rewrite plus_comm; eauto.
}

}
}
}
(* Dnat 1 *)
+{
simpl in *.

assert (Gamma |-- T === TmNat -> Delta |-- TmSb T (Sups i) === TmNat).
intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
red in H. eauto.
eauto.
eapply EQ_TP_SB_NAT.
eauto.
split; auto; intros.
(* Dnat 2 *)

simpl in *.
program_simpl.
split; auto.
split; auto.
intros.
edestruct (H3 Delta0).
eauto.
program_simpl.
exists x.
split ;auto.
eapply EQ_TRANS.
eapply EQ_SYM.
apply nc_Sups_plus_eq.
rewrite plus_comm.
eauto.
rewrite plus_comm.
auto.
}
(* Dunit 1 *)
+{
simpl in *.

assert (Gamma |-- T === TmUnit -> Delta |-- TmSb T (Sups i) === TmUnit).
intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
red in H. eauto.
eauto.
eapply EQ_TP_SB_UNIT.
eauto.
split; auto.

(* Dunit 2 *)

simpl in *.
program_simpl.
split; auto.
split; auto.
intros.
edestruct (H3 Delta0).
eauto.
program_simpl.
exists x.
split ;auto.
eapply EQ_TRANS.
eapply EQ_SYM.
apply nc_Sups_plus_eq.
rewrite plus_comm.
eauto.
rewrite plus_comm.
auto.
}
(* Duniv 1 *)
+{
assert (Gamma |-- T === TmUniv -> Delta |-- TmSb T (Sups i) === TmUniv).
intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
red in H. eauto.
eauto.
eapply EQ_TP_SB_UNIV.
eauto.
split; auto.

(* Duniv 2 *)
intros.
simpl in *.
eauto.

(* Dempty 1 *)
+{
simpl in *.

assert (Gamma |-- T === TmEmpty -> Delta |-- TmSb T (Sups i) === TmEmpty).
intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
red in H. eauto.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
split; auto.

(* Dempty 2 *)

simpl in *.
program_simpl.
split; auto. program_simpl.
intros.
program_simpl.
edestruct (H5 Delta0).
eauto.
program_simpl.
exists x.
split ;auto.
eapply EQ_TRANS.
eapply EQ_SYM.
apply nc_Sups_plus_eq.
rewrite plus_comm.
eauto.
rewrite plus_comm.
auto.
}
}

(* Univ 1 *)
+{
split; intros.
simpl in *.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eapply H1.
eapply EQ_TP_SB_UNIV.
eauto.
(* Univ 2 *)
simpl in *.
decompose record H1.
repeat split; eauto.

{
intros.
assert (CxtShift Delta0 (i1 + i0) Gamma) as HX1.
{
eapply CxtShift_comp; eauto.
}

specialize (H3 _ _ HX1).
destruct H3 as [tv [HTV1 HTV2]].
exists tv.
split; auto.
eapply EQ_SYM.
eapply EQ_TRANS.
eapply EQ_SYM.
eassumption.
rewrite plus_comm.
eapply nc_Sups_plus_eq.
rewrite plus_comm.
eauto.
}

exists x; eauto.

specialize (H _ _ x).
specialize (H t).
specialize (H _ _ _ H0).
destruct H.
eapply H.
eauto.
}

(* Dfun 1 *)
+{
renamer.
rename i into HDA.
rename i2 into i.
rename e into eDB.
rename e0 into eDB'.
rename i1 into HREC.
rename H into IHF.
rename IHHDT into IHA.
rename i0 into HPDA.
rename H0 into HCxtShift.

split.
intros.
simpl in *.
destruct H as [A [F [HS1 [HS2 HS3]]]].
exists (TmSb A (Sups i)), (TmSb F (Sext (Sseq (Sups i) Sup) TmVar)).
split.

assert (Gamma |-- A /\  Gamma,,A |-- F).
eapply Inversion_FUN_F.
eauto.

clear_inversion H.
eauto.
(**)
split.
specialize (IHA A Gamma Delta i HCxtShift).
destruct IHA as [IHA_Type IHA_Term].
auto.

(**)

intros Psi j a da DB DB' PA HPA Hda Happ Happ' HCxtShift' HP.
specialize (HS3 Psi (j + i) a da DB DB').
specialize (HS3 PA HPA Hda Happ Happ').


specialize (CxtShift_comp); intro HCxtShift''.
specialize (HCxtShift'' Gamma Delta i Psi j HCxtShift HCxtShift').
specialize (HS3 HCxtShift'').


assert ( Psi |-- a : TmSb (TmSb A (Sups i)) (Sups j) ) as HP1.
eapply RelTerm_implies_WtTm.
eauto.

assert ( Psi |-- TmSb (TmSb A (Sups i)) (Sups j) ) as HP2.
eauto.

assert ( Psi ||- a : TmSb A (Sups (j + i)) #aeq# da #in# HDA ) as HP3.
eapply RelTerm_ClosedForConversion.
eauto.
apply EQ_TP_SYM.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq'.
rewrite plus_comm; eauto.
eapply RelType_implies_WfTp; eauto.
eauto.
eauto.
apply EQ_REFL.
auto.

specialize (HS3 HP3).
eapply RelType_ClosedForConversion.
eauto.

assert (Gamma |-- TmFun A F) as HX1 by eauto.

assert (Gamma |-- A /\ Gamma,,A |-- F) as HX2.
{
eapply Inversion_FUN_F; eauto.
}

destruct HX2 as [HX2 HX3].

eapply F_tp_subst; eauto 3.

eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply SB_F.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eauto.

(* Dfun 2 *)
+{
intros.
simpl in *.
destruct H as [HS1 [A [F [PT [HS2 [HS3 [HS4 [HS5 HS6]]]]]]]].

repeat (split; auto).
eauto.

(**)

exists (TmSb A (Sups i)), (TmSb F (Sext (Sseq (Sups i) Sup) TmVar)).
exists PT.
repeat (split; auto).

assert (Gamma |-- TmFun A F) by eauto.
assert (Gamma |-- A /\ Gamma,,A |-- F) as HX2.
{
eapply Inversion_FUN_F; eauto.
}
destruct HX2 as [HX2 HX3].
eauto.

(**)

specialize (IHA A Gamma Delta i HCxtShift).
destruct IHA as [IHA_Type IHA_Term].
eauto.

(**)

intros Psi j a da DB DB' PA HPA Hda Happ Happ' HCxtShift' HP.
specialize (HS6 Psi (j + i) a da DB DB').
specialize (HS6 PA HPA Hda Happ Happ').

specialize (CxtShift_comp); intro HCxtShift''.
specialize (HCxtShift'' Gamma Delta i Psi j HCxtShift HCxtShift').
specialize (HS6 HCxtShift'').

assert ( Psi |-- a : TmSb (TmSb A (Sups i)) (Sups j) ) as HP1.
eapply RelTerm_implies_WtTm.
eauto.

assert ( Psi |-- TmSb (TmSb A (Sups i)) (Sups j) ) as HP2.
eauto.

assert (Gamma |-- TmFun A F) as HYX1 by eauto.
assert (Gamma |-- A /\ Gamma,,A |-- F) as HYX2.
{
eapply Inversion_FUN_F; eauto.
}
destruct HYX2 as [HYX2 HYX3].

assert ( Psi ||- a : TmSb A (Sups (j + i)) #aeq# da #in# HDA ) as HP3.
eapply RelTerm_ClosedForConversion.
eauto.
apply EQ_TP_SYM.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq'.
rewrite plus_comm; eauto.
eauto.
eauto.
eauto.
apply EQ_REFL.
auto.

specialize (HS6 HP3).

destruct HS6 as [dy HS6].
exists dy.
destruct HS6 as [HS7 HS8].
split; auto.

eapply RelTerm_ClosedForConversion.
eauto.

eapply F_tp_subst.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eauto.
eauto.
eauto.
eapply SB_F.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eauto.
eauto.

eapply F_subst.
eapply RelTerm_implies_WtTm.
eauto.
}
}
Qed.

(******************************************************************************
 *)

Fact RelTypeLifting: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall Delta i, CxtShift Delta i Gamma ->
    Gamma ||- T #in# HDT -> Delta ||- TmSb T (Sups i) #in# HDT
.
Proof.
intros.
edestruct RelLifting; eauto.
Qed.

(******************************************************************************
 *)
Fact RelTermLifting: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType) t dt,
  forall Delta i, CxtShift Delta i Gamma ->
    Gamma ||- t : T #aeq# dt #in# HDT -> Delta ||- TmSb t (Sups i) : TmSb T (Sups i) #aeq# dt #in# HDT
.
Proof.
intros.
edestruct RelLifting; eauto.
Qed.

(******************************************************************************
 *)
Fact Sups1_tpeq: forall Gamma A T,
  Gamma |-- T ->
  Gamma,,A |-- TmSb T (Sups 1) ->
  Gamma,,A |-- TmSb T (Sups 1) === TmSb T Sup
.
Proof.
intros.
assert (Gamma,,A |--) by eauto.

clear_inversion H1.
simpl.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SIDL.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Fact Sups1_wt: forall Gamma A T,
  Gamma |-- T ->
  Gamma |-- A ->
  Gamma,,A |-- TmSb T (Sups 1)
.
Proof.
intros; simpl.
eapply SB_F; auto.
eapply SSEQ; eauto.
auto.
Qed.

(******************************************************************************
 *)

Corollary RelTypeLift1: forall Gamma A T DT DT' (HDT : DT === DT' #in# PerType),
    Gamma ||- T #in# HDT -> Gamma |-- A -> Gamma,,A ||- TmSb T Sup #in# HDT
.
Proof.
intros.
remember (Gamma,,A) as Delta.
eapply RelType_ClosedForConversion with (T := TmSb T (Sseq Sid Sup)).
fold (Sups 1).
eapply RelTypeLifting.
red; subst; simpl; eauto.
auto.
subst; apply Sups1_tpeq; eauto.
eapply RelType_implies_WfTp; eauto.
apply Sups1_wt; eauto.
eapply RelType_implies_WfTp.
eauto.
Qed.

(******************************************************************************
 *)

Corollary RelTermLift1: forall Gamma A T DT DT' (HDT : DT === DT' #in# PerType) t dt,
    Gamma ||- t : T #aeq# dt #in# HDT -> Gamma |-- A -> Gamma,,A ||- TmSb t Sup : TmSb T Sup #aeq# dt #in# HDT
.
Proof.
intros.
remember (Gamma,,A) as Delta.
eapply RelTerm_ClosedForConversion with (T := TmSb T (Sseq Sid Sup)) (t := TmSb t (Sseq Sid Sup)); fold (Sups 1).
eapply RelTypeLifting.
red; subst; simpl; eauto.
eapply RelTerm_implies_RelType; eauto.
subst; apply Sups1_tpeq; auto.
eapply RelTerm_implies_WfTp; eauto.
apply Sups1_wt; auto.
eapply RelType_implies_WfTp; auto.
eapply RelTerm_implies_RelType; eauto.
(**)
eapply RelTermLifting.
subst; red; simpl; eauto.
auto.
(**)
subst; simpl.
eapply EQ_CONG_SB.
eauto.
eapply EQ_REFL.
eapply RelTerm_implies_WtTm; auto.
eauto.
Qed.

(******************************************************************************
 *)

Lemma RelSb_lift1: forall Gamma sb Delta denv A,
  Gamma ||- [sb] : Delta #aeq# denv ->
  Gamma |-- A ->
  Gamma,,A ||- [Sseq sb Sup] : Delta #aeq# denv
.
Proof.
intros.
induction H.
constructor 1.
eauto.
(*-*)
assert (Gamma |-- [sb] : Delta).
eapply RelSb_wellformed; eauto.

assert (Gamma |-- [Sext sb M] : Delta,,A0).
eauto. 
clear_inversion_on_sb H9.

econstructor 2; eauto. 
eapply RelType_ClosedForConversion with (T := TmSb (TmSb A0 sb) Sup).
eapply RelTypeLift1.
eauto.
auto.
(**)
eapply EQ_TP_SBSEQ; eauto.
(**)
eapply RelTerm_ClosedForConversion with (T := TmSb (TmSb A0 sb) Sup) (t := TmSb M Sup).
eapply RelTypeLift1.
eauto.
auto.
eapply EQ_TP_SBSEQ; eauto.
(**)
eapply RelTermLift1.
eauto.
auto.
eapply EQ_REFL;  eauto.
(**)
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL; eauto.
apply H3.
eauto.
Qed.

(******************************************************************************
 *)
Lemma RelSb_lift1_left: forall Gamma sb Delta denv dt A,
  Gamma ||- [sb] : Delta,,A #aeq# (Dext denv dt) ->
  Delta |-- A ->
  Gamma ||- [Sseq Sup sb] : Delta #aeq# denv
.
Proof.
intros. 
clear_inversion H.
rename sb into SB.
rename sb0 into sb.

assert (Gamma |-- [sb] : Delta).
eapply RelSb_wellformed; eauto.

assert (Gamma |-- [Sext sb M] : Delta,,A).
eauto.

clear_inversion_on_sb H1.

eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eauto.
eapply EQ_SB_REFL; eauto.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
auto.
Qed.


(******************************************************************************
 *)

Lemma RelSb_ClosedUnderLift: forall Gamma sb Delta denv,
  Gamma ||- [sb] : Delta #aeq# denv -> forall Psi i, CxtShift Psi i Gamma -> 
    Psi ||- [Sseq sb (Sups i)] : Delta #aeq# denv
.
Proof.
(**)
intros.
red in H0.
generalize dependent Gamma.
generalize dependent sb.
generalize dependent Delta.
generalize dependent denv.
generalize dependent Psi.
induction i; intros.
(**)
simpl in *.
clear_inversion_on_sb H0.
eapply RelSb_ContextConversion; eauto.
eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDR.
eauto using RelSb_wellformed.
eauto.
(**)
simpl in *.
edestruct WtSb_SUPL with (Gamma := Psi) as [Theta [A [HTheta HA] ]]; eauto.
eapply RelSb_ContextConversion; eauto.
specialize (IHi Theta denv Delta).
specialize (IHi sb Gamma H).
specialize (IHi HA).

eapply RelSb_ClosedUnderEq.
eapply EQ_SB_COMM.
eapply SUP.
assert (Theta,,A |--) by eauto.
clear_inversion H1.
auto.

eauto.
eapply RelSb_wellformed; eauto.

apply RelSb_lift1.
auto.
assert (Theta,,A |--) by eauto.
clear_inversion H1.
auto.
Qed.

(******************************************************************************
 *)

Lemma ValEnv_interp: forall A Gamma denv denv' PA,
  Gamma |-- A ->
  InterpTypePer A denv PA ->
  [denv] === [denv'] #in# Gamma ->
  InterpTypePer A denv' PA
.
Proof.
intros.
assert (Gamma |= A).
apply Valid_for_WfTp; auto.
clear_inversion H2.
edestruct H4 as [DA [DB]].
eauto.
program_simpl.
red.
exists DB; split; auto.
red in H0.
destruct H0.
destruct H0.
compute_sth.
Qed.

