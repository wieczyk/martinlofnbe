(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Proof that context-conversion work with logical relations
 *)

Set Implicit Arguments.


Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.

(******************************************************************************
 *)
Lemma RelContextConversion: forall Gamma DT DT' (HDT : DT === DT' #in# PerType),
  (forall T, 
    Gamma ||- T #in# HDT -> forall Psi, |-- {Gamma} === {Psi} ->
      Psi ||- T #in# HDT) /\
  (forall t T dt,
    Gamma ||- t : T #aeq# dt #in# HDT -> forall Psi, |-- {Gamma} === {Psi} ->
      Psi ||- t : T #aeq# dt #in# HDT)
.
Proof.
do 4 intro.
induction HDT using PerType_dind; split.

(* Neutral type *)
+{
intros.
simpl in *.
decompose record H.
repeat split; eauto.
eapply ContextConversion_tp; eauto.
}
(**)
+{
intros.
simpl in *.
decompose record H.
repeat split; eauto.
eapply ContextConversion_tp; eauto.
}
(*Nat *)
+{
intros.
simpl in *.
eapply ContextConversion_tpeq; eauto.
}
(**)
+{
intros.
simpl in *.
program_simpl.
repeat split; auto.
eapply ContextConversion_tpeq; eauto.
intros.
edestruct H2.
eauto.
program_simpl.
exists x.
split; auto.
}
(*Unit*)
+{
intros.
simpl in *.
eapply ContextConversion_tpeq; eauto.
}
(**)
+{
intros.
simpl in *.
program_simpl.
repeat split; auto.
eapply ContextConversion_tpeq; eauto.
intros.
edestruct H2.
eauto.
program_simpl.
exists x.
split; auto.
}
(* Empty *)
+{
intros.
simpl in *.
eapply ContextConversion_tpeq; eauto.
}
(**)
+{
intros.
simpl in *.
program_simpl.
repeat split; auto.
eapply ContextConversion_tpeq; eauto.
intros.
edestruct H2.
eauto.
program_simpl.
exists x.
split; auto.
}
(*Univ*) 
+{
intros.
simpl in *.
eapply ContextConversion_tpeq; eauto.
}
(**)
+{
intros.
simpl in *.
decompose record H0.
repeat split; eauto.
eapply ContextConversion_tm; eauto.
eapply ContextConversion_tpeq; eauto.
exists x.
specialize (H dt dt x).
destruct H.
specialize (H t).
eapply H; eauto.
}
(*Fun*)
+{
intros.
simpl in *.
program_simpl.
rename H0 into A.
rename H4 into F.

exists A. exists F.
repeat split; eauto.
eapply ContextConversion_tpeq; eauto.
}
(**)
+{
intros.
simpl in *.
program_simpl.
rename H2 into A.
rename H5 into F.
rename H6 into PT.
repeat split; eauto.

eapply ContextConversion_tm; eauto.
exists A, F, PT.
repeat split; eauto.

eapply ContextConversion_tpeq; eauto.
}
Qed.


(******************************************************************************
 *)
Lemma RelType_ContextConversion: forall Gamma DT DT' (HDT : DT === DT' #in# PerType) T,

    Gamma ||- T #in# HDT -> forall Psi, |-- {Gamma} === {Psi} ->
      Psi ||- T #in# HDT
.
Proof.
eapply RelContextConversion.
Qed.

(******************************************************************************
 *)
Lemma RelTerm_ContextConversion: forall Gamma DT DT' (HDT : DT === DT' #in# PerType) t T dt,
    Gamma ||- t : T #aeq# dt #in# HDT -> forall Psi, |-- {Gamma} === {Psi} ->
      Psi ||- t : T #aeq# dt #in# HDT
.
Proof.
eapply RelContextConversion.
Qed.


(******************************************************************************
 *)
Lemma RelSb_ContextConversion: forall Gamma sb Delta denv,
  Gamma ||- [sb] : Delta #aeq# denv -> forall Psi,
    |-- {Gamma} === {Psi} ->
  Psi ||- [sb] : Delta #aeq# denv
.
Proof.
do 5 intro.
induction H; intros.
constructor 1.
eapply ContextConversion_sb.
eauto.
eauto.

econstructor 2.
eapply IHRelSb; eauto.
eapply RelType_ContextConversion; eauto.
eapply RelTerm_ContextConversion; eauto.
eapply ContextConversion_sbeq; eauto.
eauto.
eauto.
eauto.
eauto.
Qed.
