(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Logical relations
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

(******************************************************************************
 *)

Scheme PerType_dind := Induction for PerType Sort Prop.

(******************************************************************************
 * Notations for Logical Relations
 *)
Reserved Notation "Gamma ||- A #in# X "
 (no associativity, at level 75, A, X at next level).

Reserved Notation "Gamma ||- t : A #aeq# d #in# X"
 (no associativity, at level 75, A, X, t, d at next level).

(******************************************************************************
 * Inversion function for recursion
 *)
Fact PerType_inv_Dfun_DA: forall DT DT' DA DF DA' DF',
  DT === DT' #in# PerType ->
  DT = Dfun DA DF ->
  DT' = Dfun DA' DF' ->
  DA === DA' #in# PerType
.
do 7 intro.  
auto_inversion_on H.
intros.
injection H5.
injection H6.
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr.
assumption.
Defined.

(******************************************************************************
 * Inversion function for recursion
 *)
Fact PerType_inv_Dfun_DB: forall DT DT' DA DF DA' DF',
  DT === DT' #in# PerType ->
  DT = Dfun DA DF ->
  DT' = Dfun DA' DF' ->
  (forall a0 a1 DB0 DB1 PA, InterpType DA PA -> a0 === a1 #in# PA -> App DF a0 DB0 -> App DF' a1 DB1 ->
    DB0 === DB1 #in# PerType
  )
.
do 7 intro.  
auto_inversion_on H.
do 12 intro.
injection H5.
injection H6.
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr;
let Hr := fresh "Hr" in intro Hr; try rewrite <- Hr.
assumption.
Defined.

(******************************************************************************
 * Inversion function for recursion
 *)
Fact PerType_inv_Duniv: forall DT DT',
   DT === DT' #in# PerType ->
   DT = Duniv ->
   DT' = Duniv ->
   forall d d', d === d' #in# PerUniv -> d === d' #in# PerType
.
do 3 intro.
auto_inversion_on H.
do 3 intro.
assumption.
Defined.

(******************************************************************************
 * Logical relations
 *
 * The relation RelType connects a judgement 'Gamma |-- T' with 
 * an element 'DT' from PerType's domain, such that reification of
 * 'DT' is convertible to A.
 *
 * Second relation is between a judgement 'Gamma |-- t : T' and
 * an element 'DT' from PerType's domain and an element 'dt' from
 * PER relation assigned to DT. Relation guards if reification of 'dt'
 * is convertible to 't'.
 *
 * Relation have to be defined on elements which belongs to PerType's
 * domains, we achieve this by structural recursion on the
 * proof 'DT === DT #in# PerType'. Shape of this proof should be irrelevant
 * for relation, we check this in 'Rel_ProofIrrelevance' lemma.
 *)

Fixpoint RelType (Gamma:Cxt) (T:Tm) (DT DT':D) (HDT : DT === DT' #in# PerType) : Prop :=
  match DT as _DT, DT' as _DT' return (DT = _DT -> DT' = _DT' -> Prop)
    with

    | DupN DE, DupN DE' => fun _ _ =>
      Gamma |-- T /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (DdownN DT) v /\
          Delta |-- TmSb T (Sups i) === v 
      ) 
      

    | Dnat, Dnat => fun _ _ =>
      Gamma |-- T === TmNat

    | Dunit, Dunit => fun _ _ =>
      Gamma |-- T === TmUnit

    | Dempty, Dempty => fun _ _ =>
      Gamma |-- T === TmEmpty

    | Duniv, Duniv => fun _ _ =>
      Gamma |-- T === TmUniv

    | Dfun DA DF, Dfun DA' DF' => fun Heq Heq' => 
      exists A F,
        Gamma |-- T === TmFun A F /\
        Gamma ||- A #in# (PerType_inv_Dfun_DA HDT Heq Heq') /\
          (forall Delta i a da DB DB' PA (HPA : InterpType DA PA)
            (Hda : da === da #in# PA)
            (Happ : App DF da DB) (Happ' : App DF' da DB'),
            CxtShift Delta i Gamma ->
            Delta ||- a : (TmSb A $ Sups i) #aeq# da #in# (PerType_inv_Dfun_DA HDT Heq Heq') ->
            Delta ||- TmSb F (Sext (Sups i) a) #in# (PerType_inv_Dfun_DB HDT Heq Heq' HPA Hda Happ Happ')
          )

    | _, _ => fun _ _ =>
      False

  end (eq_refl DT) (eq_refl DT')
  where "Gamma ||- A #in# HDT" := (RelType Gamma A HDT) 

with RelTerm Gamma t T dt DT DT' (HDT : DT === DT' #in# PerType) : Prop :=
  match DT as _DT, DT' as _DT' return (DT = _DT -> DT' = _DT' -> Prop)
    with

    | DupN DE, DupN DE' => fun _ _ =>
      dt === dt #in# PerNeUniv /\
      Gamma |-- T /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (DdownN DT) v /\
          Delta |-- TmSb T (Sups i) === v 
      )  /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (Ddown DT dt) v /\
          Delta |-- TmSb t (Sups i) === v : TmSb T (Sups i)
      )  

    | Dnat, Dnat => fun _ _ =>
      dt === dt #in# PerNat /\
      Gamma |-- T === TmNat /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (Ddown Dnat dt) v /\
          Delta |-- TmSb t (Sups i) === v : TmNat 
      ) 

    | Dunit, Dunit => fun _ _ =>
      dt === dt #in# PerUnit /\
      Gamma |-- T === TmUnit /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (Ddown Dunit dt) v /\
          Delta |-- TmSb t (Sups i) === v : TmUnit 
      ) 

    | Dempty, Dempty => fun _ _ =>
      dt === dt #in# PerEmpty /\
      Gamma |-- T === TmEmpty /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (Ddown Dempty dt) v /\
          Delta |-- TmSb t (Sups i) === v : TmEmpty
      ) 

    | Duniv, Duniv => fun Heq Heq' => 
      Gamma |-- t : T /\
      Gamma |-- T === TmUniv /\
      (forall Delta i,
        CxtShift Delta i Gamma ->
        exists v, RbNf (length Delta) (Ddown Duniv dt) v /\
          Delta |-- TmSb t (Sups i) === v : TmUniv
      ) /\
      exists (HDU : dt === dt #in# PerUniv), Gamma ||- t #in# ((PerType_inv_Duniv HDT Heq Heq') _ _ HDU)

    | Dfun DA DF, Dfun DA' DF' => fun Heq Heq' =>
      Gamma |-- t : T /\
      exists A F PT,
        InterpType (Dfun DA DF) PT /\
        dt === dt #in# PT /\
        Gamma |-- T === TmFun A F /\
        Gamma ||- A #in# (PerType_inv_Dfun_DA HDT Heq Heq') /\
          (forall Delta i a da DB DB' PA (HPA : InterpType DA PA)
            (Hda : da === da #in# PA) (Happ : App DF da DB) (Happ' : App DF' da DB'),
            CxtShift Delta i Gamma ->
            Delta ||- a : (TmSb A $ Sups i) #aeq# da #in# (PerType_inv_Dfun_DA HDT Heq Heq') ->
            exists dy, App dt da dy /\
            Delta ||- TmApp (TmSb t (Sups i)) a : TmSb F (Sext (Sups i) a) #aeq# dy #in# (PerType_inv_Dfun_DB HDT Heq Heq' HPA Hda Happ Happ')
          )

    | _, _ => fun _ _ =>
      False
  end (eq_refl DT)  (eq_refl DT')

where "Gamma ||- t : A #aeq# d #in# X" := (RelTerm Gamma t A d X)
.

(******************************************************************************
 * Logical relations
 * Relations for substitutions
 *)

Reserved Notation "Gamma ||- [ s ] : Delta #aeq# env"
 (no associativity, at level 75, s at next level).


Inductive RelSb : Cxt -> Sb -> Cxt -> DEnv -> Prop :=
  | RelSb_Sid: forall Gamma sb,
    Gamma |-- [ sb ] : nil ->
    Gamma ||- [ sb ] : nil #aeq# Did

 |  RelSb_Sext: forall Gamma Delta A M SB sb denv dM DX DX' (HDX : DX === DX' #in# PerType) PX,
   Gamma ||- [ sb ] : Delta #aeq# denv ->
   Gamma ||- TmSb A sb #in# HDX ->
   Gamma ||- M : TmSb A sb #aeq# dM #in# HDX ->
   Gamma |-- [ SB ] === [Sext sb M ] : Delta,,A ->
   [denv] === [denv] #in# Delta ->
   EvalTm A denv DX ->
   InterpType DX PX ->
   dM === dM #in# PX ->
   Gamma ||- [ SB ] : Delta,,A #aeq# Dext denv dM
    
where "Gamma ||- [ s ] : Delta #aeq# denv" := (RelSb Gamma s Delta denv)
.
Hint Constructors RelSb.


