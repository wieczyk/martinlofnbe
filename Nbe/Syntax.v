(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Characterization theorem for reflexion and reification
 *)
 
Require Export Nbe.Syntax.Def.
Require Export Nbe.Syntax.System.

Require Export Nbe.Syntax.Inversion.
Require Export Nbe.Syntax.ContextConversion.
Require Export Nbe.Syntax.Validity.
Require Export Nbe.Syntax.CxtShift.

Require Export Nbe.Syntax.SbInversion.
