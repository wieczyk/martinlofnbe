import Extracted
import Instances

--
-- test
--

tV :: Int -> Tm
tV = tmVar 

(-->) = \a b -> TmFun a (TmSb b Sup)
infixr -->

-- combinators 

tpS = (TmNat --> TmNat --> TmNat) --> (TmNat --> TmNat) --> TmNat --> TmNat
tmS = TmAbs TmNat $ TmAbs TmNat $ TmAbs TmNat $ (tV 2) `TmApp` tV 0 `TmApp`  (tV 1 `TmApp` tV 0)

tpK = TmNat --> TmNat --> TmNat
tmK = TmAbs TmNat $ TmAbs TmNat $ TmSb (TmVar) Sup

tpT = (TmNat --> TmNat --> TmNat) --> TmNat --> TmNat
tmT = TmAbs TmNat $ TmAbs TmNat $ tV 1 `TmApp` tV 0 `TmApp` tV 0

tpI = (TmNat --> TmNat)
tmI = tmS `TmApp` tmK `TmApp` tmK

test_00 = ([], tmS, tpS)
test_01 = ([], tmK, tpK)
test_02 = ([], tmI, tpI)
test_03 = ([], tmT, tpT)

-- natural numbers

fromTm (Tm0)
    = 0
fromTm (TmS tm)
    = succ (fromTm tm)

apps :: Tm -> Tm -> Int -> Tm
apps f x 0 = x
apps f x n = f `TmApp` (apps f x $ n - 1)

tpN = TmNat
tmN = Tm0

tpChurch   = (TmNat --> TmNat) --> TmNat --> TmNat
tmChurch n = TmAbs (TmNat --> TmNat) $ TmAbs TmNat $ apps (tV 1) (tV 0) n

test_04 = ([], tmChurch 0, tpChurch)
test_05 = ([], tmChurch 3, tpChurch)

tpFromChurch = tpChurch --> TmNat
--        \f.     f S 0
tmFromChurch = TmAbs tpChurch $ tV 0 `TmApp` (TmAbs TmNat (TmS TmVar)) `TmApp` Tm0

tpAdd   = tpChurch --> tpChurch --> tpChurch
--        \a[3]   \b[2]   \f[1]   \x[0].  a f (b f x)
tmAdd   = TmAbs tpChurch $ TmAbs tpChurch $ TmAbs (TmNat --> TmNat) $ TmAbs TmNat $ _A `TmApp` _F `TmApp` (_B `TmApp` _F `TmApp` _X)
 where
    _A = tV 3
    _B = tV 2
    _F = tV 1
    _X = tV 0

tpAddN  = tpChurch --> tpChurch --> TmNat
--        \a[1]  \b[0]  . tpFromChurch (tpAdd a b)
tmAddN  = TmAbs tpChurch $ TmAbs tpChurch $ tmFromChurch `TmApp` (tmAdd `TmApp` _A `TmApp` _B)
 where
    _A = tV 1
    _B = tV 0


genAdd  :: Int -> Int -> Tm
genAdd a b = tmAddN `TmApp` (tmChurch a) `TmApp` (tmChurch b)

-- add 'a' and 'b' as church numerals, then convert to natural number
testAdd a b = fromTm $ norm (genAdd a b) TmNat

test_06 = ([], tmFromChurch `TmApp` tmChurch 0, TmNat)
test_07 = ([], tmFromChurch `TmApp` tmChurch 3, TmNat)
test_08 = ([], tmAddN `TmApp` tmChurch 2 `TmApp` tmChurch 3, TmNat)

-- expected:\x:Unit. ()
tpU = TmUnit
tmU = Tm1
test_10 = ([], tmU, tpU)

-- expected: \x:Unit. ()
tpU2 = TmUnit --> TmUnit
tmU2 = TmAbs TmUnit $ TmVar
test_11 = ([], tmU2, tpU2)

-- tests for neutral variables

tpSucc = TmNat --> TmNat
tmSucc = TmAbs TmNat $ TmS TmVar
test_12 = ([], tmSucc, tpSucc)

-- expected: tmEta1
tpEta1  = (TmNat --> TmNat) --> TmNat --> TmNat
tmEta1  = TmAbs (TmNat --> TmNat) $ TmAbs TmNat $ _F `TmApp` _X
 where
    _F = tV 1
    _X = tV 0


-- expected: \x. S x
tpEta2   = TmNat --> TmNat
tmEta2   = TmAbs TmNat (TmS TmVar)
test_13  = ([], tmEta2, tpEta2)

-- expected: x_0
cxtVar0  = [TmNat]
tpVar0   = TmNat
tmVar0   = tV 0
test_14  = (cxtVar0, tmVar0, tpVar0)

-- expected: x_1
cxtVar1  = [TmNat, TmNat]
tpVar1   = TmNat
tmVar1   = tV 1
test_15  = (cxtVar1, tmVar1, tpVar1)

-- expected: \x. x
cxtEta3  = [TmNat, TmNat --> TmNat]
tpEta3   = TmNat --> TmNat
tmEta3   = tV 1
tmEta3'  = TmAbs TmNat $ tV 2 `TmApp` tV 0

test_16  = (cxtEta3, tmEta3,  tpEta3)
test_17  = (cxtEta3, tmEta3', tpEta3)

-- TmFun, not tmArr
tpDep1   = TmFun TmNat TmNat
tmDep1   = TmAbs TmNat $ Tm0
test_18  = ([], tmDep1, tpDep1)

-- computation on type-level
tpDep2   = tmI `TmApp` (TmNat --> TmNat --> TmNat)
tmDep2   = tmK
test_19  = ([], tmDep2, tpDep2)

-- wrapper
norm :: Tm -> Tm -> Tm
norm tm tp = nbe tp [] tm

normCxt :: [Tm] -> Tm -> Tm -> Tm
normCxt gamma tm tp = nbe tp gamma tm

normTest :: ([Tm], Tm, Tm) -> Tm
normTest (gamma, tm, tp) = normCxt gamma tm tp

printNormTest :: ([Tm], Tm, Tm) -> IO ()
printNormTest (gamma, tm, tp) = do
    print "============================"
    print $ "Context: " ++ (show gamma)
    print $ "Term   : " ++ (show tm)
    print $ "Type   : " ++ (show tp)
    print "---------------------------"
    print $ "Result : " ++ (show . normTest $ (gamma, tm, tp))

-- named tests

tests = [
    test_00,
    test_01,
    test_02,
    test_03,
    test_04,
    test_05,
    test_06,
    test_07,
    test_08,
    test_10,
    test_11,
    test_12,
    test_13,
    test_14,
    test_15,
    test_16,
    test_17,
    test_18,
    test_19
    ]

runTests = mapM_ (printNormTest) tests
