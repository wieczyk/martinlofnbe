module Extracted where

import qualified Prelude

__ :: any
__ = Prelude.error "Logical or arity value used"

type Sig a =
  a
  -- singleton inductive, whose constructor was exist
  
sumbool_rect :: (() -> a1) -> (() -> a1) -> Prelude.Bool -> a1
sumbool_rect f f0 s =
  case s of {
   Prelude.True -> f __;
   Prelude.False -> f0 __}

sumbool_rec :: (() -> a1) -> (() -> a1) -> Prelude.Bool -> a1
sumbool_rec =
  sumbool_rect

sub :: Prelude.Int -> Prelude.Int -> Prelude.Int
sub = \a b -> if (Prelude.<=) a b then 0 else (Prelude.-) a b 

data Tm =
   TmVar
 | TmApp Tm Tm
 | TmAbs Tm Tm
 | Tm0
 | TmS Tm
 | TmSb Tm Sb
 | Tm1
 | TmNat
 | TmUnit
 | TmFun Tm Tm
 | TmEmpty
 | TmAbsurd Tm
 | TmUniv
data Sb =
   Sid
 | Sup
 | Sseq Sb Sb
 | Sext Sb Tm

tm_rect :: a1 -> (Tm -> a1 -> Tm -> a1 -> a1) -> (Tm -> a1 -> Tm -> a1 -> a1)
           -> a1 -> (Tm -> a1 -> a1) -> (Tm -> a1 -> Sb -> a1) -> a1 -> a1 ->
           a1 -> (Tm -> a1 -> Tm -> a1 -> a1) -> a1 -> (Tm -> a1 -> a1) -> a1
           -> Tm -> a1
tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t =
  case t of {
   TmVar -> f;
   TmApp t0 t1 ->
    f0 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t1);
   TmAbs t0 t1 ->
    f1 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t1);
   Tm0 -> f2;
   TmS t0 -> f3 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0);
   TmSb t0 s -> f4 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0) s;
   Tm1 -> f5;
   TmNat -> f6;
   TmUnit -> f7;
   TmFun t0 t1 ->
    f8 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0) t1
      (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t1);
   TmEmpty -> f9;
   TmAbsurd t0 -> f10 t0 (tm_rect f f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 t0);
   TmUniv -> f11}

tm_rec :: a1 -> (Tm -> a1 -> Tm -> a1 -> a1) -> (Tm -> a1 -> Tm -> a1 -> a1)
          -> a1 -> (Tm -> a1 -> a1) -> (Tm -> a1 -> Sb -> a1) -> a1 -> a1 ->
          a1 -> (Tm -> a1 -> Tm -> a1 -> a1) -> a1 -> (Tm -> a1 -> a1) -> a1
          -> Tm -> a1
tm_rec =
  tm_rect

sb_rect :: a1 -> a1 -> (Sb -> a1 -> Sb -> a1 -> a1) -> (Sb -> a1 -> Tm -> a1)
           -> Sb -> a1
sb_rect f f0 f1 f2 s =
  case s of {
   Sid -> f;
   Sup -> f0;
   Sseq s0 s1 -> f1 s0 (sb_rect f f0 f1 f2 s0) s1 (sb_rect f f0 f1 f2 s1);
   Sext s0 t -> f2 s0 (sb_rect f f0 f1 f2 s0) t}

sb_rec :: a1 -> a1 -> (Sb -> a1 -> Sb -> a1 -> a1) -> (Sb -> a1 -> Tm -> a1)
          -> Sb -> a1
sb_rec =
  sb_rect

tmVar :: Prelude.Int -> Tm
tmVar i =
  (\ fO fS n -> if (Prelude.==) n 0 then fO () else fS ((Prelude.-) n 1))
    (\_ ->
    TmVar)
    (\k -> TmSb (tmVar k)
    Sup)
    i

type Cxt = [] Tm

data D =
   Dclo Tm DEnv
 | DupX D DNe
 | DupN DNe
 | DS D
 | D0
 | Dnat
 | Dfun D D
 | Dunit
 | D1
 | Dempty
 | Dabsurd D
 | Duniv
data DNe =
   Dvar Prelude.Int
 | Dapp DNe DNf
 | DneAbsurd DNf DNe
data DNf =
   DdownX D D
 | DdownN D
data DEnv =
   Did
 | Dext DEnv D

disFun_dec :: D -> Prelude.Bool
disFun_dec d =
  case d of {
   Dfun _ _ -> Prelude.True;
   _ -> Prelude.False}

disUnit_dec :: D -> Prelude.Bool
disUnit_dec d =
  case d of {
   Dunit -> Prelude.True;
   _ -> Prelude.False}

dup :: D -> DNe -> D
dup dT d =
  let {s = disFun_dec dT} in
  case s of {
   Prelude.True -> DupX dT d;
   Prelude.False ->
    let {s0 = disUnit_dec dT} in
    case s0 of {
     Prelude.True -> DupX dT d;
     Prelude.False -> DupN d}}

ddown :: D -> D -> DNf
ddown dT d =
  let {s = disFun_dec dT} in
  case s of {
   Prelude.True -> DdownX dT d;
   Prelude.False ->
    let {s0 = disUnit_dec dT} in
    case s0 of {
     Prelude.True -> DdownX dT d;
     Prelude.False -> DdownN d}}

evalTm :: Tm -> DEnv -> D
evalTm tm denv =
  case tm of {
   TmVar ->
    case denv of {
     Did -> Prelude.error "absurd case";
     Dext _ d' -> d'};
   TmApp tm0 tm1 -> app (evalTm tm0 denv) (evalTm tm1 denv);
   TmAbs _ tm0 -> Dclo tm0 denv;
   Tm0 -> D0;
   TmS n -> DS (evalTm n denv);
   TmSb tm0 sb0 -> evalTm tm0 (evalSb sb0 denv);
   Tm1 -> D1;
   TmNat -> Dnat;
   TmUnit -> Dunit;
   TmFun tp0 tp1 -> Dfun (evalTm tp0 denv) (Dclo tp1 denv);
   TmEmpty -> Dempty;
   TmAbsurd a -> Dabsurd (evalTm a denv);
   TmUniv -> Duniv}

evalSb :: Sb -> DEnv -> DEnv
evalSb sb denv =
  case sb of {
   Sid -> denv;
   Sup ->
    case denv of {
     Did -> Prelude.error "absurd case";
     Dext denv' _ -> denv'};
   Sseq s1 s2 -> evalSb s1 (evalSb s2 denv);
   Sext s1 m1 -> Dext (evalSb s1 denv) (evalTm m1 denv)}

app :: D -> D -> D
app df dx =
  case df of {
   Dclo tm denv -> evalTm tm (Dext denv dx);
   DupX dT e ->
    case dT of {
     Dfun dA dF -> dup (app dF dx) (Dapp e (ddown dA dx));
     _ -> Prelude.error "absurd case"};
   Dabsurd dA ->
    case dx of {
     DupN u -> dup dA (DneAbsurd (DdownN dA) u);
     _ -> Prelude.error "absurd case"};
   _ -> Prelude.error "absurd case"}

evalCxt :: Cxt -> DEnv
evalCxt gamma =
  case gamma of {
   ([]) -> Did;
   (:) a' gamma' ->
    let {denv = evalCxt gamma'} in
    Dext denv (dup (evalTm a' denv) (Dvar (Prelude.length gamma')))}

rbNe :: Prelude.Int -> DNe -> Tm
rbNe m e =
  case e of {
   Dvar j -> tmVar (sub m (Prelude.succ j));
   Dapp e0 d -> TmApp (rbNe m e0) (rbNf m d);
   DneAbsurd dA u -> TmApp (TmAbsurd (rbNf m dA)) (rbNe m u)}

rbNf :: Prelude.Int -> DNf -> Tm
rbNf m e =
  case e of {
   DdownX dT f ->
    case dT of {
     Dfun dA dB -> TmAbs (rbNf m (DdownN dA))
      (rbNf (Prelude.succ m)
        (ddown (app dB (dup dA (Dvar m))) (app f (dup dA (Dvar m)))));
     Dunit -> Tm1;
     _ -> Prelude.error "absurd case"};
   DdownN da ->
    case da of {
     DupN e0 -> rbNe m e0;
     DS d -> TmS (rbNf m (DdownN d));
     D0 -> Tm0;
     Dnat -> TmNat;
     Dfun dA dF -> TmFun (rbNf m (DdownN dA))
      (rbNf (Prelude.succ m) (DdownN (app dF (dup dA (Dvar m)))));
     Dunit -> TmUnit;
     Dempty -> TmEmpty;
     Duniv -> TmUniv;
     _ -> Prelude.error "absurd case"}}

nbe :: Tm -> Cxt -> Tm -> Tm
nbe a gamma t =
  let {denv = evalCxt gamma} in
  rbNf (Prelude.length gamma) (ddown (evalTm a denv) (evalTm t denv))

wtTm_nbetest :: Cxt -> Tm -> Tm -> Tm -> Prelude.Bool
wtTm_nbetest gamma t s a =
  let {
   tm_eqdec tm0 tm1 =
     tm_rec (\tm2 ->
       case tm2 of {
        TmVar -> Prelude.True;
        _ -> Prelude.False}) (\t0 _ t1 _ tm2 ->
       case tm2 of {
        TmApp t2 t3 ->
         sumbool_rec (\_ ->
           sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
             (tm_eqdec t1 t3)) (\_ -> Prelude.False) (tm_eqdec t0 t2);
        _ -> Prelude.False}) (\t0 _ t1 _ tm2 ->
       case tm2 of {
        TmAbs t2 t3 ->
         sumbool_rec (\_ ->
           sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
             (tm_eqdec t1 t3)) (\_ -> Prelude.False) (tm_eqdec t0 t2);
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        Tm0 -> Prelude.True;
        _ -> Prelude.False}) (\t0 _ tm2 ->
       case tm2 of {
        TmS t1 ->
         sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
           (tm_eqdec t0 t1);
        _ -> Prelude.False}) (\t0 _ s0 tm2 ->
       case tm2 of {
        TmSb t1 s1 ->
         sumbool_rec (\_ ->
           sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
             (sb_rec (\s2 ->
               case s2 of {
                Sid -> Prelude.True;
                _ -> Prelude.False}) (\s2 ->
               case s2 of {
                Sup -> Prelude.True;
                _ -> Prelude.False}) (\_ h0 _ h1 s3 ->
               case s3 of {
                Sseq s4 s5 ->
                 sumbool_rec (\_ ->
                   sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
                     (h1 s5)) (\_ -> Prelude.False) (h0 s4);
                _ -> Prelude.False}) (\_ h0 t2 s2 ->
               case s2 of {
                Sext s3 t3 ->
                 sumbool_rec (\_ ->
                   sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
                     (tm_eqdec t2 t3)) (\_ -> Prelude.False) (h0 s3);
                _ -> Prelude.False}) s0 s1)) (\_ -> Prelude.False)
           (tm_eqdec t0 t1);
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        Tm1 -> Prelude.True;
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        TmNat -> Prelude.True;
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        TmUnit -> Prelude.True;
        _ -> Prelude.False}) (\t0 _ t1 _ tm2 ->
       case tm2 of {
        TmFun t2 t3 ->
         sumbool_rec (\_ ->
           sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
             (tm_eqdec t1 t3)) (\_ -> Prelude.False) (tm_eqdec t0 t2);
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        TmEmpty -> Prelude.True;
        _ -> Prelude.False}) (\t0 _ tm2 ->
       case tm2 of {
        TmAbsurd t1 ->
         sumbool_rec (\_ -> Prelude.True) (\_ -> Prelude.False)
           (tm_eqdec t0 t1);
        _ -> Prelude.False}) (\tm2 ->
       case tm2 of {
        TmUniv -> Prelude.True;
        _ -> Prelude.False}) tm0 tm1}
  in tm_eqdec (nbe a gamma t) (nbe a gamma s)

nf :: Cxt -> Tm -> Tm -> Tm
nf gamma t a =
  nbe a gamma t

